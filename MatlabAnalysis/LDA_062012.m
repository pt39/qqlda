function [Errorsfilt,FinalData1a,FinalData1a1,FinalData1b,FinalData1c,FinalData1d ] = LDA_062012(SynDataOBJ1,SynDataOBJ2,numberOfTrialsOBJ1,numberOfTrialsOBJ2,ia,ma,excelname,pptname,fingerOnset1_1,fingerOnset2_1,fingerOnset1_2,fingerOnset2_2)            
%close all
Y=200;%the point where you want to plot data up to.
smary=200;%if you want to save then put any thing other than 7.
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

Data1M=SynDataOBJ1;
Data1=SynDataOBJ2;
 g1=max(size(Data1));
 g2=max(size(Data1M));
 g=min(g1,g2);
 if g1>g2
     Data1(g2+1:end,:,:)=[];
 else
     Data1M(g1+1:end,:,:)=[];
 end
 SynData=cat(3,Data1M,Data1);
FinalData=SynData;
   FinalData1a=SynData(:,5,:);  
   FinalData1a1=SynData(:,6,:); 
   FinalData1b=SynData(:,8:10,:);
   FinalData1c=SynData(:,12:14,:); 
   FinalData1d=SynData(:,16:18,:);

 % FinalData11=cat(2,FinalData1a,FinalData1a1,FinalData1b,FinalData1c,FinalData1d); %%%%%excellent results here
 FinalData11=cat(2,SynData(:,1:5,:),SynData(:,9:11,:),SynData(:,13:15,:),SynData(:,18:20,:));
 %FinalData11=cat(2,FinalData1a,FinalData1a1,FinalData1b,FinalData1c,FinalData1d);
 %FinalData11=cat(2,FinalData(:,6,:),FinalData(:,10,:),FinalData(:,14,:),FinalData(:,8,:),FinalData(:,12,:),FinalData(:,16,:)); 
 
 FinalData11=FinalData(:,1:8,:) ;
% FinalData11=cat(2,FinalData(:,6,:),FinalData(:,9,:));
 NofTri=[numberOfTrialsOBJ1;numberOfTrialsOBJ2];  
%  NofTri=[1;1];  
 
% fid = fopen('DB.txt','a')
% fwrite(fid,Errorsfilt)
 AR11=[];
 AR12=[];
 AR13=[];
 AR21=[];
 AR22=[];
 AR23=[];
 
%%
j1=6;
%hala alan bayad bebinam 
figure(1)
title('IndexMPJ angle for two conditions,black(obj1)')
xlabel('index')
ylabel('IndexMPJ angle')
for i=1:numberOfTrialsOBJ1
% for i=1:1
hold on 
plot(SynDataOBJ1(1:Y,j1,i),'k')
hold on
%plot(fingerOnset1_1(i),SynDataOBJ1(fingerOnset1_1(i)),'r*')
%pause(2)

  AR11=cat(2,AR11,SynDataOBJ1(:,j1,i));
 
end
 hold on 

for i=1:numberOfTrialsOBJ2
% for i=1:1
hold on 
plot(SynDataOBJ2(1:Y,j1,i),'m')
AR21=cat(2,AR21,SynDataOBJ2(:,j1,i));
hold on
%plot(fingerOnset1_2(i),SynDataOBJ1(fingerOnset1_2(i)),'r*')
%pause(2)
end
%%
figure(2)
title('IndexPIJ angle for two conditions,black(obj1)')
xlabel('index')
ylabel('IndexPIJ angle')
j2=6;
for i=1:numberOfTrialsOBJ1
for i=1:1
hold on 
plot(SynDataOBJ1(1:Y,j2,i),'k')
AR12=cat(2,AR12,SynDataOBJ1(:,j2,i));
%pause(1)
end
hold on 
for i=1:numberOfTrialsOBJ2
% for i=1:1
hold on 

plot(SynDataOBJ2(1:Y,j2,i),'m')
AR22=cat(2,AR22,SynDataOBJ2(:,j2,i));
%pause(1)
end

%%
%Middle Index Abduction
% j3=8
% figure(3)
% title('Index Abduction angle for two conditions,black(obj1)')
% xlabel('index')
% ylabel('Abduction angle')
% for i=1:numberOfTrialsOBJ1
% hold on 
% plot(SynDataOBJ1(1:Y,j3,i),'k')
% AR13=cat(2,AR13,SynDataOBJ1(:,j3,i));
% %pause(1)
% end
% hold on 
% for i=1:numberOfTrialsOBJ2
% hold on 
% plot(SynDataOBJ2(1:Y,j3,i)+15,'m')
% AR23=cat(2,AR23,SynDataOBJ2(:,j3,i));
% %pause(1)
% end
% 
%%
% %calculates LDA and plot it on figure 4
 Errors=LinDiscrimTMS(FinalData11,NofTri)
Mean_Errors=mean(Errors);
[b,a]=butter(2,1/10);
Errorsfilt=filtfilt(b,a,Errors);

figure(4)
title('classification Error')
plot(Errorsfilt(1:70),'g','linewidth',2)

%%
resultDIRECTORY=['C:\LDA_analysis_summer2012\LDA_Results_subj\EVERYTHING\HAT\MT'];      %change current directory to subjects results
%cd(resultDIRECTORY);
%define the sheet name based on data
if ia==2 & ma==1
    s1='preleft';
elseif ia==2 & ma==2
    s1='preright';
elseif ia==3 & ma==1
    s1='postleft';
else
    s1='postright';
end

%%
index=1:1:500;

%%


%%EXPORTING TO EXCEL FILE
% if smary==7
%     
% else
% xlswrite(excelname,'index',s1,'A1')
% xlswrite(excelname,index',s1,'A2')
% xlswrite(excelname,AR11,s1,'B2')
% xlswrite(excelname,AR12,s1,'M2')
% xlswrite(excelname,AR13,s1,'X2')
% xlswrite(excelname,AR21,s1,'AI2')
% xlswrite(excelname,AR22,s1,'AT2')
% xlswrite(excelname,AR23,s1,'BE2')
% xlswrite(excelname,Errorsfilt',s1,'BP2')
% 
% r1 = strcat(s1,'column6');
% r2 = strcat(s1,'column7');
% r3 = strcat(s1,'column8');
%  
% saveppt(pptname,r1,'-f1')
% saveppt(pptname,r2,'-f2')
% saveppt(pptname,r3,'-f3')  
% saveppt(pptname,'LDA','-f4')
% end
end

