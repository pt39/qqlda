function output=Calibration(sample, caliData, caliData2, caliData3);

for i=1:size(sample,2)
    for k=1:size(sample,1)
        sample_deg(k,i)=sample(k,i)*180/pi;
    end
end

for i=1:size(sample_deg,2)
    for k=5:(size(sample_deg,1)-1)
        if ((sample_deg(k,i)-sample_deg(k-1,i))>20)
            sample_deg(k,i)=(sample_deg(k-4,i)+sample_deg(k-3,i)+sample_deg(k-2,i)+sample_deg(k-1,i)+sample_deg(k+1,i))/5;
        end
    end
end

for i=1:size(caliData,2)
    for k=1:size(caliData,1)
        caliData_deg(k,i)=caliData(k,i)*180/pi;
    end
end

for i=1:size(caliData2,2)
    for k=1:size(caliData2,1)
        caliData2_deg(k,i)=caliData2(k,i)*180/pi;
    end
end

for i=1:size(caliData3,2)
    for k=1:size(caliData3,1)
        caliData3_deg(k,i)=caliData3(k,i)*180/pi;
    end
end

trotation(1)=mean(caliData_deg(:,1));  % 0 degree
trotation(2)=mean(caliData2_deg(:,1)); % 90 degree

tMPJ(1)=mean(caliData_deg(:,2));  % 0 degree
tMPJ(2)=mean(caliData2_deg(:,2)); % 90 degree

tIJ(1)=mean(caliData_deg(:,3));  % 0 degree
tIJ(2)=mean(caliData2_deg(:,3)); % 90 degree

tABD(1)=mean(caliData_deg(:,4));  % 0 degree
tABD(2)=mean(caliData3_deg(:,4)); % 30 degree

iMPJ(1)=mean(caliData_deg(:,5));  % 0 degree
iMPJ(2)=mean(caliData2_deg(:,5)); % 90 degree

iPIJ(1)=mean(caliData_deg(:,6));  % 0 degree
iPIJ(2)=mean(caliData2_deg(:,6)); % 90 degree

iDIJ(1)=mean(caliData_deg(:,7));  % 0 degree
iDIJ(2)=mean(caliData2_deg(:,7)); % 90 degree

iABD(1)=mean(caliData_deg(:,8));  % 0 degree
iABD(2)=mean(caliData3_deg(:,8)); % 20 degree

mMPJ(1)=mean(caliData_deg(:,9));  % 0 degree
mMPJ(2)=mean(caliData2_deg(:,9)); % 90 degree

mPIJ(1)=mean(caliData_deg(:,10));  % 0 degree
mPIJ(2)=mean(caliData2_deg(:,10)); % 90 degree

mDIJ(1)=mean(caliData_deg(:,11));  % 0 degree
mDIJ(2)=mean(caliData2_deg(:,11)); % 90 degree

miABD(1)=mean(caliData_deg(:,12));  % 0 degree
miABD(2)=mean(caliData3_deg(:,12)); % 20 degree

rMPJ(1)=mean(caliData_deg(:,13));  % 0 degree
rMPJ(2)=mean(caliData2_deg(:,13)); % 90 degree

rPIJ(1)=mean(caliData_deg(:,14));  % 0 degree
rPIJ(2)=mean(caliData2_deg(:,14)); % 90 degree

rDIJ(1)=mean(caliData_deg(:,15));  % 0 degree
rDIJ(2)=mean(caliData2_deg(:,15)); % 90 degree

rmABD(1)=mean(caliData_deg(:,16));  % 0 degree
rmABD(2)=mean(caliData3_deg(:,16)); % 20 degree

pMPJ(1)=mean(caliData_deg(:,17));  % 0 degree
pMPJ(2)=mean(caliData2_deg(:,17)); % 90 degree

pPIJ(1)=mean(caliData_deg(:,18));  % 0 degree
pPIJ(2)=mean(caliData2_deg(:,18)); % 90 degree

pDIJ(1)=mean(caliData_deg(:,19));  % 0 degree
pDIJ(2)=mean(caliData2_deg(:,19)); % 90 degree

prABD(1)=mean(caliData_deg(:,20));  % 0 degree
prABD(2)=mean(caliData3_deg(:,20)); % 20 degree

for i=1:size(sample_deg,1)
%    sample_deg_cal(i,1)=90*(sample_deg(i,1)-trotation(1))/(trotation(2)-trotation(1));
%    sample_deg_cal(i,2)=90*(sample_deg(i,2)-tMPJ(1))/(tMPJ(2)-tMPJ(1));
%    sample_deg_cal(i,3)=90*(sample_deg(i,3)-tIJ(1))/(tIJ(2)-tIJ(1));
%    sample_deg_cal(i,4)=30*(sample_deg(i,4)-tABD(1))/(tABD(2)-tABD(1));
%    sample_deg_cal(i,5)=90*(sample_deg(i,5)-iMPJ(1))/(iMPJ(2)-iMPJ(1));
%    sample_deg_cal(i,6)=90*(sample_deg(i,6)-iPIJ(1))/(iPIJ(2)-iPIJ(1));
%    sample_deg_cal(i,7)=90*(sample_deg(i,7)-mMPJ(1))/(mMPJ(2)-mMPJ(1));
%    sample_deg_cal(i,8)=90*(sample_deg(i,8)-mPIJ(1))/(mPIJ(2)-mPIJ(1));
%    sample_deg_cal(i,9)=20*(sample_deg(i,9)-miABD(1))/(miABD(2)-miABD(1));
%    sample_deg_cal(i,10)=90*(sample_deg(i,10)-rMPJ(1))/(rMPJ(2)-rMPJ(1));
%    sample_deg_cal(i,11)=90*(sample_deg(i,11)-rPIJ(1))/(rPIJ(2)-rPIJ(1));
%    sample_deg_cal(i,12)=20*(sample_deg(i,12)-rmABD(1))/(rmABD(2)-rmABD(1));
%    sample_deg_cal(i,13)=90*(sample_deg(i,13)-pMPJ(1))/(pMPJ(2)-pMPJ(1));
%    sample_deg_cal(i,14)=90*(sample_deg(i,14)-pPIJ(1))/(pPIJ(2)-pPIJ(1));
%    sample_deg_cal(i,15)=20*(sample_deg(i,15)-prABD(1))/(prABD(2)-prABD(1));
  
   sample_deg_cal(i,1)=90*(sample_deg(i,1)-trotation(1))/(trotation(2)-trotation(1));
   if (tMPJ(2)==tMPJ(1))
       sample_deg_cal(i,2)=90*(sample_deg(i,2)-tMPJ(1))/90;
   else
       sample_deg_cal(i,2)=90*(sample_deg(i,2)-tMPJ(1))/(tMPJ(2)-tMPJ(1));
   end
   sample_deg_cal(i,3)=90*(sample_deg(i,3)-tIJ(1))/(tIJ(2)-tIJ(1));
   sample_deg_cal(i,4)=30*(sample_deg(i,4)-tABD(1))/(tABD(2)-tABD(1));
   sample_deg_cal(i,5)=90*(sample_deg(i,5)-iMPJ(1))/(iMPJ(2)-iMPJ(1));
   sample_deg_cal(i,6)=90*(sample_deg(i,6)-iPIJ(1))/(iPIJ(2)-iPIJ(1));
   sample_deg_cal(i,7)=90*(sample_deg(i,7)-iDIJ(1))/(iDIJ(2)-iDIJ(1));
   sample_deg_cal(i,8)=20*(sample_deg(i,8)-iABD(1))/(iABD(2)-iABD(1));
   sample_deg_cal(i,9)=90*(sample_deg(i,9)-mMPJ(1))/(mMPJ(2)-mMPJ(1));
   sample_deg_cal(i,10)=90*(sample_deg(i,10)-mPIJ(1))/(mPIJ(2)-mPIJ(1));
   sample_deg_cal(i,11)=90*(sample_deg(i,11)-mDIJ(1))/(mDIJ(2)-mDIJ(1));
   sample_deg_cal(i,12)=20*(sample_deg(i,12)-miABD(1))/(miABD(2)-miABD(1));
   sample_deg_cal(i,13)=90*(sample_deg(i,13)-rMPJ(1))/(rMPJ(2)-rMPJ(1));
   sample_deg_cal(i,14)=90*(sample_deg(i,14)-rPIJ(1))/(rPIJ(2)-rPIJ(1));
   sample_deg_cal(i,15)=90*(sample_deg(i,15)-rDIJ(1))/(rDIJ(2)-rDIJ(1));
   sample_deg_cal(i,16)=20*(sample_deg(i,16)-rmABD(1))/(rmABD(2)-rmABD(1));
   sample_deg_cal(i,17)=90*(sample_deg(i,17)-pMPJ(1))/(pMPJ(2)-pMPJ(1));
   sample_deg_cal(i,18)=90*(sample_deg(i,18)-pPIJ(1))/(pPIJ(2)-pPIJ(1));
   sample_deg_cal(i,19)=90*(sample_deg(i,19)-pDIJ(1))/(pDIJ(2)-pDIJ(1));
   sample_deg_cal(i,20)=20*(sample_deg(i,20)-prABD(1))/(prABD(2)-prABD(1));
   
end

output=sample_deg_cal;