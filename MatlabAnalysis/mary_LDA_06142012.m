clear all
close all
%Get Glove data files

% sub=20;
% sub2=20;
% pathname1 = uigetdir;
% pathname2=uigetdir;
%pathname1 ='C:\LDA_analysis_summer2012\RAW\HAT raw\DBHAT';
pathname1='R:\LDA_backup\data\LDA\RTG raw data\DBHAT';

pathname2='C:\LDA_analysis_summer2012\CYCLED\HAT cycled\DBHAT';
%pathname2='R:\LDA_backup\data\reachGrasp(processed)\CYCLED_GoodSide\HAT cycled\DBHAT';

%filename='C:\Documents and Settings\Saumya Puthenveettil\Desktop\My_Thesis_Adamovich\Copy of July_StrokeTraining_JFK\MF\MF\pretest\left\bigcircle\offset1_onset2.xls'
%pathname1 is raw data
%pathname2 is cycled file

% for mb=1:6
% 
%     for mr=1:4
%       close all  
%     
% if mr==1
%     mr1=2;  
%     mr2=1;
% elseif mr==2
%     mr1=3;
%     mr2=1;
% elseif mr==3
%     mr1=2;
%     mr2=2;
% else
%     mr1=3;
%     mr2=2;
% end
%     
%if you want to synchronize based on percentage of velocity synch=2,if want to synchronize based on absolute threshold you have to put synch=1. 
synch=1; 
  
  
    for ia=3%need to strat SN
        for ma=2   %1:2
           
            for ka=3%4 
                switch ma
                    case 1
                        impairedHand = ['left'];
                    case 2
                        impairedHand = ['right'];
                end
                   
                
              
                switch ia
                    case 2
                        dir = [pathname1 '\pretest\' impairedHand '\'];
                    case 3
                        dir = [pathname1 '\posttest\' impairedHand '\'];
                end
                
                switch ka
                   
                    case 1
                         Object1 = ['bigcircle'];
                         Object2 = ['smallcube'];
                         excelname='bigcircle smallcube.xlsx';
                         pptname='bigcircle smallcube';
                    case 2
                         Object1 = ['bigcircle'];
                         Object2 = ['smallcircle'];
                         excelname='bigcircle smallcircle.xlsx';
                         pptname='bigcircle smallcircle1';
                    case 3
                         Object1 = ['bigcube'];
                         Object2 = ['bigcircle'];
                         excelname='bigcube bigcircle.xlsx';
                         pptname='bigcube bigcircle';
                    case 4
                         Object1 = ['smallcube'];
                         Object2 = ['smallcircle'];
                         excelname='smallcube smallcircle.xlsx';
                         pptname='smallcube smallcircle';
                    case 5
                         Object1 = ['smallcube'];
                         Object2 = ['bigcube'];
                         excelname='smallcube bigcube.xlsx';
                         pptname='smallcube bigcube';
                     case 6
                         Object1 = ['bigcube'];
                         Object2 = ['smallcircle'];
                         excelname='bigcube smallcircle.xlsx';
                         pptname='bigcube smallcircle';
                         
                      case 7
                         Object1 = ['cylinder'];
                         Object2 = ['bigcircle'];
                         excelname='cylinder bigcircle.xlsx';
                         pptname='cylinder bigcircle';
                      case 8
                         Object1 = ['cylinder'];
                         Object2 = ['smallcube'];
                         excelname='cylinder smallcube.xlsx';
                         pptname='cylinder smallcube';
                      case 9
                         Object1 = ['cylinder'];
                         Object2 = ['bigcube'];
                         excelname='cylinder bigcube.xlsx';
                         
               %%%%%%%%%%%%%%%%%%% Force Sensor Subjects
                    case 10   
                         Object1 = ['Hugecircle'];
                         Object2 = ['smallcube'];
                         excelname='Hugecircle smallcube.xlsx';
                         pptname='Hugecircle smallcube';
                       case 11   
                         Object1 = ['Hugecircle'];
                         Object2 = ['bigcircle'];
                         excelname='Hugecircle bigcircle.xlsx';
                         pptname='Hugecircle bigcircle';
                      case 12   
                         Object1 = ['Hugecircle'];
                         Object2 = ['bigcube'];
                         excelname='Hugecircle bigcube.xlsx';
                         pptname='Hugecircle bigcube';
                      case 13
                         Object1 = ['Hugecircle'];
                         Object2 = ['smallcircle']; 
                         excelname='Hugecircle smallcircle.xlsx';
                         pptname='Hugecircle smallcircle';
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
                    case 14
                         Object1 = ['Hugecircle'];
                         Object2 = ['Wedge']; 
                         excelname='Hugecircle Wedge.xls';
                                            
                        
                end
                ka
                %%
clear Shape1file*
clear Shape2file*
clear force*
clear cycled*
    
                
                %s1= [dir Object1 '\offset1_onset2.xls']
                
                s1cali=[dir 'calibration'];      %load same calibration files for both objects
                cd(s1cali);
  if strcmp('left',impairedHand)==1
 shape1cali=ls('LCG*.txt');
Calitemp=load(shape1cali(1,:));    %%%%For left hand type 'RCG
Cali2temp=load(shape1cali(2,:));
Cali3temp=load(shape1cali(3,:));
end

if strcmp('right',impairedHand)==1
shape1cali=ls('RCG*.txt');
Calitemp=load(shape1cali(1,:));    %%%%For right hand type 'RCG
Cali2temp=load(shape1cali(2,:));
Cali3temp=load(shape1cali(3,:));
end
    
                %s2= [dir Object2 '\offset1_onset2.xls']
               % filename1=s1;
               % filename2=s2; 
       
             
%read data
%filename='C:\Documents and Settings\Saumya Puthenveettil\Desktop\My_Thesis_Adamovich\Copy of July_StrokeTraining_JFK\MF\MF\pretest\left\bigcircle\offset1_onset2.xls'
%sheet='Times';
%[num1,txt1,raw1] = xlsread(filename1,sheet);  %shape 1
%[num2,txt2,raw2] = xlsread(filename2,sheet);   %shape 2

 s1= [dir Object1];
 cd(s1);
 
if strcmp('left',impairedHand)==1
Shape1files=ls('LCG*.txt');    %%%%For left hand type 'RCG
Shape1files=cellstr(Shape1files);
end

if strcmp('right',impairedHand)==1
    Shape1files=ls('RCG*.txt');    %%%%For left hand type 'RCG
    Shape1files=cellstr(Shape1files);
end
    
 s2= [dir Object2];
 cd(s2);
if strcmp('left',impairedHand)==1
Shape2files=ls('LCG*.txt');    %%%%For left hand type 'RCG
Shape2files=cellstr(Shape2files);
end

if strcmp('right',impairedHand)==1
    Shape2files=ls('RCG*.txt');    %%%%For left hand type 'RCG
    Shape2files=cellstr(Shape2files);
end
    
% Shape1files=cellstr(Shape1files); 
 switch ia
                    case 2
                        dir2 = [pathname2 '\pretest\' impairedHand '\'];
                        dir1 = [pathname1 '\pretest\' impairedHand '\'];
                    case 3
                        dir2 = [pathname2 '\posttest\' impairedHand '\'];
                        dir1 = [pathname1 '\posttest\' impairedHand '\'];
                end
filename3=[dir2 Object1 '\Cycled.xls'];
filename4=[dir2 Object2 '\Cycled.xls'];
%finding nuber of trials


% fdirectory1=[dir1 Object1 '\*.txt'];
% fdirectory2=[dir1 Object2 '\*.txt'];
% 
% 
% files1=ls(fdirectory1);
% files2=ls(fdirectory2);
% qqq=size(files1,1)/3;
% qqq2=size(files2,1)/3;
%%
%read cycled file
[num3,txt3,raw3] = xlsread(filename3);
[num4,txt4,raw4] = xlsread(filename4);

      

 %%%%SHAPE 1%first organize cycled times files as increasing order
 cycledtimes1=zeros(length(txt3),3);  %shape 1
 
 kk=['_1.txt';'_2.txt';'_3.txt';'_4.txt';'_5.txt';'_6.txt';'_7.txt';'_8.txt';'_9.txt';'10.txt';'11.txt';'12.txt'];
 
for i=1:size(txt3,1)
   if strcmp(Shape1files(i,1),'RCGdata1_1.txt') 
       
    else
 for k=1:length(kk)
    val=strfind(Shape1files(i,1),kk(k,:));
    val=cell2mat(val);
    if isempty(val)==0
        break
   end
 end
 Shape1filesa(k,1)=Shape1files(i,1);
 cycledtimes1(k,1)=num3(i,1);
 cycledtimes1(k,2)=num3(i,2);
 cycledtimes1(k,3)=num3(i,3);
  end
 end
 
%%%%Second organize force sensor files in order

 %forcetimes1=zeros(length(txt1),2);
 kk=['r1.txt';'r2.txt';'r3.txt';'r4.txt';'r5.txt';'r6.txt';'r7.txt';'r8.txt';'r9.txt';'10.txt';'11.txt';'12.txt'];
%  



%%%%SHAPE 2%first organize cycled times files as increasing order
 cycledtimes2=zeros(length(txt4),3);  %shape 2
 
 kk=['_1.txt';'_2.txt';'_3.txt';'_4.txt';'_5.txt';'_6.txt';'_7.txt';'_8.txt';'_9.txt';'10.txt';'11.txt';'12.txt'];
 
for i=1:size(txt4,1)
    
    i
    %if there is any outlier trial then uncomment next line and replace the
    %trial name.
%    if strcmp(Shape2files(i,1),'RCGdata1_1.txt') 
%       
%    else
 for k=1:length(kk)
    val=strfind(Shape2files(i,1),kk(k,:));
    val=cell2mat(val);
    if isempty(val)==0
        break
    end
 
 end
 Shape2filesa(k,1)=Shape2files(i,1);
 cycledtimes2(k,1)=num4(i,1);
 cycledtimes2(k,2)=num4(i,2);
 cycledtimes2(k,3)=num4(i,3);
     %end
 end
 
%%%%Second organize force sensor files in order

%  forcetimes2=zeros(length(txt2),2);
%  kk=['r1.txt';'r2.txt';'r3.txt';'r4.txt';'r5.txt';'r6.txt';'r7.txt';'r8.txt';'r9.txt';'10.txt';'11.txt';'12.txt'];
%  
% for i=1:length(txt2)
%   
%  for k=1:length(kk)
%     val=strfind(raw2(i,1),kk(k,:));
%     val=cell2mat(val);
%     if isempty(val)==0
%         break
%     end
%  end
%  
%  forcetimes2(k,1)=num2(i,1);
%  forcetimes2(k,2)=num2(i,2);
%  
% end

%%%check if new shapeXfiles a has any empty cells for each shape
%%if its empty then delete that cell

t=1;
for i=1:length(Shape1filesa)
if iscellstr(Shape1filesa(i,:))==1
Shape1filesa1(t,:)=Shape1filesa(i,:);
t=t+1;
end
end
Shape1filesa=Shape1filesa1;
   %shape 2
t=1;
for i=1:length(Shape2filesa)
if iscellstr(Shape2filesa(i,:))==1
Shape2filesa1(t,:)=Shape2filesa(i,:);
t=t+1;
end
end
Shape2filesa=Shape2filesa1;
%%%check if new ForcetimesX  has any empty cells for each shape
%%if its empty then delete that cell

% check=find(forcetimes1(1:end,1));    %find nonzero entries
% t=1
% for i=1:length(check)
% forcetimes1a(t,1)=forcetimes1(check(i),1);
% forcetimes1a(t,2)=forcetimes1(check(i),2);
% t=t+1;
% end
% 
% forcetimes1=forcetimes1a;
   %shape 2
% check=find(forcetimes2(1:end,1));    %find nonzero entries
% t=1
% for i=1:length(check)
% forcetimes2a(t,1)=forcetimes2(check(i),1);
% forcetimes2a(t,2)=forcetimes2(check(i),2);
% t=t+1;
% end
% 
% forcetimes2=forcetimes2a;

%%check cycledtimes files for any zeros

for i=1:length(cycledtimes1)
    if cycledtimes1(i,1)<20
        cycledtimes1(i,1)=0;
    end
end

for i=1:length(cycledtimes2)
    if cycledtimes2(i,1)<20
        cycledtimes2(i,1)=0;
    end
end



check=find(cycledtimes1(1:end,1));    %find nonzero entries
t=1;
for i=1:length(check)
cycledtimes1a(t,1)=cycledtimes1(check(i),1);
cycledtimes1a(t,2)=cycledtimes1(check(i),2);
cycledtimes1a(t,3)=cycledtimes1(check(i),3);
t=t+1;
end

cycledtimes1=cycledtimes1a;
   %shape 2
check=find(cycledtimes2(1:end,1));    %find nonzero entries
t=1
for i=1:length(check)
cycledtimes2a(t,1)=cycledtimes2(check(i),1);
cycledtimes2a(t,2)=cycledtimes2(check(i),2);
cycledtimes2a(t,3)=cycledtimes2(check(i),3);
t=t+1;
end

cycledtimes2=cycledtimes2a;
v=1;
% %AVERAGE cycled times data
% for i=1:length(cycledtimes1)
%     if cycledtimes1(i,1)>0
%     newvector1(v,1)=cycledtimes1(i,1);
%     v=v+1;
%     end
% end
% av1=mean(newvector1);
% 
% v=1;
% %AVERAGE cycled times data
% for i=1:length(cycledtimes2)
%     if cycledtimes2(i,1)>0
%     newvector2(v,1)=cycledtimes2(i,1);
%     v=v+1;
%     end
% end
% av2=mean(newvector2);

min1=min(cycledtimes1(:,1));   %synchronization
   sub=min1-1;
%   for z=1:size(cycledtimes1,2)
%   for i5=1:size(cycledtimes1,1)
%   cycledtimes1(i5,z)=cycledtimes1a(i5,z)-cycledtimes1a(i5,1)+min1;
%   end
%   end


 %%%%load Shape 1 Glove files
 cd(s1)
Data1=[];
OriginalData1=[];
 i2=1;
 for i=1:size(cycledtimes1,1)
 clear x1
  clear xtemp
  clear nonzeroentry
 x=load(char(Shape1filesa(i)));
 [b,a]=butter(2,1/10);%%15%%50 is the best
    x=filtfilt(b,a,x);
  x=calibration(x,Calitemp,Cali2temp,Cali3temp);
  
%
  
 if cycledtimes1(i,1)~=-999999
     
%     if cycledtimes1(i,1)<av1; %43;
%         cycledtimes1(i,1)=av1;
%     end
 
 %x1=x(cycledtimes1(i,1)-24:end,:);   
  if i2<size(cycledtimes1,1)+1 
     if length(x)>=(cycledtimes1(i,2))
         if cycledtimes1(i,2)>0
         %x1=x(cycledtimes1(i,1)-5:end,:);
         x1=x;
         end
     end
     end
     
      if i2<size(cycledtimes1,1)+1 
    
          if cycledtimes1(i,2)<=0
       % x1=x(cycledtimes1(i,1)-5:end,:);%%%open to suggestion
        x1=x;
          end
    
      end
     
       
      if i2<size(cycledtimes1,1)+1 
    
          if size(x,1)<(cycledtimes1(i,2))
        x1=x(:,:);%%%open to suggestion
          end
    
     end
     % end
     
      
     if size(x1,1)<100
         x4=zeros(100,20);
         x4(1:size(x1,1),:)=x1(:,:);
         for ii=size(x1,1)+1:length(x4)
         x4(ii,:)=x1(size(x1,1),:);
         end
         x1=x4;
     end
     
      x2=(size(x1,1)/100);   %normalize to 100 points
      x3=zeros(100,20);
      for norm=1:100
      x3(norm,:)=(x1(floor(x2*norm),:));
      end
      Data1=cat(3,Data1,x3);    % only from onset1 to offset 1
   
 else
     CONTINUE
 end
 i2=i2+1;

 %%
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%save original data, normalize
 %x=x(cycledtimes1(i,1)-sub:end,:);
      x=x(1:end,:);
     if size(x,1)<500
         x4test=zeros(500,20);
         x4test(1:size(x,1),:)=x(:,:);
         
     
     for ii=size(x,1)+1:length(x4test)
         x4test(ii,:)=x(length(x),:);
     end
     x=x4test;
 end
 
  xx=(size(x,1)/500);   %normalize to 500 points
      xxx=zeros(500,20);
      for normx=1:500
      xxx(normx,:)=(x(floor(xx*normx),:));
      end           % preserve data in another variable for entire movement
      OriginalData1=cat(3,OriginalData1,xxx);  
 end
 
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%
   min2=min(cycledtimes2(:,1));   %synchronize
%   for z=1:size(cycledtimes2,2)
%   for i=1:size(cycledtimes2,1)
%   cycledtimes2(i,z)=cycledtimes2a(i,z)-cycledtimes2a(i,1)+min2;
%   end
%   end

 
sub2=min2-1;
 
%load Shape 2 Glove files
  cd(s2)
Data2=[];
 i2=1;
 
 OriginalData2=[];
 for i=1:size(cycledtimes2,1)
 clear x1
  clear xtemp
  clear nonzeroentry
 x=load(char(Shape2filesa(i)));
 [b,a]=butter(2,1/10);%15%%50 is the best one
    x=filtfilt(b,a,x);
  x=calibration(x,Calitemp,Cali2temp,Cali3temp);
 if cycledtimes2(i,1)~=-999999
   
  
       
%     if cycledtimes2(i,1)<av2 %41%av2/2;
%         cycledtimes2(i,1)=av2;
%     end
%  
    %x1=x(cycledtimes2(i,1)-24:end,:);
  if i2<size(cycledtimes2,1)+1 
     if size(x,1)>=(cycledtimes2(i,2))
         if cycledtimes2(i,2)>0
        %x1=x(cycledtimes2(i,1)-5:end,:);
          x1=x;
         end
     end
     end
     
      if i2<size(cycledtimes2,1)+1 
    
          if cycledtimes2(i,2)<0
        %x1=x(cycledtimes2(i,1)-5:end,:);%%%open to suggestion
         x1=x;
          end
    
      end
     
       
      if i2<size(cycledtimes2,1)+1 
    
          if size(x,1)<(cycledtimes2(i,2))
       % x1=x(cycledtimes2(i,1)-5:end,:);%%%open to suggestion
         x1=x;
          end
    
     end
%      % end
     
      
     if size(x1,1)<100
         x4=zeros(100,20);
         x4(1:size(x1,1),:)=x1(:,:);
         for ii=size(x1,1)+1:length(x4)
         x4(ii,:)=x1(size(x1,1),:);
         end
         x1=x4;
     end
     
      x2=(size(x1,1)/100);   %normalize to 100 points
      x3=zeros(100,20);
      for norm=1:100
      x3(norm,:)=(x1(floor(x2*norm),:));
      end
      Data2=cat(3,Data2,x3);    % only from onset1 to offset 1
   
 else
    CONTINUE
 end
 i2=i2+1;
 %%
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%save original data, normalize
  %x=x(cycledtimes2(i,1)-sub2:end,:);
     x=x(1:end,:);
     if size(x,1)<500
          x4test=zeros(500,20);
         x4test(1:size(x,1),:)=x(:,:);
         
     
     for ii=size(x,1)+1:length(x4test)
         x4test(ii,:)=x(size(x,1),:);
     end
     x=x4test;
     
     end
      xx=(size(x,1)/500);   % preserve data in another variable for entire movement
      xxx=zeros(500,20);
      for normx=1:500
      xxx(normx,:)=(x(floor(xx*normx),:));
      end           % preserve data in another variable for entire movement
      OriginalData2=cat(3,OriginalData2,xxx);                   
 end
 numberOfTrialsOBJ1=size(Data1,3);
 numberOfTrialsOBJ2=size(Data2,3);
 FinalData=cat(3,Data1,Data2);
 OriginalData=cat(3,OriginalData1,OriginalData2);
 
 
 
 %%
 %Two options for synchronization the first one(Synabsolutethreshold) use absolute threshold the
 %second function(SynFun) use percentage of velocity.
   
if synch==1
[ SynData ] = Synabsolutethreshold(OriginalData(1:end,:,:),numberOfTrialsOBJ1,numberOfTrialsOBJ2);  
else
[ SynData ] = SynFun(OriginalData(1:end,:,:),numberOfTrialsOBJ1,numberOfTrialsOBJ2);
end
%SynData=OriginalData;

 
 
 %%
 SynDataOBJ1=SynData(:,:,1:numberOfTrialsOBJ1); 
 SynDataOBJ2=SynData(:,:,numberOfTrialsOBJ1+1:numberOfTrialsOBJ2+numberOfTrialsOBJ1);
%  SynDataOBJ1=SynData(:,:,1); 
%  SynDataOBJ2=SynData(:,:,numberOfTrialsOBJ1+1);
 %LDA_062012 calculations 
 [Errorsfilt,FinalData1a,FinalData1a1,FinalData1b,FinalData1c,FinalData1d] = LDA_062012(SynDataOBJ1(:,:,:),SynDataOBJ2(:,:,:),numberOfTrialsOBJ1,numberOfTrialsOBJ2,ia,ma,excelname,pptname);

aaa=1;

   end
        end
   

         
    end
%     pause(5)
%     end
%     end    

      
