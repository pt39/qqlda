close all
clear all

for ka=3:3
     close all
     
 
     
switch ka
                   
                    case 1
                         Object1 = ['bigcircle'];
                         Object2 = ['smallcube'];
                         excelname='AVG2bigcircle smallcube.xlsx';
                         pptname='AVG2bigcircle smallcube';
                         
                    case 2
                         Object1 = ['bigcircle'];
                         Object2 = ['smallcircle'];
                         excelname='AVG2bigcircle smallcircle.xlsx';
                         pptname='AVG2bigcircle smallcircle';
                         
                    case 3
                         Object1 = ['bigcube'];
                         Object2 = ['bigcircle'];
                         excelname='AVG2bigcube bigcircle.xlsx';
                         pptname='AVG2bigcube bigcircle';
                        name1='LDA(HAT)_bigcube bigcicle.xlsx'
                    case 4
                         Object1 = ['smallcube'];
                         Object2 = ['smallcircle'];
                         excelname='AVG2smallcube smallcircle.xlsx';
                         pptname='AVG2smallcube smallcircle';
                         
                    case 5
                         Object1 = ['smallcube'];
                         Object2 = ['bigcube'];
                         excelname='AVG2smallcube bigcube.xlsx';
                         pptname='AVG2smallcube bigcube';
                         
                     case 6
                         Object1 = ['bigcube'];
                         Object2 = ['smallcircle'];
                         excelname='AVG2bigcube smallcircle.xlsx';
                          pptname='AVG2bigcube smallcircle';

end

%%
%
%%
%pre impaired
A1=xlsread(excelname,'DBHAT','B2:B300');
A2=xlsread(excelname,'AD','B2:B300');
A3=xlsread(excelname,'KSHAT','B2:B300');%this
A4=xlsread(excelname,'VH','B2:B300');
A5=xlsread(excelname,'MT','B2:B300');
A6=xlsread(excelname,'JS','B2:B300');
A7=xlsread(excelname,'JJS2','B2:B300');




%this part is to concat all coloumns and then
figure(1)
plot(A1,'y','LineWidth',1.5)
hold on
plot(A2,'g','LineWidth',1.5)
hold on
plot(A3,'m','LineWidth',1.5)
hold on
plot(A4,'b','LineWidth',1.5)
hold on
plot(A5,'k','LineWidth',1.5)
hold on
plot(A6,'c','LineWidth',1.5)
hold on
plot(A7,'k--','LineWidth',1.5)



 X1 = [A1 A2 A3 A4 A5 A6 A7];
 for i=1:size(X1,1)
     
 
avg_LDA1(i)= mean(X1(i,:));
 end
 avg_preImpaired=avg_LDA1';
 hold on
 plot(avg_preImpaired,'r','LineWidth',3.5)
 legend('DBHAT','AD','KSHAT','VH','MT','JS','JJS2')
 xlswrite(name1,A1,'PRE IMPAIRED','B2');
 xlswrite(name1,A2,'PRE IMPAIRED','C2');
 xlswrite(name1,A3,'PRE IMPAIRED','D2');
 xlswrite(name1,A4,'PRE IMPAIRED','E2');
 xlswrite(name1,A5,'PRE IMPAIRED','F2');
 xlswrite(name1,A6,'PRE IMPAIRED','G2');
 xlswrite(name1,A7,'PRE IMPAIRED','H2');
 xlswrite(name1,avg_preImpaired,'PRE IMPAIRED','I2');
 index2=1:1:300
 xlswrite(name1,index2','PRE IMPAIRED','A2');
 
%%
%post impaired

C1=xlsread(excelname,'DBHAT','C2:C300');
C2=xlsread(excelname,'AD','C2:C300');
C3=xlsread(excelname,'KSHAT','C2:C300');%this
C4=xlsread(excelname,'VH','C2:C300');
C5=xlsread(excelname,'MT','C2:C300');
C6=xlsread(excelname,'JS','C2:C300');
C7=xlsread(excelname,'JJS2','C2:C300');




%
figure(2)
plot(C1,'y','LineWidth',1.5)
hold on
plot(C2,'g','LineWidth',1.5)
hold on
plot(C3,'m','LineWidth',1.5)
hold on
plot(C4,'b','LineWidth',1.5)
hold on
plot(C5,'k','LineWidth',1.5)
hold on
plot(C6,'c','LineWidth',1.5)
hold on
plot(C7,'k--','LineWidth',1.5)

 X2 = [C1 C2 C3 C4 C5 C6 C7];
 for i=1:size(X2,1)
     
 
avg_LDA2(i)= mean(X2(i,:));
 end
 avg_postImpaired=avg_LDA2';
 hold on
 plot(avg_postImpaired,'r','LineWidth',3.5)
 legend('DBHAT','AD','KSHAT','VH','MT','JS','JJS2')
xlswrite(name1,C1,'POST IMPAIRED','B2');
 xlswrite(name1,C2,'POST IMPAIRED','C2');
 xlswrite(name1,C3,'POST IMPAIRED','D2');
 xlswrite(name1,C4,'POST IMPAIRED','E2');
 xlswrite(name1,C5,'POST IMPAIRED','F2');
 xlswrite(name1,C6,'POST IMPAIRED','G2');
 xlswrite(name1,C7,'POST IMPAIRED','H2');
 xlswrite(name1,avg_postImpaired,'POST IMPAIRED','I2');
 xlswrite(name1,index2','POST IMPAIRED','A2');
 %this part is optional

%%
%PRE UNIMPAIRED
D1=xlsread(excelname,'DBHAT','D2:D270');
D2=xlsread(excelname,'AD','D2:D270');
D3=xlsread(excelname,'KSHAT','D2:D270');
D4=xlsread(excelname,'VH','D2:D270');
D5=xlsread(excelname,'MT','D2:D270');
D6=xlsread(excelname,'JS','D2:D270');
D7=xlsread(excelname,'JJS2','D2:D270');



%this part is to concat all coloumns and then
figure(3)
plot(D1,'y','LineWidth',1.5)
hold on
plot(D2,'g','LineWidth',1.5)
hold on
plot(D3,'m','LineWidth',1.5)
hold on
plot(D4,'b','LineWidth',1.5)
hold on
plot(D5,'k','LineWidth',1.5)
hold on
plot(D6,'c','LineWidth',1.5)
hold on
plot(D7,'k--','LineWidth',1.5)


 X3 = [D1 D2 D3 D4 D5 D6 D7];
 for i=1:size(X3,1)
     
 
avg_LDA3(i)= mean(X3(i,:));
 end
 avg_preUnimpaired=avg_LDA3';
 hold on
 plot(avg_preUnimpaired,'r','LineWidth',3.5)
 legend('DBHAT','AD','KSHAT','VH','MT','JS','JJS2')
 xlswrite(name1,D1,'PRE UNIMPAIRED','B2');
 xlswrite(name1,D2,'PRE UNIMPAIRED','C2');
 xlswrite(name1,D3,'PRE UNIMPAIRED','D2');
 xlswrite(name1,D4,'PRE UNIMPAIRED','E2');
 xlswrite(name1,D5,'PRE UNIMPAIRED','F2');
 xlswrite(name1,D6,'PRE UNIMPAIRED','G2');
 xlswrite(name1,D7,'PRE UNIMPAIRED','H2');
 xlswrite(name1,avg_preUnimpaired,'PRE UNIMPAIRED','I2');
 xlswrite(name1,index2','PRE UNIMPAIRED','A2');
 %%
 %post unimpaired
E1=xlsread(excelname,'DBHAT','E2:E300');
E2=xlsread(excelname,'AD','E2:E300');
E3=xlsread(excelname,'KSHAT','E2:E300');
E4=xlsread(excelname,'VH','E2:E300');
E5=xlsread(excelname,'MT','E2:E300');
E6=xlsread(excelname,'JS','E2:E300');
E7=xlsread(excelname,'JJS2','E2:E300');

%this part is to concat all coloumns and then
figure(4)
plot(E1,'y','LineWidth',1.5)
hold on
plot(E2,'g','LineWidth',1.5)%the
hold on
plot(E3,'m','LineWidth',1.5)
hold on
plot(E4,'b','LineWidth',1.5)
hold on
plot(E5,'k','LineWidth',1.5)
hold on
plot(E6,'c','LineWidth',1.5)
hold on
plot(E7,'k--','LineWidth',1.5)

 X4 = [E1 E2 E3 E4 E5 E6 E7];
 for i=1:size(X4,1)
     
 
avg_LDA4(i)= mean(X4(i,:));
 end
 avg_postUnimpaired=avg_LDA4';
 hold on
 plot(avg_postUnimpaired,'r','LineWidth',3.5)
 legend('DBHAT','AD','KSHAT','VH','MT','JS','JJS2')
 xlswrite(name1,E1,'POST UNIMPAIRED','B2');
 xlswrite(name1,E2,'POST UNIMPAIRED','C2');
 xlswrite(name1,E3,'POST UNIMPAIRED','D2');
 xlswrite(name1,E4,'POST UNIMPAIRED','E2');
 xlswrite(name1,E5,'POST UNIMPAIRED','F2');
 xlswrite(name1,E6,'POST UNIMPAIRED','G2');
 xlswrite(name1,E7,'POST UNIMPAIRED','H2');
 xlswrite(name1,avg_postUnimpaired,'POST UNIMPAIRED','I2');
 xlswrite(name1,index2','POST UNIMPAIRED','A2');

%%
index=1:1:500;
xlswrite(excelname,avg_preImpaired,'AVG','B2');
xlswrite(excelname,avg_postImpaired,'AVG','C2');
xlswrite(excelname,avg_preUnimpaired,'AVG','D2');
xlswrite(excelname,avg_postUnimpaired,'AVG','E2');
xlswrite(excelname,index','AVG','A2');

saveppt(pptname,'Pretest impaired hand','-f1')
saveppt(pptname,'posttest impaired hand','-f2')
saveppt(pptname,'pretest unimpaired hand','-f3')  
saveppt(pptname,'posttest unimpaired hand','-f4')


end



