function varargout = RTGSynchroCheck(varargin)
%lines with hold on:578 559 374 353
% RTGSYNCHROCHECK M-file for RTGSynchroCheck.fig
%      RTGSYNCHROCHECK, by itself, creates a new RTGSYNCHROCHECK or raises the existing
%      singleton*.
%
%      H = RTGSYNCHROCHECK returns the handle to a new RTGSYNCHROCHECK or the handle to
%      the existing singleton*.
%
%      RTGSYNCHROCHECK('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in RTGSYNCHROCHECK.M with the given input arguments.
%
%      RTGSYNCHROCHECK('Property','Value',...) creates a new RTGSYNCHROCHECK or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before RTGSynchroCheck_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to RTGSynchroCheck_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help RTGSynchroCheck

% Last Modified by GUIDE v2.5 07-May-2014 16:02:07




%%

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @RTGSynchroCheck_OpeningFcn, ...
    'gui_OutputFcn',  @RTGSynchroCheck_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before RTGSynchroCheck is made visible.
function RTGSynchroCheck_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to RTGSynchroCheck (see VARARGIN)

% Choose default command line output for RTGSynchroCheck
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes RTGSynchroCheck wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = RTGSynchroCheck_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in TestCatigory.
function TestCatigory_Callback(hObject, eventdata, handles)
% hObject    handle to TestCatigory (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns TestCatigory contents as cell array
%        contents{get(hObject,'Value')} returns selected item from TestCatigory


% --- Executes during object creation, after setting all properties.
function TestCatigory_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TestCatigory (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





% --- Executes on selection change in HandsSelect.
function HandsSelect_Callback(hObject, eventdata, handles)
% hObject    handle to HandsSelect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns HandsSelect contents as cell array
%        contents{get(hObject,'Value')} returns selected item from HandsSelect


% --- Executes during object creation, after setting all properties.
function HandsSelect_CreateFcn(hObject, eventdata, handles)
% hObject    handle to HandsSelect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in Condition1.
function Condition1_Callback(hObject, eventdata, handles)
% hObject    handle to Condition1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Condition1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Condition1


% --- Executes during object creation, after setting all properties.
function Condition1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Condition1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function SInitial_Callback(hObject, eventdata, handles)
% hObject    handle to SInitial (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of SInitial as text
%        str2double(get(hObject,'String')) returns contents of SInitial as a double


% --- Executes during object creation, after setting all properties.
function SInitial_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SInitial (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in DataDir.
function DataDir_Callback(hObject, eventdata, handles)
% hObject    handle to DataDir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global currentDir;
global FBDir;
currentDir = uigetdir('C:');
list = find(currentDir=='\');
set(handles.SInitial,'String',currentDir(list(end)+1:end))
%gruName='HAS raw';
 %gruName='controlGroup_raw';
FBDir = [currentDir '\'];
HandSelect = ls([FBDir 'pretest']);
set(handles.Hand,'String',HandSelect(3,:));

% --- Executes on button press in LoadOuput1.
function LoadOuput1_Callback(hObject, eventdata, handles)
% hObject    handle to LoadOuput1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global A1_output A1_headertext
% load cycled output file to get onset and offset of the movement
[A1_FileName,A1_PathName,FilterIndex]= uigetfile ('*.xls','Get cycled output file');
ExcelFileName_A1 = [A1_PathName A1_FileName];
[A1_output, A1_headertext] = xlsread(ExcelFileName_A1);


% --- Executes on button press in Check.
function Check_Callback(hObject, eventdata, handles)
% hObject    handle to Check (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global currentDir;
global A1_output A1_headertext;
global FBDir;

cla(handles.axes1);
cla(handles.axes2);
cla(handles.axes5);
cla(handles.axes6);

Data=[];Data1=[];Data2=[];
% get varibles
Object1_index_selected = get(handles.Object1,'Value');
Object1_list = get(handles.Object1,'String');
Object1 = Object1_list(Object1_index_selected);

Object2_index_selected = get(handles.Object2,'Value');
Object2_list = get(handles.Object2,'String');
Object2 = Object2_list(Object2_index_selected);


TestCati={['pretest'],['posttest']};


Hands = get(handles.Hand,'String');
%
% Con1_index_selected = get(handles.Condition1,'Value');
% Con1_list = get(handles.Condition1,'String');
% Con1 = Con1_list(Con1_index_selected);

% define data directory
Objects={char(Object1),char(Object2)};
path1=currentDir;

%%
%pre test
%first object
for (test=1:1)
    directory =[path1 '\' char(TestCati(test)) '\' char(Hands) '\'];
    fingerpath=[path1 '\bigcube bigcircle.xlsx'];
    a=char(TestCati(test));
     sheetName=strcat(a(1:end-4),char(Hands));
     fingerData=xlsread(fingerpath,sheetName);
     
    objectList=ls(directory);
     
    for (ObjectNum=1:1)
        
         message = ['Plotting ' char(TestCati(test)) '\' char(Hands) '\' char(Objects(ObjectNum)) '.....'];
        set(handles.MessageBox,'String',message);
        Data=[];Data1=[];Data2=[];
        
        ExcelFileName_A1 = [directory strtrim(char(Objects(ObjectNum))) '\Cycled.xls'];
        [A1_output, A1_headertext] = xlsread(ExcelFileName_A1);
        Numtrials=size(A1_output,1);
        axSHAPE1min=min(min(fingerData(:,2:Numtrials+1)));
        axSHAPE1max=max(max(fingerData(:,2:Numtrials+1)));
        
        axSHAPE2min=min(min(fingerData(:,35:Numtrials+35)));
        axSHAPE2max=max(max(fingerData(:,2:Numtrials+35)));
%%        
%         axes(handles.axes1);
%         for i=1:size(A1_output,1)
%             hold on
%           plot(fingerData(:,i+1),'m')   
%         end
%         axes(handles.axes11);
%        for i=1:size(A1_output,1)
%             hold on
%           plot(fingerData(:,i+1),'m')   
%         end
%         hold on
%         
%         
                    
                    %%
        
        indexRecord=[];
        for (i=1:size(A1_output,1))
            
            A1_startindex_1 = A1_output(i,1);
            A1_endindex_1 = A1_output(i,2); % movement one
            
            % save onset offset information for each file
            indexRecord(i,:)=A1_output(i,1:6);
            BadIndex=find(indexRecord(i,:)==-999999);
            for (kkk=1:size(BadIndex,2))
                indexRecord(i,BadIndex(kkk))=0;
            end
            
            NOBfilenam = [directory strtrim(char(Objects(ObjectNum))) '\' char(A1_headertext(i,1))];
            
            if exist(NOBfilenam)
            NOBData = load(NOBfilenam);
            
            
            switch Hands
                case 'left'
                    GBfilename = regexprep(char(A1_headertext(i,1)),'NOB','LCG');
                case 'right'
                    GBfilename = regexprep(char(A1_headertext(i,1)),'NOB','RCG');
                    
            end
            fileG2=[FBDir char(TestCati(test)) '\' char(Hands) '\' strtrim(char(Objects(ObjectNum))) '\' GBfilename];
            GBdata=load(fileG2);
            
            
            Data = NOBData(:,:);
            DataFB = GBdata(:,:);
%             Data1 = NOBData(:,:);
%             Data2 = GBdata(:,:);
%             if (A1_endindex_1 > size(NOBData,1))
%                 index_1=index_orig-A1_startindex_1;
%             else
%                 %             Data = NOBData(A1_startindex_1:A1_endindex_1,:);
%                 %             DataFB = GBdata(A1_startindex_1:A1_endindex_1,:);
%                 index_1=index_orig;
%                 %                 Data = NOBData(:,:);
% %                 DataFB = GBdata(:,:);
%             end
            
            if isempty(Data1) | isequal(size(Data,1),size(Data1,1))
                Data1=cat(3,Data1,Data);
            else
                k=size(Data,1)-size(Data1,1);
                if k<0
                    for j=1:abs(k)
                        Data=[Data;Data(end,:)];
                    end
                    Data1=cat(3,Data1,Data);
                end
                if k>0
                    Data(end-k+1:end,:)=[];
                    Data1=cat(3,Data1,Data);
                end
            end
            
            if isempty(Data2) | isequal(size(DataFB,1),size(Data2,1))
                Data2=cat(3,Data2,DataFB);
            else
                k=size(DataFB,1)-size(Data2,1);
                if k<0
                    for j=1:abs(k)
                        DataFB=[DataFB;DataFB(end,:)];
                    end
                    Data2=cat(3,Data2,DataFB);
                end
                if k>0
                    DataFB(end-k+1:end,:)=[];
                    Data2=cat(3,Data2,DataFB);
                end
            end
            end
        end
        
        
        
        wrist=[];
        indexMPJ=[];
        for l=1:size(Data1,3)
            Arm=Data1(:,1:3,l);
            [b,a]=butter(2,8/50);
            Armf=filtfilt(b,a,Arm);
            If=size(Armf,1);
            handdif1=difnum(Armf,3,If,.01,1,1,1);
            Tangvel=(sqrt(sum((handdif1.^2)')))';
            Tangvel=procfilt(Tangvel,100);
            Tangvel=Tangvel';
            
            
%             wrist=[wrist,Data1(:,1,l)];
            wrist=[wrist,Tangvel];
            
            indexPIPt=filtfilt(b,a,Data2(:,6,l));
            indexMPJ=[indexMPJ,indexPIPt];
            switch ObjectNum
%             switch l
                case 1
                    axes(handles.axes1);
                case 2
                    axes(handles.axes2);
            end
            hold on
           index_orig=1:1:(size(Tangvel(10:end),1));
           index_1=index_orig-A1_output(l,1)+10;
           switch test
                case 1
                    plot(index_1,Tangvel(10:end),'Color','k','LineWidth',2);
                    hold on
                    axis([-100,500,0,40])
                    plot(A1_output(l,2)-A1_output(l,1),Tangvel(A1_output(l,2)),'r*')
                case 2
                    plot(index_1,Tangvel(10:end),'Color','k','LineWidth',2);
                    hold on
                    axis([-100,500,0,40])
                    plot(A1_output(l,2)-A1_output(l,1),Tangvel(A1_output(l,2)),'r*')
            end
%             for(lines=1:6)
%                 line([indexRecord(l,lines)-A1_output(l,1)+10 indexRecord(l,lines)-A1_output(l,1)+10],[-5 30],'Color','r');
%             end
            switch ObjectNum
%              switch l
                 case 1
                    axes(handles.axes5);
                    set(handles.MessageBox2,'String',char(A1_headertext(l,1)));
                case 2
                    axes(handles.axes6);
                    set(handles.MessageBox3,'String',char(A1_headertext(l,1)));
            end
            hold on;
            switch test
                
                case 1
                    %plot(index_1,indexPIPt(10:end)*30+15,'Color','k','LineWidth',2);
                    plot(fingerData(:,l+1))
                    %axis([-100,500,-10,80])
                    axis([-100,500,axSHAPE1min-5,axSHAPE1max+5])
                case 2
                    %plot(index_1,indexPIPt(10:end)*30+15,'Color','k','LineWidth',2);
                    plot(fingerData(:,l+1))
                    %axis([-100,500,-10,80])
                    axis([-100,500,axSHAPE1min-5,axSHAPE1max+5])
            end
%             for(lines=1:6)
%                 line([indexRecord(l,lines)-A1_output(l,1)+10 indexRecord(l,lines)-A1_output(l,1)+10],[-20 30],'Color','r');
%             end           
%             set(handles.MessageBox2,'String',char(A1_headertext(l,1)));
            waitforbuttonpress;
        end
        
        
        
        
        %%
        
        
%         switch ObjectNum
%             case 1
%                 axes(handles.axes1);
%             case 2
%                 axes(handles.axes2);
%             case 3
%                 axes(handles.axes3);
%             case 4
%                 axes(handles.axes4);
%             case 5
%                 axes(handles.axes5);
%             case 6
%                 axes(handles.axes6);
%             case 7
%                 axes(handles.axes7);
%             case 8
%                 axes(handles.axes8);
%         end
%         meanWrist=mean(wrist,2);
%         stdWrist=std(wrist,0,2);
%         x=[1:size(stdWrist)];
%         X=[x,fliplr(x)];                %#create continuous x value array for plotting
%         Y=[(meanWrist+stdWrist)',fliplr((meanWrist-stdWrist)')];
% %         fill(X,Y,[0.7 0.7 0.7]);
%         hold on
% %         plot(meanWrist,'*');
%         switch test
%             case 1
%                 plot(wrist(:,:),'Color','k','LineWidth',2);
%             case 2
%                 plot(wrist(:,:),'Color','r','LineWidth',2);
%         end
%         switch ObjectNum
%             case 1
%                 axes(handles.axes5);
%             case 2
%                 axes(handles.axes6);
%         end
%         hold on;
%         switch test
%             case 1
%                 plot(indexMPJ(:,:)*30+15,'Color','k','LineWidth',2);
%             case 2
%                 plot(indexMPJ(:,:)*30+15,'Color','r','LineWidth',2);
%         end

        
%         title(strtrim(objectList(ObjectNum,:)));
    end
end

%%
% pre test
% second object
for (test=1:1)
    directory =[path1 '\' char(TestCati(test)) '\' char(Hands) '\'];
    
    objectList=ls(directory);
    
    for (ObjectNum=2:2)
        
         message = ['Plotting ' char(TestCati(test)) '\' char(Hands) '\' char(Objects(ObjectNum)) '.....'];
        set(handles.MessageBox,'String',message);
        Data=[];Data1=[];Data2=[];
        
        ExcelFileName_A1 = [directory strtrim(char(Objects(ObjectNum))) '\Cycled.xls'];
        [A1_output, A1_headertext] = xlsread(ExcelFileName_A1);
        
        indexRecord=[];
        for (i=1:size(A1_output,1))
            
            A1_startindex_1 = A1_output(i,1);
            A1_endindex_1 = A1_output(i,2); % movement one
            
            % save onset offset information for each file
            indexRecord(i,:)=A1_output(i,1:6);
            BadIndex=find(indexRecord(i,:)==-999999);
            for (kkk=1:size(BadIndex,2))
                indexRecord(i,BadIndex(kkk))=0;
            end
            
            NOBfilenam = [directory strtrim(char(Objects(ObjectNum))) '\' char(A1_headertext(i,1))];
            
            if exist(NOBfilenam)
            NOBData = load(NOBfilenam);
            set(handles.MessageBox3,'String',NOBfilenam);
            
            switch Hands
                case 'left'
                    GBfilename = regexprep(char(A1_headertext(i,1)),'NOB','LCG');
                case 'right'
                    GBfilename = regexprep(char(A1_headertext(i,1)),'NOB','RCG');
                    
            end
            fileG2=[FBDir char(TestCati(test)) '\' char(Hands) '\' strtrim(char(Objects(ObjectNum))) '\' GBfilename];
            GBdata=load(fileG2);
            
            Data = NOBData(:,:);
            DataFB = GBdata(:,:);
%             if (A1_endindex_1 > size(NOBData,1))
%                 index_1=index_orig-A1_startindex_1;
% %                 Data = NOBData(A1_startindex_1:end,:);
% %                 DataFB = GBdata(A1_startindex_1:end,:);
%             else
%                 %             Data = NOBData(A1_startindex_1:A1_endindex_1,:);
%                 %             DataFB = GBdata(A1_startindex_1:A1_endindex_1,:);
%                 index_1=index_orig;
% %                 Data = NOBData(:,:);
% %                 DataFB = GBdata(:,:);
%             end
            
            if isempty(Data1) | isequal(size(Data,1),size(Data1,1))
                Data1=cat(3,Data1,Data);
            else
                k=size(Data,1)-size(Data1,1);
                if k<0
                    for j=1:abs(k)
                        Data=[Data;Data(end,:)];
                    end
                    Data1=cat(3,Data1,Data);
                end
                if k>0
                    Data(end-k+1:end,:)=[];
                    Data1=cat(3,Data1,Data);
                end
            end
            
            if isempty(Data2) | isequal(size(DataFB,1),size(Data2,1))
                Data2=cat(3,Data2,DataFB);
            else
                k=size(DataFB,1)-size(Data2,1);
                if k<0
                    for j=1:abs(k)
                        DataFB=[DataFB;DataFB(end,:)];
                    end
                    Data2=cat(3,Data2,DataFB);
                end
                if k>0
                    DataFB(end-k+1:end,:)=[];
                    Data2=cat(3,Data2,DataFB);
                end
            end
            end
        end
                
        
        
        wrist=[];
        indexMPJ=[];
        for l=1:size(Data1,3)
            Arm=Data1(:,1:3,l);
            [b,a]=butter(2,8/50);
            Armf=filtfilt(b,a,Arm);
            If=size(Armf,1);
            handdif1=difnum(Armf,3,If,.01,1,1,1);
            Tangvel=(sqrt(sum((handdif1.^2)')))';
            Tangvel=procfilt(Tangvel,100);
            Tangvel=Tangvel';
            
            
%             wrist=[wrist,Data1(:,1,l)];
            wrist=[wrist,Tangvel];
            
            indexPIPt=filtfilt(b,a,Data2(:,6,l));
            indexMPJ=[indexMPJ,indexPIPt];
            switch ObjectNum
                case 1
                    axes(handles.axes1);
                case 2
                    axes(handles.axes2);
            end
            hold on
           index_orig=1:1:(size(Tangvel(10:end),1));
           index_1=index_orig-A1_output(l,1)+10;
            switch test
                case 1
                    plot(index_1,Tangvel(10:end),'Color','k','LineWidth',2);
                    hold on
                    axis([-100,500,0,40])
                    plot(A1_output(l,2)-A1_output(l,1),Tangvel(A1_output(l,2)),'r*')
                case 2
                    plot(index_1,Tangvel(10:end),'Color','k','LineWidth',2);
                    hold on
                    axis([-100,500,0,40])
                    plot(A1_output(l,2)-A1_output(l,1),Tangvel(A1_output(l,2)),'r*')
            end
%             for(lines=1:6)
%                 line([indexRecord(l,lines)-A1_output(l,1)+10 indexRecord(l,lines)-A1_output(l,1)+10],[-5 30],'Color','r');
%             end
            switch ObjectNum
                case 1
                    axes(handles.axes5);
                case 2
                    axes(handles.axes6);
            end
            hold on;
            switch test
                case 1
                   % plot(index_1,indexPIPt(10:end)*30+15,'Color','k','LineWidth',2);
                    plot(fingerData(:,l+34),'k','LineWidth',2)
                    %axis([-100,500,-10,50])
                    axis([-100,500,axSHAPE2min-5,axSHAPE2max+5])
                case 2
                   % plot(index_1,indexPIPt(10:end)*30+15,'Color','k','LineWidth',2);
                    plot(fingerData(:,l+34),'k','LineWidth',2)
                    %axis([-100,500,-10,50])
                    axis([-100,500,axSHAPE2min-5,axSHAPE2max+5])
            end
%             for(lines=1:6)
%                 line([indexRecord(l,lines)-A1_output(l,1)+10 indexRecord(l,lines)-A1_output(l,1)+10],[-20 30],'Color','r');
%             end        
            set(handles.MessageBox3,'String',char(A1_headertext(l,1)));
            waitforbuttonpress;
        end
        
%         switch ObjectNum
%             case 1
%                 axes(handles.axes1);
%             case 2
%                 axes(handles.axes2);
%             case 3
%                 axes(handles.axes3);
%             case 4
%                 axes(handles.axes4);
%             case 5
%                 axes(handles.axes5);
%             case 6
%                 axes(handles.axes6);
%             case 7
%                 axes(handles.axes7);
%             case 8
%                 axes(handles.axes8);
%         end
%         meanWrist=mean(wrist,2);
%         stdWrist=std(wrist,0,2);
%         x=[1:size(stdWrist)];
%         X=[x,fliplr(x)];                %#create continuous x value array for plotting
%         Y=[(meanWrist+stdWrist)',fliplr((meanWrist-stdWrist)')];
% %         fill(X,Y,[0.7 0.7 0.7]);
%         hold on
% %         plot(meanWrist,'*');
%         switch test
%             case 1
%                 plot(wrist(:,:),'Color','k','LineWidth',2);
%             case 2
%                 plot(wrist(:,:),'Color','r','LineWidth',2);
%         end
%         switch ObjectNum
%             case 1
%                 axes(handles.axes5);
%             case 2
%                 axes(handles.axes6);
%         end
%         hold on;
%         switch test
%             case 1
%                 plot(indexMPJ(:,:)*30+15,'Color','k','LineWidth',2);
%             case 2
%                 plot(indexMPJ(:,:)*30+15,'Color','r','LineWidth',2);
%         end

        
%         title(strtrim(objectList(ObjectNum,:)));
    end
end

%%
% post test
% first object
for (test=2:2)
    directory =[path1 '\' char(TestCati(test)) '\' char(Hands) '\'];
    
    objectList=ls(directory);
    
    for (ObjectNum=1:1)
        
         message = ['Plotting ' char(TestCati(test)) '\' char(Hands) '\' char(Objects(ObjectNum)) '.....'];
        set(handles.MessageBox,'String',message);
        Data=[];Data1=[];Data2=[];
        
        ExcelFileName_A1 = [directory strtrim(char(Objects(ObjectNum))) '\Cycled.xls'];
        [A1_output, A1_headertext] = xlsread(ExcelFileName_A1);
        
        indexRecord=[];
        for (i=1:size(A1_output,1))
            
            A1_startindex_1 = A1_output(i,1);
            A1_endindex_1 = A1_output(i,2); % movement one
            
            % save onset offset information for each file
            indexRecord(i,:)=A1_output(i,1:6);
            BadIndex=find(indexRecord(i,:)==-999999);
            for (kkk=1:size(BadIndex,2))
                indexRecord(i,BadIndex(kkk))=0;
            end
            
            NOBfilenam = [directory strtrim(char(Objects(ObjectNum))) '\' char(A1_headertext(i,1))];
            
            if exist(NOBfilenam)
            NOBData = load(NOBfilenam);
            
            switch Hand_index_selected
                case 1
                    GBfilename = regexprep(char(A1_headertext(i,1)),'NOB','LCG');
                case 2
                    GBfilename = regexprep(char(A1_headertext(i,1)),'NOB','RCG');
                    
            end
            fileG2=[FBDir char(TestCati(test)) '\' char(Hands) '\' strtrim(char(Objects(ObjectNum))) '\' GBfilename];
            GBdata=load(fileG2);
            
            Data = NOBData(:,:);
            DataFB = GBdata(:,:);
%             if (A1_endindex_1 > size(NOBData,1))
%                 index_1=index_orig-A1_startindex_1;
% %                 Data = NOBData(A1_startindex_1:end,:);
% %                 DataFB = GBdata(A1_startindex_1:end,:);
%             else
%                 %             Data = NOBData(A1_startindex_1:A1_endindex_1,:);
%                 %             DataFB = GBdata(A1_startindex_1:A1_endindex_1,:);
%                 index_1=index_orig;
% %                 Data = NOBData(:,:);
% %                 DataFB = GBdata(:,:);
%             end
            
            if isempty(Data1) | isequal(size(Data,1),size(Data1,1))
                Data1=cat(3,Data1,Data);
            else
                k=size(Data,1)-size(Data1,1);
                if k<0
                    for j=1:abs(k)
                        Data=[Data;Data(end,:)];
                    end
                    Data1=cat(3,Data1,Data);
                end
                if k>0
                    Data(end-k+1:end,:)=[];
                    Data1=cat(3,Data1,Data);
                end
            end
            
            if isempty(Data2) | isequal(size(DataFB,1),size(Data2,1))
                Data2=cat(3,Data2,DataFB);
            else
                k=size(DataFB,1)-size(Data2,1);
                if k<0
                    for j=1:abs(k)
                        DataFB=[DataFB;DataFB(end,:)];
                    end
                    Data2=cat(3,Data2,DataFB);
                end
                if k>0
                    DataFB(end-k+1:end,:)=[];
                    Data2=cat(3,Data2,DataFB);
                end
            end
            end
        end
        
        
        
        wrist=[];
        indexMPJ=[];
        for l=1:size(Data1,3)
            Arm=Data1(:,1:3,l);
            [b,a]=butter(2,8/50);
            Armf=filtfilt(b,a,Arm);
            If=size(Armf,1);
            handdif1=difnum(Armf,3,If,.01,1,1,1);
            Tangvel=(sqrt(sum((handdif1.^2)')))';
            Tangvel=procfilt(Tangvel,100);
            Tangvel=Tangvel';
            
            
%             wrist=[wrist,Data1(:,1,l)];
            wrist=[wrist,Tangvel];
            
            indexPIPt=filtfilt(b,a,Data2(:,6,l));
            indexMPJ=[indexMPJ,indexPIPt];
            switch ObjectNum
                case 1
                    axes(handles.axes1);
                case 2
                    axes(handles.axes2);
            end
            if (l==1)
             hold off
            else
                hold on
            end
           index_orig=1:1:(size(Tangvel(10:end),1));
           index_1=index_orig-A1_output(l,1)+10;
            switch test
                case 1
                    plot(index_1,Tangvel(10:end),'Color','k','LineWidth',2);
                    
                    hold on
                    axis([-100,500,0,40])
                    plot(A1_output(l,2)-A1_output(l,1),Tangvel(A1_output(l,2)),'r*')
                case 2
                    plot(index_1,Tangvel(10:end),'Color','k','LineWidth',2);
                    hold on
                    axis([-100,500,0,40])
                    plot(A1_output(l,2)-A1_output(l,1),Tangvel(A1_output(l,2)),'r*')
            end
%             for(lines=1:6)
%                 line([indexRecord(l,lines)-A1_output(l,1)+10 indexRecord(l,lines)-A1_output(l,1)+10],[-5 30],'Color','r');
%             end
            switch ObjectNum
                case 1
                    axes(handles.axes5);
                case 2
                    axes(handles.axes6);
            end
            if (l==1)
             hold off
            else
                hold on
            end
%             hold on;
            switch test
                case 1
                    %plot(index_1,indexPIPt(10:end)*30+15,'Color','k','LineWidth',2);
                    plot(fingerData(:,l+1))
                   % axis([-100,500,-10,50])
                   axis([-100,500,axSHAPE1min-5,axSHAPE1max+5])
                case 2
                    %plot(index_1,indexPIPt(10:end)*30+15,'Color','k','LineWidth',2);
                    plot(fingerData(:,l+1))
                   % axis([-100,500,-10,50])
                   axis([-100,500,axSHAPE1min-5,axSHAPE1max+5])
            end
%             for(lines=1:6)
%                 line([indexRecord(l,lines)-A1_output(l,1)+10 indexRecord(l,lines)-A1_output(l,1)+10],[-20 30],'Color','r');
%             end        
            set(handles.MessageBox2,'String',char(A1_headertext(l,1)));
            waitforbuttonpress;
        end
        
%         switch ObjectNum
%             case 1
%                 axes(handles.axes1);
%             case 2
%                 axes(handles.axes2);
%             case 3
%                 axes(handles.axes3);
%             case 4
%                 axes(handles.axes4);
%             case 5
%                 axes(handles.axes5);
%             case 6
%                 axes(handles.axes6);
%             case 7
%                 axes(handles.axes7);
%             case 8
%                 axes(handles.axes8);
%         end
%         meanWrist=mean(wrist,2);
%         stdWrist=std(wrist,0,2);
%         x=[1:size(stdWrist)];
%         X=[x,fliplr(x)];                %#create continuous x value array for plotting
%         Y=[(meanWrist+stdWrist)',fliplr((meanWrist-stdWrist)')];
% %         fill(X,Y,[0.7 0.7 0.7]);
%         hold on
% %         plot(meanWrist,'*');
%         switch test
%             case 1
%                 plot(wrist(:,:),'Color','k','LineWidth',2);
%             case 2
%                 plot(wrist(:,:),'Color','r','LineWidth',2);
%         end
%         switch ObjectNum
%             case 1
%                 axes(handles.axes5);
%             case 2
%                 axes(handles.axes6);
%         end
%         hold on;
%         switch test
%             case 1
%                 plot(indexMPJ(:,:)*30+15,'Color','k','LineWidth',2);
%             case 2
%                 plot(indexMPJ(:,:)*30+15,'Color','r','LineWidth',2);
%         end

        
%         title(strtrim(objectList(ObjectNum,:)));
    end
end

%%
% post test
% second object
for (test=2:2)
    directory =[path1 '\' char(TestCati(test)) '\' char(Hands) '\'];
    
    objectList=ls(directory);
    
    for (ObjectNum=2:2)
        
        message = ['Plotting ' char(TestCati(test)) '\' char(Hands) '\' char(Objects(ObjectNum)) '.....'];
        set(handles.MessageBox,'String',message);
        Data=[];Data1=[];Data2=[];
        
        ExcelFileName_A1 = [directory strtrim(char(Objects(ObjectNum))) '\Cycled.xls'];
        [A1_output, A1_headertext] = xlsread(ExcelFileName_A1);
        
        indexRecord=[];
        for (i=1:size(A1_output,1))
            
            A1_startindex_1 = A1_output(i,1);
            A1_endindex_1 = A1_output(i,2); % movement one
            
            % save onset offset information for each file
            indexRecord(i,:)=A1_output(i,1:6);
            BadIndex=find(indexRecord(i,:)==-999999);
            for (kkk=1:size(BadIndex,2))
                indexRecord(i,BadIndex(kkk))=0;
            end
            
            NOBfilenam = [directory strtrim(char(Objects(ObjectNum))) '\' char(A1_headertext(i,1))];
            
            if exist(NOBfilenam)
            NOBData = load(NOBfilenam);
            set(handles.MessageBox3,'String',NOBfilenam);
            
            switch Hand_index_selected
                case 1
                    GBfilename = regexprep(char(A1_headertext(i,1)),'NOB','LCG');
                case 2
                    GBfilename = regexprep(char(A1_headertext(i,1)),'NOB','RCG');
                    
            end
            fileG2=[FBDir char(TestCati(test)) '\' char(Hands) '\' strtrim(char(Objects(ObjectNum))) '\' GBfilename];
            GBdata=load(fileG2);
            
            Data = NOBData(:,:);
            DataFB = GBdata(:,:);
%             if (A1_endindex_1 > size(NOBData,1))
%                 index_1=index_orig-A1_startindex_1;
% %                 Data = NOBData(A1_startindex_1:end,:);
% %                 DataFB = GBdata(A1_startindex_1:end,:);
%             else
%                 %             Data = NOBData(A1_startindex_1:A1_endindex_1,:);
%                 %             DataFB = GBdata(A1_startindex_1:A1_endindex_1,:);
%                 index_1=index_orig;
% %                 Data = NOBData(:,:);
% %                 DataFB = GBdata(:,:);
%             end
%             
            if isempty(Data1) | isequal(size(Data,1),size(Data1,1))
                Data1=cat(3,Data1,Data);
            else
                k=size(Data,1)-size(Data1,1);
                if k<0
                    for j=1:abs(k)
                        Data=[Data;Data(end,:)];
                    end
                    Data1=cat(3,Data1,Data);
                end
                if k>0
                    Data(end-k+1:end,:)=[];
                    Data1=cat(3,Data1,Data);
                end
            end
            
            if isempty(Data2) | isequal(size(DataFB,1),size(Data2,1))
                Data2=cat(3,Data2,DataFB);
            else
                k=size(DataFB,1)-size(Data2,1);
                if k<0
                    for j=1:abs(k)
                        DataFB=[DataFB;DataFB(end,:)];
                    end
                    Data2=cat(3,Data2,DataFB);
                end
                if k>0
                    DataFB(end-k+1:end,:)=[];
                    Data2=cat(3,Data2,DataFB);
                end
            end
            end
        end
        
        
        
        wrist=[];
        indexMPJ=[];
        for l=1:size(Data1,3)
            Arm=Data1(:,1:3,l);
            [b,a]=butter(2,8/50);
            Armf=filtfilt(b,a,Arm);
            If=size(Armf,1);
            handdif1=difnum(Armf,3,If,.01,1,1,1);
            Tangvel=(sqrt(sum((handdif1.^2)')))';
            Tangvel=procfilt(Tangvel,100);
            Tangvel=Tangvel';
            
            
%             wrist=[wrist,Data1(:,1,l)];
            wrist=[wrist,Tangvel];
            
            indexPIPt=filtfilt(b,a,Data2(:,6,l));
            indexMPJ=[indexMPJ,indexPIPt];
            switch ObjectNum
                case 1
                    axes(handles.axes1);
                case 2
                    axes(handles.axes2);
            end
%             hold on
            if (l==1)
             hold off
            else
                hold on
            end
            index_orig=1:1:(size(Tangvel(10:end),1));
           index_1=index_orig-A1_output(l,1);
           switch test
                case 1
                    plot(index_1,Tangvel(10:end),'Color','m','LineWidth',2);
                    
                    hold on
                    axis([-100,500,0,40])
                    plot(A1_output(l,2)-A1_output(l,1),Tangvel(A1_output(l,2)),'r*')
                case 2
                    plot(index_1,Tangvel(10:end),'Color','m','LineWidth',2);
                   
                    hold on
                    axis([-100,500,0,40])
                    plot(A1_output(l,2)-A1_output(l,1),Tangvel(A1_output(l,2)),'r*')
            end
%             for(lines=1:6)
%                 line([indexRecord(l,lines)-A1_output(l,1)+10 indexRecord(l,lines)-A1_output(l,1)+10],[-5 30],'Color','r');
%             end
            switch ObjectNum
                case 1
                    axes(handles.axes5);
                case 2
                    axes(handles.axes6);
            end
%             hold on;
            if (l==1)
             hold off
            else
                hold on
            end
            switch test
                case 1
                    %plot(index_1,indexPIPt(10:end)*30+15,'Color','m','LineWidth',2);
                    plot(fingerData(:,l+34),'k','LineWidth',2)
                   % axis([-100,500,-10,50])
                   axis([-100,500,axSHAPE2min-5,axSHAPE2max+5])
                case 2
%                     plot(index_1,indexPIPt(10:end)*30+15,'Color','m','LineWidth',2);
                     plot(fingerData(:,l+34),'k','LineWidth',2)
                    %axis([-100,500,-10,50])
                    axis([-100,500,axSHAPE2min-5,axSHAPE2max+5])
            end
%             for(lines=1:6)
%                 line([indexRecord(l,lines)-A1_output(l,1)+10 indexRecord(l,lines)-A1_output(l,1)+10],[-20 30],'Color','r');
%             end        
            set(handles.MessageBox3,'String',char(A1_headertext(l,1)));
            waitforbuttonpress;
        end
        
%         switch ObjectNum
%             case 1
%                 axes(handles.axes1);
%             case 2
%                 axes(handles.axes2);
%             case 3
%                 axes(handles.axes3);
%             case 4
%                 axes(handles.axes4);
%             case 5
%                 axes(handles.axes5);
%             case 6
%                 axes(handles.axes6);
%             case 7
%                 axes(handles.axes7);
%             case 8
%                 axes(handles.axes8);
%         end
%         meanWrist=mean(wrist,2);
%         stdWrist=std(wrist,0,2);
%         x=[1:size(stdWrist)];
%         X=[x,fliplr(x)];                %#create continuous x value array for plotting
%         Y=[(meanWrist+stdWrist)',fliplr((meanWrist-stdWrist)')];
% %         fill(X,Y,[0.7 0.7 0.7]);
%         hold on
% %         plot(meanWrist,'*');
%         switch test
%             case 1
%                 plot(wrist(:,:),'Color','k','LineWidth',2);
%             case 2
%                 plot(wrist(:,:),'Color','r','LineWidth',2);
%         end
%         switch ObjectNum
%             case 1
%                 axes(handles.axes5);
%             case 2
%                 axes(handles.axes6);
%         end
%         hold on;
%         switch test
%             case 1
%                 plot(indexMPJ(:,:)*30+15,'Color','k','LineWidth',2);
%             case 2
%                 plot(indexMPJ(:,:)*30+15,'Color','r','LineWidth',2);
%         end

        
%         title(strtrim(objectList(ObjectNum,:)));
    end
end

%%


set(handles.MessageBox,'String','READY....');

% --- Executes on selection change in Object1.
function Object1_Callback(hObject, eventdata, handles)
% hObject    handle to Object1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Object1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Object1


% --- Executes during object creation, after setting all properties.
function Object1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Object1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in Object2.
function Object2_Callback(hObject, eventdata, handles)
% hObject    handle to Object2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Object2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Object2


% --- Executes during object creation, after setting all properties.
function Object2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Object2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global currentDir;
global A1_output A1_headertext;
global FBDir;

cla(handles.axes1);
cla(handles.axes2);
cla(handles.axes5);
cla(handles.axes6);

Data=[];Data1=[];Data2=[];
% get varibles
Object1_index_selected = get(handles.Object1,'Value');
Object1_list = get(handles.Object1,'String');
Object1 = Object1_list(Object1_index_selected);

Object2_index_selected = get(handles.Object2,'Value');
Object2_list = get(handles.Object2,'String');
Object2 = Object2_list(Object2_index_selected);


TestCati={['pretest'],['posttest']};

Hand_index_selected = get(handles.HandsSelect,'Value');
Hand_list = get(handles.HandsSelect,'String');
Hands = Hand_list(Hand_index_selected);
%
% Con1_index_selected = get(handles.Condition1,'Value');
% Con1_list = get(handles.Condition1,'String');
% Con1 = Con1_list(Con1_index_selected);

% define data directory
Objects={char(Object1),char(Object2)};
path1=currentDir;

% plot all together
for (test=1:2)
    directory =[path1 '\' char(TestCati(test)) '\' char(Hands) '\'];
    
    objectList=ls(directory);
    
    for (ObjectNum=1:2)
        
         message = ['Plotting ' char(TestCati(test)) '\' char(Hands) '\' char(Objects(ObjectNum)) '.....'];
        set(handles.MessageBox,'String',message);
        Data=[];Data1=[];Data2=[];
        
        ExcelFileName_A1 = [directory strtrim(char(Objects(ObjectNum))) '\Cycled.xls'];
        [A1_output, A1_headertext] = xlsread(ExcelFileName_A1);
        
        indexRecord=[];
        for (i=1:size(A1_output,1))
            
            A1_startindex_1 = A1_output(i,1);
            A1_endindex_1 = A1_output(i,2); % movement one
            
            % save onset offset information for each file
            indexRecord(i,:)=A1_output(i,1:6);
            BadIndex=find(indexRecord(i,:)==-999999);
            for (kkk=1:size(BadIndex,2))
                indexRecord(i,BadIndex(kkk))=0;
            end
            
            NOBfilenam = [directory strtrim(char(Objects(ObjectNum))) '\' char(A1_headertext(i,1))];
            NOBData = load(NOBfilenam);
            
            switch Hand_index_selected
                case 1
                    GBfilename = regexprep(char(A1_headertext(i,1)),'NOB','LCG');
                case 2
                    GBfilename = regexprep(char(A1_headertext(i,1)),'NOB','RCG');
                    
            end
            fileG2=[FBDir char(TestCati(test)) '\' char(Hands) '\' strtrim(char(Objects(ObjectNum))) '\' GBfilename];
            GBdata=load(fileG2);
            
            if (A1_endindex_1 > size(NOBData,1))
                Data = NOBData(A1_startindex_1:end,:);
                DataFB = GBdata(A1_startindex_1:end,:);
            else
                %             Data = NOBData(A1_startindex_1:A1_endindex_1,:);
                %             DataFB = GBdata(A1_startindex_1:A1_endindex_1,:);
                Data = NOBData(:,:);
                DataFB = GBdata(:,:);
            end
            
            if isempty(Data1) | isequal(size(Data,1),size(Data1,1))
                Data1=cat(3,Data1,Data);
            else
                k=size(Data,1)-size(Data1,1);
                if k<0
                    for j=1:abs(k)
                        Data=[Data;Data(end,:)];
                    end
                    Data1=cat(3,Data1,Data);
                end
                if k>0
                    Data(end-k+1:end,:)=[];
                    Data1=cat(3,Data1,Data);
                end
            end
            
            if isempty(Data2) | isequal(size(DataFB,1),size(Data2,1))
                Data2=cat(3,Data2,DataFB);
            else
                k=size(DataFB,1)-size(Data2,1);
                if k<0
                    for j=1:abs(k)
                        DataFB=[DataFB;DataFB(end,:)];
                    end
                    Data2=cat(3,Data2,DataFB);
                end
                if k>0
                    DataFB(end-k+1:end,:)=[];
                    Data2=cat(3,Data2,DataFB);
                end
            end
        end
        
        
        
        wrist=[];
        indexMPJ=[];
        for l=1:size(Data1,3)
            Arm=Data1(:,1:3,l);
            [b,a]=butter(2,8/50);
            Armf=filtfilt(b,a,Arm);
            If=size(Armf,1);
            handdif1=difnum(Armf,3,If,.01,1,1,1);
            Tangvel=(sqrt(sum((handdif1.^2)')))';
            Tangvel=procfilt(Tangvel,100);
            Tangvel=Tangvel';
            
            
%             wrist=[wrist,Data1(:,1,l)];
            wrist=[wrist,Tangvel];
            
            indexPIPt=filtfilt(b,a,Data2(:,6,l));
            indexMPJ=[indexMPJ,indexPIPt];

        end
        
        switch ObjectNum
            case 1
                axes(handles.axes1);
            case 2
                axes(handles.axes2);
            case 3
                axes(handles.axes3);
            case 4
                axes(handles.axes4);
            case 5
                axes(handles.axes5);
            case 6
                axes(handles.axes6);
            case 7
                axes(handles.axes7);
            case 8
                axes(handles.axes8);
        end
        meanWrist=mean(wrist,2);
        stdWrist=std(wrist,0,2);
        x=[1:size(stdWrist)];
        X=[x,fliplr(x)];                %#create continuous x value array for plotting
        Y=[(meanWrist+stdWrist)',fliplr((meanWrist-stdWrist)')];
%         fill(X,Y,[0.7 0.7 0.7]);
        hold on
%         plot(meanWrist,'*');
        switch test
            case 1
                plot(wrist(:,:),'Color','g','LineWidth',2);
            case 2
                plot(wrist(:,:),'Color','r','LineWidth',2);
        end
        switch ObjectNum
            case 1
                axes(handles.axes5);
            case 2
                axes(handles.axes6);
        end
        hold on;
        switch test
            case 1
                plot(indexMPJ(:,:)*30+15,'Color','g','LineWidth',2);
            case 2
                plot(indexMPJ(:,:)*30+15,'Color','r','LineWidth',2);
        end

        
%         title(strtrim(objectList(ObjectNum,:)));
    end
end

set(handles.MessageBox,'String','READY....');



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu10.
function popupmenu10_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu10 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu10


% --- Executes during object creation, after setting all properties.
function popupmenu10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu11.
function popupmenu11_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu11 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu11


% --- Executes during object creation, after setting all properties.
function popupmenu11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Hand_Callback(hObject, eventdata, handles)
% hObject    handle to Hand (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Hand as text
%        str2double(get(hObject,'String')) returns contents of Hand as a double


% --- Executes during object creation, after setting all properties.
function Hand_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Hand (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
