function [Errorsfilt] = Sync_SVM_Classification(SynDataOBJ1,SynDataOBJ2,numberOfTrialsOBJ1,numberOfTrialsOBJ2)            
%close all
%data dimensions timepoints x joint x trial, data1m and data1 are for the
%two different conditions
Data1M=SynDataOBJ1;
Data1=SynDataOBJ2;
 g1=max(size(Data1));
 g2=max(size(Data1M));
 g=min(g1,g2);
 if g1>g2
     Data1(g2+1:end,:,:)=[];
 else
     Data1M(g1+1:end,:,:)=[];
 end
 FinalData11=cat(3,Data1M,Data1);
 NofTri=[numberOfTrialsOBJ1;numberOfTrialsOBJ2];  
%  NofTri=[1;1];  
 

%%
% %calculates LDA and plot it on figure 4
Errors=svmTMS(FinalData11,NofTri);
Mean_Errors=mean(Errors);
[b,a]=butter(2,1/10);
Errorsfilt=filtfilt(b,a,Errors);

% figure(4)
% title('classification Error')
% plot(Errorsfilt(1:70),'g','linewidth',2)



