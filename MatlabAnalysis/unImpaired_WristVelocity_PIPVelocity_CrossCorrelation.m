function CrossCoeff = unImpaired_WristVelocity_PIPVelocity_CrossCorrelation()
clear all
close all
clc

[data text] = xlsread('../results/DataSheet/RTG ALL with average 11_07_2013_imp_unImp.xls','Unimpaired Latest Finger+elbow');

condition_old = [];
test_old = [];
figure

for i=1:size(data,1)
    subInit = char(text(i+1,1));
    group_temp = char(text(i+1,2));
    test = char(text(i+1,3));
    hand = char(text(i+1,4));
    object = char(text(i+1,6));
    filename = char(text(i+1,7));
    
    switch group_temp
        case 'HAT'
            group = 'HAT Cycled';
        case 'HAS'
            group = 'HAS Cycled';
        case 'Controls'
            group = 'Control Group Cycled';
    end
    
    handOnset = data(i,1)/10;
    onset1 = data(i,32)/10;
    offset1 = data(i,33)/10;
    onset2 = data(i,34)/10;
    offset2 = data(i,35)/10;
    
    startIndex = onset1;
    
    condition_new = ['../CYCLED_GoodSide/' group '/' subInit '/' hand '/' object];
    test_new = ['../CYCLED_GoodSide/' group '/' subInit '/' test '/' hand '/' object];
    
    if strcmp(condition_old, condition_new)
        hold on
    else
       saveppt('unImpaired_crossCorrelation_RTGWristVelocity_PIPVelocity.ppt');
        close all
        firstFlag = false;
        secondFlag = false;
    end
    
    if (strcmp(test, 'pretest')&& (firstFlag ~= true))
        subplot(1,2,1)
        firstFlag = true;
    end
    if (strcmp(test, 'posttest')&& (secondFlag ~= true))
        subplot(1,2,2);
        secondFlag = true;
    end
    
    
    if strcmp(hand,'right')
        handPrefix='RCG';
    else
        handPrefix = 'LCG';
    end
    
    CGfilename = [handPrefix filename(4:end)];
    fileDirectory = ['../CYCLED_GoodSide/' group '/' subInit '/' test '/' hand '/' object '/' CGfilename];
    CGfile = load(fileDirectory);
    [b,a]=butter(2,8/50);
    GBFileData = filtfilt(b,a,CGfile);
    
    % wrist
    fileDirectory = ['../CYCLED_GoodSide/' group '/' subInit '/' test '/' hand '/' object '/' filename];
    NOBfile = load(fileDirectory);
    FingerVelocity = diff(GBFileData(startIndex:onset2,6))/100;
   
    Arm = [NOBfile(startIndex:onset2,4),NOBfile(startIndex:onset2,2),NOBfile(startIndex:onset2,3)];
    If = size(Arm,1);
    handdif1=difnum(Arm,3,If,.01,1,1,1);
    Tangvel=(sqrt(sum((handdif1.^2)')))';
    Tangvel=procfilt(Tangvel,100);
    Tangvel=Tangvel';
    
    if max(Tangvel>1000)
        crossCorrelation(i,1:4)=99999;
    else
        [c_ww lags]=xcorr(Tangvel(2:end),FingerVelocity,'coeff');
        [crossCorrelation(i,1) lagminIndex]=min(c_ww);
        crossCorrelation(i,2)=lags(lagminIndex);
        [crossCorrelation(i,3) lagmaxIndex]=max(c_ww);
        crossCorrelation(i,4)=lags(lagmaxIndex);
        if (strcmp(test,'pretest'))
            plot(lags,c_ww);
        else
            plot(lags,c_ww,'r');
        end
    end    
    
    if (strcmp(test,'pretest'))
        plot(lags,c_ww);
    else
        plot(lags,c_ww,'r');
    end
    title(condition_new);
    condition_old =condition_new;
    test_old = test_new;
    
end

CrossCoeff = crossCorrelation;