function varargout = plotTable(varargin)
% tableplot.m -- Programmatic main function to set up a GUI
%                containing a uitable and an axes which
%                displays rows of the table as lines,
%                plus markers that show table selections.
%
% The following callbacks are also provided, as subfunctions:
%    plot1_callback   - Plot column selected on menu as line
%    select_callback  - Plot selected table data as markers
%  Being subfunctions, they do not need handles passed to them.
%
%   Copyright 2008 The MathWorks, Inc.

% Create a figure that will have a uitable, axes and checkboxes
figure('Position', [200, 50, 1700, 960],...
    'Name', 'TablePlot',...  % Title figure
    'NumberTitle', 'off',... % Do not show figure number
    'MenuBar', 'figure');      % Hide standard menu bar menus

filename=char(varargin(1));
sheet= char(varargin{1,2});
CatigoryColumn=2;
% Load some tabular data (traffic counts from somewhere)
[count_temp, A1_headertext] = xlsread(filename,sheet);

count=count_temp(:,1:end);
% count = load('count.dat');
tablesize = size(count);    % This demo data is 24-by-3

% Define parameters for a uitable (col headers are fictional)
colnames = A1_headertext(1,2:end);
% Define parameters for a uitable (col headers are fictional)
rownames = A1_headertext(2:end,1);

% All column contain numeric data (integers, actually)
colfmt = {'numeric', 'numeric', 'numeric'};
% Disallow editing values (but this can be changed)
coledit = [false false false];
% Set columns all the same width (must be in pixels)
colwdt = {60 60 60};
% Create a uitable on the left side of the figure
htable = uitable('Units', 'normalized',...
    'Position', [0.025 0.03 0.5 0.92],...
    'Data',  count,...
    'ColumnName', colnames,...
    'RowName', rownames,...
    'ToolTipString',...
    'Select cells to highlight them on the plot',...
    'CellSelectionCallback', {@select_callback});

% Create an axes on the right side; set x and y limits to the
% table value extremes, and format labels for the demo data.
haxes = axes('Units', 'normalized',...
    'Position', [0.55 .065 .40 .85])
title(haxes, 'Plot')   % Describe data set
% Prevent axes from clearing when new lines or markers are plotted
hold(haxes, 'all')

% Create an invisible marker plot of the data and save handles
% to the lineseries objects; use this to simulate data brushing.
hmkrs = plot(count, 'LineStyle', 'none',...
    'Marker', 'o',...
    'MarkerFaceColor', 'y',...
    'HandleVisibility', 'off',...
    'Visible', 'off');

% Create an advisory message (prompt) in the plot area;
% it will vanish once anything is plotted in the axes.
axpos = get(haxes, 'Position');
ptpos = axpos(1) + .1*axpos(3);
ptpos(2) = axpos(2) + axpos(4)/2;
ptpos(3) = .4; ptpos(4) = .035;

% Create three check boxes to toggle plots for columns

initBox=.91;

for CheckBox=1:45
    hcheckBox(CheckBox)=uicontrol('Style', 'checkbox',...
        'Units', 'normalized',...
        'Position', [.014 initBox-(CheckBox-1)*0.0187 .009 .015],...
        'Value', 0,...
        'Callback', {@plot_callback,CheckBox});
end

% Create a text label to say what the checkboxes do
uicontrol('Style', 'text',...
    'Units', 'normalized',...
    'Position', [.035 .96 .06 .015],...
    'String', 'Select All',...
    'FontWeight', 'bold');

hcheckBox_all=uicontrol('Style', 'checkbox',...
    'Units', 'normalized',...
    'Position', [.014 0.96 .009 .015],...
    'Value', 0,...
    'Callback', {@selectall_callback});

hgroup = uicontrol('Style', 'checkbox',...
    'Units', 'normalized',...
    'Position', [.1 0.96 .05 .015],...
    'Value', 0,...
    'String','Plot Group',...
    'Callback', {@Groupplot_callback,46});

hnormalize = uicontrol('Style', 'checkbox',...
    'Units', 'normalized',...
    'Position', [.17 0.96 .05 .015],...
    'Value', 0,...
    'String','Normalize',...
    'Callback', {@Normalize_callback});


%creat popup mennu listing variables
Variables_all=char(A1_headertext(1,2:end));
Variables=cellstr(Variables_all(CatigoryColumn+1:end,1:4));
Var_list=unique(Variables,'first');
hpopup=uicontrol('Style', 'popupmenu',...
    'Units', 'normalized',...
    'String', Var_list,...
    'Position', [0.3 0.97 0.05 0.015],...
    'Callback', {@Replot_callback,42});




% Create a text label to say what the checkboxes do
uicontrol('Style', 'text',...
    'Units', 'normalized',...
    'Position', [.55 .955 .06 .035],...
    'String', 'Number of Catigory Columns',...
    'FontWeight', 'bold');

% Create a text label to say what the checkboxes do
hedit=uicontrol('Style', 'edit',...
    'Units', 'normalized',...
    'Position', [.62 .955 .02 .035],...
    'String', CatigoryColumn,...
    'FontWeight', 'bold',...
    'Callback', {@edit_callback});

% Create a text label to say what the checkboxes do
uicontrol('Style', 'text',...
    'Units', 'normalized',...
    'Position', [.75 .955 .04 .035],...
    'String', 'Group with',...
    'FontWeight', 'bold');

% Create a text label to say what the checkboxes do
hCatiList=uicontrol('Style', 'listbox',...
    'Units', 'normalized',...
    'Position', [.8 .94 .1 .05],...
    'String', cellstr(Variables_all(1:CatigoryColumn,:)),...
    'FontWeight', 'bold',...
    'Callback', {@edit_callback});


% Subfuntions implementing the two callbacks
% ------------------------------------------

    function plot_callback(hObject, eventdata, row)
        % hObject     Handle to Plot menu
        % eventdata   Not used
        % column      Number of column to plot or clear
        
        HASgroupData=[];
        HATgroupData=[];
        normalized_group=[];
        ydata = get(htable, 'Data');
        
        colors = {'b','m','r','y','g','k'}; % Use consistent color for lines
        rownames = get(htable, 'RowName');
        rowname = rownames{row};
        if ~(get(hgroup,'Value'))
            if ~(get(hnormalize,'Value'))
            if get(hObject, 'Value')
                %         % Turn off the advisory text; it never comes back
                %         set(hprompt, 'Visible', 'off')
                % Obtain the data for that row
                str = get(hpopup, 'String');
                val = get(hpopup,'Value');
                ColumnName = str(val);
                columnInd=strmatch(ColumnName{1,1},char(Variables_all));
                set(haxes, 'NextPlot', 'Add')
                %              'YLim', [min(min(ydata(:,columnInd))) max(max(ydata(:,columnInd)))])
                % Draw the line plot for row
                if (row<=20) % HAS group
                    plot(haxes, ydata(row,columnInd),...
                        'DisplayName', rowname,...
                        'Color', colors{mod(row,6)+1});
                    legend('-DynamicLegend');
                else
                    plot(haxes, ydata(row,columnInd),...
                        'DisplayName', rowname,...
                        'Color', colors{mod(row,6)+1},...
                        'LineStyle',':');
                    legend('-DynamicLegend');
                end
            else % Adding a line to the plot
                % Find the lineseries object and delete it
                delete(findobj(haxes, 'DisplayName', rowname))
            end
            else
                    normalized_SubjData=NORMALIZE(rowname,ydata(row,:));
                    plot(normalized_SubjData,'DisplayName', rowname,...
                        'Color', colors{mod(row,6)+1});
                    set(haxes, 'NextPlot', 'Add');
            end
        else
             if ~(get(hnormalize,'Value'))
            % Obtain the data for that row
            delete(findobj(haxes, 'DisplayName', 'group average'));
            
            set(haxes, 'NextPlot', 'Add')
            ydata = get(htable, 'Data');
            str = get(hpopup, 'String');
            val = get(hpopup,'Value');
            ColumnName = str(val);
            columnInd=strmatch(ColumnName{1,1},char(Variables_all));
            % Draw the line plot for row
            for subInit=1:20
                checkval=get(hcheckBox(subInit),'Value');
                if checkval
                    HASgroupData=[HASgroupData; ydata(subInit,columnInd)];
                end
            end
            if (size(HASgroupData,1)>0)
                HASGroupMean=mean(HASgroupData,1);
                HASGroupSTDV=std(HASgroupData,1);
                %             set(haxes,'YLim',[min(min(ydata(:,columnInd))) max(max(ydata(:,columnInd)))]);
                %errorbar(HASGroupMean,HASGroupSTDV,'LineWidth',3,'DisplayName','group average');
                plot(HASGroupMean,'LineWidth',3,'DisplayName','group average')
                
                set(haxes, 'NextPlot', 'Add')
                legend('-DynamicLegend');
            end
            
            for subInit=22:41
                checkval=get(hcheckBox(subInit),'Value');
                if checkval
                    HATgroupData=[HATgroupData; ydata(subInit,columnInd)];
                end
            end
            if (size(HATgroupData,1)>0)
                HATGroupMean=mean(HATgroupData,1);
                HATGroupSTDV=std(HATgroupData,1);
                %             set(haxes,'YLim',[min(min(ydata(:,columnInd))) max(max(ydata(:,columnInd)))]);
                errorbar(HATGroupMean,HATGroupSTDV,'color','r','LineWidth',3,'DisplayName','group average');
                legend('-DynamicLegend');
            end
            
            
            if get(hObject, 'Value')
                %         % Turn off the advisory text; it never comes back
                %         set(hprompt, 'Visible', 'off')
                % Obtain the data for that row
                ydata = get(htable, 'Data');
                str = get(hpopup, 'String');
                val = get(hpopup,'Value');
                ColumnName = str(val);
                columnInd=strmatch(ColumnName{1,1},char(Variables_all));
                set(haxes, 'NextPlot', 'Add')
                %              'YLim', [min(min(ydata(:,columnInd))) max(max(ydata(:,columnInd)))])
                % Draw the line plot for row
                if (row<=20) % HAS group
                    plot(haxes, ydata(row,columnInd),...
                        'DisplayName', rowname,...
                        'Color', colors{mod(row,6)+1});
                    legend('-DynamicLegend');
                else
                    plot(haxes, ydata(row,columnInd),...
                        'DisplayName', rowname,...
                        'Color', colors{mod(row,6)+1},...
                        'LineStyle',':');
                    legend('-DynamicLegend');
                end
            else % Adding a line to the plot
                % Find the lineseries object and delete it
                delete(findobj(haxes, 'DisplayName', rowname))
            end
             else
            % Obtain the data for that row

            set(haxes, 'NextPlot', 'Replace');
            delete(findobj(haxes, 'DisplayName', 'group average'));
            
            ydata = get(htable, 'Data');
            str = get(hpopup, 'String');
            val = get(hpopup,'Value');
            ColumnName = str(val);
            columnInd=strmatch(ColumnName{1,1},char(Variables_all));
            % Draw the line plot for row
            for subInit=1:size(count,1)
                checkval=get(hcheckBox(subInit),'Value');
                if checkval
                        normalized_SubjData=NORMALIZE(rownames{subInit},ydata(subInit,:));
                        normalized_group=[normalized_group; normalized_SubjData];
                    plot(normalized_SubjData,'DisplayName', rownames{subInit},...
                        'Color', colors{mod(subInit,6)+1});
                    set(haxes, 'NextPlot', 'Add');
                end
            end
                normalized_mean=mean(normalized_group,1);
                plot(normalized_mean,'DisplayName', 'normalized average',...
                    'LineWidth',3,'Color','r');
                 
            end
             legend('-DynamicLegend');
                 
        end
        
    end


    function select_callback(hObject, eventdata, row)
        % hObject    Handle to uitable1 (see GCBO)
        % eventdata  Currently selected table indices
        % Callback to erase and replot markers, showing only those
        % corresponding to user-selected cells in table.
        % Repeatedly called while user drags across cells of the uitable
        
        % hmkrs are handles to lines having markers only
        set(hmkrs, 'Visible', 'off') % turn them off to begin
        
        % Get the list of currently selected table cells
        sel = eventdata.Indices;     % Get selection indices (row, col)
        % Noncontiguous selections are ok
        selcols = unique(sel(:,2));  % Get all selected data col IDs
        table = get(hObject,'Data'); % Get copy of uitable data
        
        % Get vectors of x,y values for each row in the selection;
        for idx = 1:numel(selcols)
            col = selcols(idx);
            xvals = sel(:,1);
            xvals(sel(:,2) ~= col) = [];
            yvals = table(xvals, col)';
            % Create Z-vals = 1 in order to plot markers above lines
            zvals = col*ones(size(xvals));
            % Plot markers for xvals and yvals using a line object
            set(hmkrs(col), 'Visible', 'on',...
                'XData', xvals,...
                'YData', yvals,...
                'ZData', zvals)
        end
    end

    function Replot_callback(hObject, eventdata, row)
        % hObject     Handle to Plot menu
        % eventdata   Not used
        % column      Number of column to plot or clear
        
        HASgroupData=[];
        HATgroupData=[];
        set(haxes, 'NextPlot', 'replace')
        colors = {'b','m','r','y','g','k'}; % Use consistent color for lines
        rownames = get(htable, 'RowName');
        %     if get(hObject, 'Value')
        %         % Turn off the advisory text; it never comes back
        %         set(hprompt, 'Visible', 'off')
        % Obtain the data for that row
        if ~(get(hgroup,'Value'))
            ydata = get(htable, 'Data');
            str = get(hpopup, 'String');
            val = get(hpopup,'Value');
            ColumnName = str(val);
            columnInd=strmatch(ColumnName{1,1},char(Variables_all));
            % Draw the line plot for row
            for subInit=1:45
                checkval=get(hcheckBox(subInit),'Value');
                if checkval
                    if (subInit<=20)
                        %                   set(haxes,'YLim',[min(min(ydata(:,columnInd))) max(max(ydata(:,columnInd)))]);
                        plot(haxes, ydata(subInit,columnInd),...
                            'DisplayName', rownames{subInit},...
                            'Color', colors{mod(subInit,6)+1});
                    else
                        plot(haxes, ydata(subInit,columnInd),...
                            'DisplayName', rownames{subInit},...
                            'Color', colors{mod(subInit,6)+1},...
                            'LineStyle',':');
                    end
                    
                    set(haxes, 'NextPlot', 'Add')
                end
            end
            legend('-DynamicLegend');
            
            %     else % Adding a line to the plot
            %         % Find the lineseries object and delete it
            %         delete(findobj(haxes, 'DisplayName', rowname))
        else
            ydata = get(htable, 'Data');
            str = get(hpopup, 'String');
            val = get(hpopup,'Value');
            ColumnName = str(val);
            columnInd=strmatch(ColumnName{1,1},char(Variables_all));
            % Draw the line plot for row
            for subInit=1:45
                checkval=get(hcheckBox(subInit),'Value');
                if checkval
                    if (subInit<=20)
                        %                   set(haxes,'YLim',[min(min(ydata(:,columnInd))) max(max(ydata(:,columnInd)))]);
                        plot(haxes, ydata(subInit,columnInd),...
                            'DisplayName', rownames{subInit},...
                            'Color', colors{mod(subInit,6)+1});
                    else
                        plot(haxes, ydata(subInit,columnInd),...
                            'DisplayName', rownames{subInit},...
                            'Color', colors{mod(subInit,6)+1},...
                            'LineStyle',':');
                    end
                    
                    set(haxes, 'NextPlot', 'Add')
                end
            end
            legend('-DynamicLegend');
            
            % Obtain the data for that row
            delete(findobj(haxes, 'DisplayName', 'group average'));
            
            set(haxes, 'NextPlot', 'Add')
            ydata = get(htable, 'Data');
            str = get(hpopup, 'String');
            val = get(hpopup,'Value');
            ColumnName = str(val);
            columnInd=strmatch(ColumnName{1,1},char(Variables_all));
            % Draw the line plot for row
            for subInit=1:20
                checkval=get(hcheckBox(subInit),'Value');
                if checkval
                    HASgroupData=[HASgroupData; ydata(subInit,columnInd)];
                end
            end
            if (size(HASgroupData,1)>0)
                HASGroupMean=mean(HASgroupData,1);
                HASGroupSTDV=std(HASgroupData,1);
                %             set(haxes,'YLim',[min(min(ydata(:,columnInd))) max(max(ydata(:,columnInd)))]);
                errorbar(HASGroupMean,HASGroupSTDV,'LineWidth',3,'DisplayName','group average');
                set(haxes, 'NextPlot', 'Add')
                legend('-DynamicLegend');
            end
            
            for subInit=22:41
                checkval=get(hcheckBox(subInit),'Value');
                if checkval
                    HATgroupData=[HATgroupData; ydata(subInit,columnInd)];
                end
            end
            if (size(HATgroupData,1)>0)
                HATGroupMean=mean(HATgroupData,1);
                HATGroupSTDV=std(HATgroupData,1);
                %             set(haxes,'YLim',[min(min(ydata(:,columnInd))) max(max(ydata(:,columnInd)))]);
                errorbar(HATGroupMean,HATGroupSTDV,'color','r','LineWidth',3,'DisplayName','group average');
                legend('-DynamicLegend');
            end
        end
    end
% end

    function Groupplot_callback(hObject, eventdata, row)
        % hObject     Handle to Plot menu
        % eventdata   Not used
        % column      Number of column to plot or clear
        
        HASgroupData=[];
        HATgroupData=[];
        set(haxes, 'NextPlot', 'replace')
        colors = {'b','m','r','y','g','k'}; % Use consistent color for lines
        rownames = get(htable, 'RowName');
        if get(hObject, 'Value')
            ydata = get(htable, 'Data');
            str = get(hpopup, 'String');
            val = get(hpopup,'Value');
            ColumnName = str(val);
            columnInd=strmatch(ColumnName{1,1},char(Variables_all));
            % Draw the line plot for row
            for subInit=1:45
                checkval=get(hcheckBox(subInit),'Value');
                if checkval
                    if (subInit<=20)
                        %                   set(haxes,'YLim',[min(min(ydata(:,columnInd))) max(max(ydata(:,columnInd)))]);
                        plot(haxes, ydata(subInit,columnInd),...
                            'DisplayName', rownames{subInit},...
                            'Color', colors{mod(subInit,6)+1});
                    else
                        plot(haxes, ydata(subInit,columnInd),...
                            'DisplayName', rownames{subInit},...
                            'Color', colors{mod(subInit,6)+1},...
                            'LineStyle',':');
                    end
                    
                    set(haxes, 'NextPlot', 'Add')
                end
            end
            legend('-DynamicLegend');
            
            % Obtain the data for that row
            ydata = get(htable, 'Data');
            str = get(hpopup, 'String');
            val = get(hpopup,'Value');
            ColumnName = str(val);
            columnInd=strmatch(ColumnName{1,1},char(Variables_all));
            % Draw the line plot for row
            for subInit=1:size(count,1)
                checkval=get(hcheckBox(subInit),'Value');
                if checkval
                    HASgroupData=[HASgroupData; ydata(subInit,columnInd)];
                end
            end
            if (size(HASgroupData,1)>0)
                HASGroupMean=mean(HASgroupData,1);
                HASGroupSTDV=std(HASgroupData,1);
                %             set(haxes,'YLim',[min(min(ydata(:,columnInd))) max(max(ydata(:,columnInd)))]);
                plot(HASGroupMean,'LineWidth',3,'DisplayName','group average');
                set(haxes, 'NextPlot', 'Add')
                legend('-DynamicLegend');
            end
            
            for subInit=22:41
                checkval=get(hcheckBox(subInit),'Value');
                if checkval
                    HATgroupData=[HATgroupData; ydata(subInit,columnInd)];
                end
            end
            if (size(HATgroupData,1)>0)
                HATGroupMean=mean(HATgroupData,1);
                HATGroupSTDV=std(HATgroupData,1);
                %             set(haxes,'YLim',[min(min(ydata(:,columnInd))) max(max(ydata(:,columnInd)))]);
                plot(HATGroupMean,'color','r','LineWidth',3,'DisplayName','group Average');
                legend('-DynamicLegend');
            end
        else
            ydata = get(htable, 'Data');
            str = get(hpopup, 'String');
            val = get(hpopup,'Value');
            ColumnName = str(val);
            columnInd=strmatch(ColumnName{1,1},char(Variables_all));
            % Draw the line plot for row
            for subInit=1:45
                checkval=get(hcheckBox(subInit),'Value');
                if checkval
                    if (subInit<=20)
                        %                   set(haxes,'YLim',[min(min(ydata(:,columnInd))) max(max(ydata(:,columnInd)))]);
                        plot(haxes, ydata(subInit,columnInd),...
                            'DisplayName', rownames{subInit},...
                            'Color', colors{mod(subInit,6)+1});
                    else
                        plot(haxes, ydata(subInit,columnInd),...
                            'DisplayName', rownames{subInit},...
                            'Color', colors{mod(subInit,6)+1},...
                            'LineStyle',':');
                    end
                    
                    set(haxes, 'NextPlot', 'Add')
                end
            end
            legend('-DynamicLegend');
        end
    end

    function Normalize_callback(hObject,eventdata)
        
        colors = {'b','m','r','y','g','k'}; % Use consistent color for lines
        
        normalized_group=[];
        set(haxes, 'NextPlot', 'Replace')
        groupval=get(hgroup,'Value');
        ydata = get(htable, 'Data');
        if get(hObject, 'Value')
            for subInit=1:size(count,1)
                checkval=get(hcheckBox(subInit),'Value');
                if checkval
                        normalized_SubjData=NORMALIZE(rownames{subInit},ydata(subInit,:));
                    if groupval
                        normalized_group=[normalized_group; normalized_SubjData];
                    end
                    plot(normalized_SubjData,'DisplayName', rownames{subInit},...
                        'Color', colors{mod(subInit,6)+1});
                    set(haxes, 'NextPlot', 'Add');
                end
            end
            legend('-DynamicLegend');
            if groupval
                normalized_mean=mean(normalized_group,1);
                plot(normalized_mean,'DisplayName', 'normalized average',...
                    'LineWidth',3,'Color','r');
            end
        else
            HASgroupData=[];
            HATgroupData=[];
            set(haxes, 'NextPlot', 'replace')
            colors = {'b','m','r','y','g','k'}; % Use consistent color for lines
            rownames = get(htable, 'RowName');
            %     if get(hObject, 'Value')
            %         % Turn off the advisory text; it never comes back
            %         set(hprompt, 'Visible', 'off')
            % Obtain the data for that row
            if ~(get(hgroup,'Value'))
                ydata = get(htable, 'Data');
                str = get(hpopup, 'String');
                val = get(hpopup,'Value');
                ColumnName = str(val);
                columnInd=strmatch(ColumnName{1,1},char(Variables_all));
                % Draw the line plot for row
                for subInit=1:45
                    checkval=get(hcheckBox(subInit),'Value');
                    if checkval
                        if (subInit<=20)
                            %                   set(haxes,'YLim',[min(min(ydata(:,columnInd))) max(max(ydata(:,columnInd)))]);
                            plot(haxes, ydata(subInit,columnInd),...
                                'DisplayName', rownames{subInit},...
                                'Color', colors{mod(subInit,6)+1});
                        else
                            plot(haxes, ydata(subInit,columnInd),...
                                'DisplayName', rownames{subInit},...
                                'Color', colors{mod(subInit,6)+1},...
                                'LineStyle',':');
                        end
                        
                        set(haxes, 'NextPlot', 'Add')
                    end
                end
                legend('-DynamicLegend');
                
                %     else % Adding a line to the plot
                %         % Find the lineseries object and delete it
                %         delete(findobj(haxes, 'DisplayName', rowname))
            else
                ydata = get(htable, 'Data');
                str = get(hpopup, 'String');
                val = get(hpopup,'Value');
                ColumnName = str(val);
                columnInd=strmatch(ColumnName{1,1},char(Variables_all));
                % Draw the line plot for row
                for subInit=1:45
                    checkval=get(hcheckBox(subInit),'Value');
                    if checkval
                        if (subInit<=20)
                            %                   set(haxes,'YLim',[min(min(ydata(:,columnInd))) max(max(ydata(:,columnInd)))]);
                            plot(haxes, ydata(subInit,columnInd),...
                                'DisplayName', rownames{subInit},...
                                'Color', colors{mod(subInit,6)+1});
                        else
                            plot(haxes, ydata(subInit,columnInd),...
                                'DisplayName', rownames{subInit},...
                                'Color', colors{mod(subInit,6)+1},...
                                'LineStyle',':');
                        end
                        
                        set(haxes, 'NextPlot', 'Add')
                    end
                end
                legend('-DynamicLegend');
                
                % Obtain the data for that row
                delete(findobj(haxes, 'DisplayName', 'group average'));
                delete(findobj(haxes, 'DisplayName', 'group average'));
                
                set(haxes, 'NextPlot', 'Add')
                ydata = get(htable, 'Data');
                str = get(hpopup, 'String');
                val = get(hpopup,'Value');
                ColumnName = str(val);
                columnInd=strmatch(ColumnName{1,1},char(Variables_all));
                % Draw the line plot for row
                for subInit=1:20
                    checkval=get(hcheckBox(subInit),'Value');
                    if checkval
                        HASgroupData=[HASgroupData; ydata(subInit,columnInd)];
                    end
                end
                if (size(HASgroupData,1)>0)
                    HASGroupMean=mean(HASgroupData,1);
                    HASGroupSTDV=std(HASgroupData,1);
                    %             set(haxes,'YLim',[min(min(ydata(:,columnInd))) max(max(ydata(:,columnInd)))]);
                    plot(HASGroupMean,'LineWidth',3,'DisplayName','group average');
                    set(haxes, 'NextPlot', 'Add')
                    legend('-DynamicLegend');
                end
                
                for subInit=22:41
                    checkval=get(hcheckBox(subInit),'Value');
                    if checkval
                        HATgroupData=[HATgroupData; ydata(subInit,columnInd)];
                    end
                end
                if (size(HATgroupData,1)>0)
                    HATGroupMean=mean(HATgroupData,1);
                    HATGroupSTDV=std(HATgroupData,1);
                    %             set(haxes,'YLim',[min(min(ydata(:,columnInd))) max(max(ydata(:,columnInd)))]);
                    plot(HATGroupMean,'color','r','LineWidth',3,'DisplayName','group average');
                    legend('-DynamicLegend');
                end
            end
        end
    end

    function edit_callback(hObject, eventdata)
        CatigoryColumn=str2num(get(hObject,'String'));
        set(hCatiList, 'String', cellstr(Variables_all(1:CatigoryColumn,:)));
        Variables=cellstr(Variables_all(CatigoryColumn+1:end,1:4));
        Var_list=unique(Variables,'first');
        set(hpopup,'String',Var_list);
    end


    function selectall_callback(hObject, eventdata)
        if (get(hObject,'Value'))
            for CheckBox=1:45
                set(hcheckBox(CheckBox),'Value',1);
                plot_callback(hcheckBox(CheckBox),1,CheckBox);
            end
        else
            for CheckBox=1:45
                set(hcheckBox(CheckBox),'Value',0);
                plot_callback(hcheckBox(CheckBox),1,CheckBox);
            end
        end
        
    end
end
