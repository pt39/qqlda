%alter	must alter all the cycleddata locations 

function kinematic_analysis
% clear all 
% close all

global  CycledData CycledHeader ColumnRangeArray DevType NumMarkers Data DataVel ... 
        CurrentTrials CurrentTrajectoryMarkers CurrentTimepoint CurrentTangVelMarker CurrentXYZ CurrentM1 CurrentM2 PeriodsChecked AlignBy...
        CurrentKinMarkers
        %The above variables are global variables. It is imperative that you keep track of the global variables throughout the program 

        
%         cd('E:\GF Thesis\RTG\BJHAS\baseline\right\')
%cd('Z:\Hamid\Kinematics\Adamovich1\baseline_left_bigcircle_optotrackonly\BP\09_02_2009');
CurrentPath = uigetdir;
cd(CurrentPath)     %sets the directory to the CurrentPath
ScreenSize = get(0,'ScreenSize');
fig=figure('name',CurrentPath,'Position',[1 ScreenSize(4)/20 ScreenSize(3) 17*ScreenSize(4)/20]); %initializes the main figure of the GUI
set(zoom,'Motion','horizontal'); %In the figure GUI, you are now only allowed to zoom in a horizontal direction 
set(pan,'Motion','horizontal'); %In the figure GUI, you are now only allowed to pan in a horizontal direction
BackgroundColor = [.3 .3 .3]; set(gcf,'Color',BackgroundColor);
NumberOfOnsets = 4; BarColors = {'g','w','y','c'}; %The number of possible onsets and offsets you can have. The Bar colors will be displayed on the individual plots only when ONE trial is selected 
initialize = -999999; %All dataslots in the 'Cycled' file not currently being used are initialized to -999999
numSTD = 1; %When the 'average checkbox' is selected, the average of the trials (rather than all the trials) is plotted in all the subplots along with the +/- standard deviation range that is specified in this line of code 

s1=subplot('position',[.05 .27 .35 .7],'XColor','w','YColor','w','ZColor','w','Color',BackgroundColor,'NextPlot','replacechildren'); %% Trajectory Plot
s2=subplot('position',[.43 .27 .45 .6],'XColor','w','YColor','w','ZColor','w','Color',BackgroundColor,'NextPlot','replacechildren'); %% Tangential Position velocity
s3=subplot('position',[.9 .43 .05 .05],'XColor','w','YColor','w','ZColor','w','Color',BackgroundColor,'NextPlot','replacechildren'); %% Lateral/saggatial/vertical velocity of selected Marker     
s4=subplot('position',[.9 .43 .05 .05],'XColor','w','YColor','w','ZColor','w','Color',BackgroundColor,'NextPlot','replacechildren'); %% Aperture of 2 selected Markers
s5=subplot('position',[.9 .43 .05 .05],'XColor','w','YColor','w','ZColor','w','Color',BackgroundColor,'NextPlot','replacechildren'); %% Aperture velocity of selected 2 Markers
%NOTE, 'replacechildren' (see the above 5 lines of code) is a great property because it only replaces the plots within the figure, rather than completely re-setting the figure properties everytime you plot 
xlabel(s1,'X');       ylabel(s1,'Y');              zlabel(s1,'Z');
xlabel(s2,'Timeseries');    ylabel(s2,'Tangential Velocity');   
xlabel(s3,'Timeseries');    ylabel(s3,'X/Y/Z Velocity');
xlabel(s4,'Timeseries');    ylabel(s4,'Aperture');
xlabel(s5,'Timeseries');    ylabel(s5,'Aperture Velocity');

PlottingAndCyclingPanel = uipanel('Parent',fig,'Title','Plotting and Cycling','BackgroundColor',BackgroundColor,'ForegroundColor',[1.0 1.0 1.0],'HighlightColor',[1.0 1.0 1.0],'Units','normalized','Position',[0.01 0.005 .45 .215]);

%Trial Selection Control Panel 
TrialSelection = uipanel('Parent',PlottingAndCyclingPanel,'Title','Trial Selection','BackgroundColor',BackgroundColor,'ForegroundColor',[1.0 1.0 1.0],'HighlightColor',[1.0 1.0 1.0],'Units','normalized','Position',[0.01 0.01 .18 .99]);
SelectTrialsTable = uicontrol(TrialSelection,'Visible','off','style','listbox','Min',0,'Max',100,'Units','normalized','Position',[.02 .11 .98 .87],'Callback', {@SelectTrialsTableCallback});                         
AverageCheckbox = uicontrol(TrialSelection,'Visible','off','Style','checkbox','Units','normalized','Position',[.01 .01 .08 .08],'Callback',@AverageCheckboxCallback);
AverageTextBox = uicontrol(TrialSelection,'Visible','off','style','text','string','Average Trials','BackgroundColor',BackgroundColor,'ForegroundColor',[1.0 1.0 1.0],'Units','normalized','Position',[.1 0 .5 .1]);

%Sample Control Panel
SampleScroll = uipanel('Parent',PlottingAndCyclingPanel,'Visible','off','Title','Sample Scroll','BackgroundColor',BackgroundColor,'ForegroundColor',[1.0 1.0 1.0],'HighlightColor',[1.0 1.0 1.0],'Units','normalized','Position',[0.21 0.68 .78 .32]);
TimepointTextBox = uicontrol(SampleScroll,'BackgroundColor',BackgroundColor,'ForegroundColor',[1.0 1.0 1.0],'Units','normalized','Position',[.15 .07 .7 .42]);
TimepointMinTextBox = uicontrol(SampleScroll,'style','text','BackgroundColor',BackgroundColor,'ForegroundColor',[1.0 1.0 1.0],'Units','normalized','Position',[.01 .17 .05 .25]);
TimepointMaxTextBox = uicontrol(SampleScroll,'style','text','BackgroundColor',BackgroundColor,'ForegroundColor',[1.0 1.0 1.0],'Units','normalized','Position',[.94 .17 .05 .25]);
TimepointSlider = uicontrol(SampleScroll,'Style','slider','Units','normalized','Position',[.02 .55 .96 .43],'Callback',@TimepointSliderCallback);

%Cycling Control Panel
CyclingControlPanel = uipanel('Parent',PlottingAndCyclingPanel,'Visible','off','Title','Cycling Control','BackgroundColor',BackgroundColor,'ForegroundColor',[1.0 1.0 1.0],'HighlightColor',[1.0 1.0 1.0],'Units','normalized','Position',[.73 0.03 .26 .64]);
AlignmentDropdown = uicontrol(CyclingControlPanel,'Style','popupmenu','Enable','on','String',{' Align by Onset1';' Align by Offset1';' Align by Onset2';' Align by Offset2';' Align by Onset3';' Align by Offset3';' Align by Onset4';' Align by Offset4'},'Units','normalized','Position',[.02 .87 .8 .12],'Callback',@AlignmentDropdownCallback);      
ManualOnsetButton1 = uicontrol(CyclingControlPanel,'style','pushbutton','string','Set Onset1','Units','normalized','position',[.02 .56 .42 .16],'Callback',@ManualOnset1ButtonCallback);
ManualOffsetButton1 = uicontrol(CyclingControlPanel,'style','pushbutton','string','Set Offset1','Units','normalized','position',[.46 .56 .42 .16],'Callback',@ManualOffset1ButtonCallback);
ManualCheckbox1 = uicontrol(CyclingControlPanel,'Style','checkbox','Value',0,'Units','normalized','Position',[.9 .56 .09 .16],'Callback',@ManualCheckboxCallback);
ManualOnsetButton2 = uicontrol(CyclingControlPanel,'style','pushbutton','string','Set Onset2','Units','normalized','position',[.02 .38 .42 .16],'Callback',@ManualOnset2ButtonCallback);
ManualOffsetButton2 = uicontrol(CyclingControlPanel,'style','pushbutton','string','Set Offset2','Units','normalized','position',[.46 .38 .42 .16],'Callback',@ManualOffset2ButtonCallback);
ManualCheckbox2 = uicontrol(CyclingControlPanel,'Style','checkbox','Value',0,'Units','normalized','Position',[.9 .38 .09 .16],'Callback',@ManualCheckboxCallback);
ManualOnsetButton3 = uicontrol(CyclingControlPanel,'style','pushbutton','string','Set Onset3','Units','normalized','position',[.02 .19 .42 .16],'Callback',@ManualOnset3ButtonCallback);
ManualOffsetButton3 = uicontrol(CyclingControlPanel,'style','pushbutton','string','Set Offset3','Units','normalized','position',[.46 .19 .42 .16],'Callback',@ManualOffset3ButtonCallback);
ManualCheckbox3 = uicontrol(CyclingControlPanel,'Style','checkbox','Value',0,'Units','normalized','Position',[.9 .19 .09 .16],'Callback',@ManualCheckboxCallback);
ManualOnsetButton4 = uicontrol(CyclingControlPanel,'style','pushbutton','string','Set Onset4','Units','normalized','position',[.02 .01 .42 .16],'Callback',@ManualOnset4ButtonCallback);
ManualOffsetButton4 = uicontrol(CyclingControlPanel,'style','pushbutton','string','Set Offset4','Units','normalized','position',[.46 .01 .42 .16],'Callback',@ManualOffset4ButtonCallback);
ManualCheckbox4 = uicontrol(CyclingControlPanel,'Style','checkbox','Value',0,'Units','normalized','Position',[.9 .01 .09 .16],'Callback',@ManualCheckboxCallback);
TrajectoryClipping = uicontrol(CyclingControlPanel,'style','text','FontSize',7,'string',['Traj';'Clip'],'BackgroundColor',BackgroundColor,'ForegroundColor',[1.0 1.0 1.0],'Units','normalized','Position',[.87 .78 .13 .22]);

%Plot Control Panel
PlotControlPanel = uipanel('Parent',PlottingAndCyclingPanel,'Visible','off','Title','Plot Control','BackgroundColor',BackgroundColor,'ForegroundColor',[1.0 1.0 1.0],'HighlightColor',[1.0 1.0 1.0],'Units','normalized','Position',[0.21 .03 .51 .64]);
TrajectoryControlPanel = uipanel('Parent',PlotControlPanel,'Title','Trajectory','BackgroundColor',BackgroundColor,'ForegroundColor',[1.0 1.0 1.0],'HighlightColor',[1.0 1.0 1.0],'Units','normalized','Position',[0.01 0.01 .26 .98]);
TrajectoryMarker = uicontrol(TrajectoryControlPanel,'style','listbox','Min',0,'Max',100,'Units','normalized','Position',[.01 .01 .90 .95],'Callback', {@TrajectoryMarkerCallback});
TangentialVelocityControlPanel = uipanel('Parent',PlotControlPanel,'Title','Tangential Vel','BackgroundColor',BackgroundColor,'ForegroundColor',[1.0 1.0 1.0],'HighlightColor',[1.0 1.0 1.0],'Units','normalized','Position',[0.28 0.50 .35 .45]);
TangentialVelocityDropdown = uicontrol(TangentialVelocityControlPanel,'Style','popupmenu','Enable','on','Units','normalized','Position',[.05 .1 .9 .80],'Callback',@TangentialVelocityDropdownCallback);
XYZvelocityControlPanel = uipanel('Parent',PlotControlPanel,'Title','X/Y/Z Vel','BackgroundColor',BackgroundColor,'ForegroundColor',[1.0 1.0 1.0],'HighlightColor',[1.0 1.0 1.0],'Units','normalized','Position',[0.64 0.50 .35 .45]);
XYZvelocityDropdown = uicontrol(XYZvelocityControlPanel,'Style','popupmenu','Enable','on','String',{'X Velocity';'Y Velocity';'Z Velocity'},'Units','normalized','Position',[.05 .1 .90 .80],'Callback',@XYZvelocityDropdownCallback);      
ApertureControlPanel = uipanel('Parent',PlotControlPanel,'Title','Aperture','BackgroundColor',BackgroundColor,'ForegroundColor',[1.0 1.0 1.0],'HighlightColor',[1.0 1.0 1.0],'Units','normalized','Position',[0.28 0.02 .71 .45]);
ApertureM1 = uicontrol(ApertureControlPanel,'Style','popupmenu','Enable','on','Units','normalized','Position',[.02 .1 .45 .80],'Callback',@ApertureM1Callback); 
ApertureM2 = uicontrol(ApertureControlPanel,'Style','popupmenu','Enable','on','Units','normalized','Position',[.50 .1 .45 .80],'Callback',@ApertureM2Callback); 

%Kinematics Control Panel
KinematicDataPanel = uipanel('Parent',fig,'Visible','off','Title','Kinematic Data','BackgroundColor',BackgroundColor,'ForegroundColor',[1.0 1.0 1.0],'HighlightColor',[1.0 1.0 1.0],'Units','normalized','Position',[0.465 0.005 .53 .215]); 
KinematicsTable = uitable(KinematicDataPanel,'ColumnWidth',{50},'Units','normalized','Position',[.12 .01 .87 .99],'CellSelectionCallback', {@KinematicsTableCallback});
KinSave = uicontrol(KinematicDataPanel,'style','pushbutton','string','Save Kinematics','Units','normalized','position',[.13 .02 .12 .12],'Callback',@KinSaveCallback);
KinMarkers = uicontrol(KinematicDataPanel,'style','listbox','Min',0,'Max',100,'Units','normalized','Position',[.01 .01 .1 .99],'Callback', {@KinMarkersCallback});

set(fig,'userdata',[s1 s2 s3 s4 s5 PlottingAndCyclingPanel TrialSelection SelectTrialsTable SampleScroll PlotControlPanel KinSave KinMarkers  ... 
    TrajectoryControlPanel CyclingControlPanel AlignmentDropdown... 
    ManualOnsetButton1 ManualOffsetButton1 ManualCheckbox1 ManualOnsetButton2 ManualOffsetButton2 ManualCheckbox2 ... 
    ManualOnsetButton3 ManualOffsetButton3 ManualCheckbox3 ManualOnsetButton4 ManualOffsetButton4 ManualCheckbox4 TrajectoryClipping ... 
    TangentialVelocityControlPanel XYZvelocityControlPanel ApertureControlPanel TangentialVelocityDropdown XYZvelocityDropdown ApertureM1 ApertureM2 ... 
    TrajectoryMarker KinematicDataPanel KinematicsTable])

GoButtonCallback;

function GoButtonCallback(source, eventdata)
    delete(get(s1,'Children'),get(s2,'Children'),get(s3,'Children'),get(s4,'Children'),get(s5,'Children'))
    ProgramDirectory = dir(CurrentPath);
    NumFiles = 0;       %will represent the number of data files in the current directory 
    CycledCheck=0;      %will be set to 1 if the data has already been cycled 
    for(i=1:numel(ProgramDirectory))
        if(~ProgramDirectory(i).isdir)
            if(strcmp(ProgramDirectory(i).name,'Cycled.xls') || strcmp(ProgramDirectory(i).name,'Format.xls'))
                CycledCheck=1; %set to 1 if the data has already been cycled...meaning there is a cycled file within the directory 
            else 
                NumFiles = NumFiles+1;            
                FileNames{NumFiles}=ProgramDirectory(i).name;
            end
        end
    end
    
    if(CycledCheck==0) %perform the follwing if the data has not been cycled already
        FileType = '';
        for(i=1:NumFiles) 
            CurrentFileName=char(FileNames(i));
            CheckFileType = FileNames{i}(end-2:end);
            if(strcmp(CheckFileType,'txt'))
                d=load(char(strcat(strcat(CurrentPath, '\'),FileNames(i))));
            elseif(strcmp(CheckFileType,'xls'))
                d=xlsread(char(strcat(strcat(CurrentPath, '\'),FileNames(i))));
            end
            if(i==1 || ~strcmp(FileType,CurrentFileName(1,1:size(FileType,2))))
                Confirmation = 0;
                while(Confirmation==0)
                    clear FileType DeviceType ColumnRange NumMarkers 
                    TraverseFileName=1;
                    while(TraverseFileName <= size(CurrentFileName,2))
                        if(isletter(CurrentFileName(TraverseFileName)))
                            FileType(TraverseFileName)=CurrentFileName(TraverseFileName);
                            TraverseFileName = TraverseFileName+1;
                        else
                            TraverseFileName = size(CurrentFileName,2)+1;
                        end
                    end
                    DeviceTypeList = {'1D Device';'3D Device'};
                    [Selection1,ok1] = listdlg('SelectionMode','single','Name','Device Type','ListSize',[250 50],'PromptString',['What type of device is ' FileType '?'],'ListString',DeviceTypeList);
                    DeviceType = DeviceTypeList{Selection1,1};
                    ColumnRange = inputdlg(['Enter the columns in which "' FileType '" data exists. For example, enter   1:3,7:9   for a 3D device with 2 markers)'],'Datafile Properties',1);
                    ColumnLength = size(str2num(ColumnRange{1}),2);
                    if(strcmp(DeviceType,'3D Device'))
                        NumMarkers = ColumnLength/3;
                    elseif(strcmp(DeviceType,'1D Device'))
                        NumMarkers = ColumnLength;
                    end
                    options.Resize='on'; options.WindowStyle='normal'; options.Interpreter='tex';
                    FilterProperties = inputdlg({   'If you would like to filter the data, complete the following. Otherwise, click Cancel. Enter the SamplingFreq: '; ...
                                                    'Enter the Order of the filter: ';'Enter the FilterType (enter 0 for bandstop/bandreject, 1 for bandpass, 2 for lowpass, 3 for highpass): '; ... 
                                                    'Enter the first cutoff frequency: ';'Enter the second cutoff frequency (leave blank for lowpass or highpass filters): '},'Filter Properties');
                    AutocycleList = {'5% Velocity'};
                    [SelectCycle,okCycle] = listdlg('SelectionMode','single','Name','Autocycle List','ListSize',[250 50],'PromptString',['Do you want to autocycle?'],'ListString',AutocycleList);
                    if(~isempty(SelectCycle))
                        Autocycle = AutocycleList{SelectCycle,1};
                        if(strcmp(Autocycle,'5% Velocity'))
                            AutocycleColumnInput = inputdlg('Which column of data should be referenced for cycling?','Autoycling Column');
                            AutocycleColumn = str2num(AutocycleColumnInput{1});
                        end
                    end
                    
                    if(NumMarkers == floor(NumMarkers))
                        ConfirmationQuestion = questdlg(['Just to confirm: Files entitled "' FileType '" contain ' num2str(ColumnLength) ' column(s) of data representing ' num2str(NumMarkers) ' marker(s) from a ' DeviceType '.'],'Confirmation','Correct','Wrong','Correct');
                        if(strcmp(ConfirmationQuestion,'Correct'))
                            Confirmation=1;
                        end
                    else
                        questdlg('There is a discrepancy between the type of device and the number of columns of data of information. You must re-enter the information','Confirmation','OK','OK')
                    end
                end
            end

            CycledMatrix{i,1} = char(FileNames(i));
            CycledMatrix{i,2} = [DeviceType(1,1:2) '_' ColumnRange{1}];
            if(isempty(FilterProperties))
                CycledMatrix{i,3} = 'None';
            else
                CycledMatrix{i,3} = [FilterProperties{1,1},',',FilterProperties{2,1},',',FilterProperties{3,1},',',FilterProperties{4,1},',',FilterProperties{5,1}];
            end
            CycledMatrix{i,4}=1;
            CycledMatrix{i,5}=size(d,1);
            for(init=6:11)
                CycledMatrix{i,init}=initialize;  
            end
            
            if(strcmp(Autocycle,'5% Velocity'))
                FilterDescription = CycledMatrix{i,3};
                if(strcmp(FilterDescription,'None'))
                    Data=d(:,AutocycleColumn);
                else
                    FilterParameters = str2num(FilterDescription);
                    SamplingFreq = FilterParameters(1,1); FilterOrder = FilterParameters(1,2); FilterType = FilterParameters(1,3); 
                    if(FilterType==0)
                        CutoffFreq = FilterParameters(1,4:5); [bFilt,aFilt] = butter(FilterOrder,CutoffFreq/(SamplingFreq/2),'stop');
                    elseif(FilterType==1)
                        CutoffFreq = FilterParameters(1,4:5); [bFilt,aFilt] = butter(FilterOrder,CutoffFreq/(SamplingFreq/2),'band');
                    elseif(FilterType==2)
                        CutoffFreq = FilterParameters(1,4); [bFilt,aFilt] = butter(FilterOrder,CutoffFreq/(SamplingFreq/2),'low');
                    elseif(FilterType==3)
                        CutoffFreq = FilterParameters(1,4); [bFilt,aFilt] = butter(FilterOrder,CutoffFreq/(SamplingFreq/2),'high');
                    end
                    Data=filtfilt(bFilt,aFilt,d(:,AutocycleColumn)); 
                end

                %getting the velocity/derivative matrix...NOTE, time is not factored into the equation...the values are just the difference values 
                %NOTE, the last vel value is repeated in order to keep Data and DataVel the same sizes 
                for(k=1:size(Data,1)-1)
                    DataVel(k)=Data(k+1,1)-Data(k,1);
                    temp=k;
                end
                DataVel(temp+1)=DataVel(temp);
                
                [maxData maxDataTime] = max(Data);
                [maxVel1 maxVelTime1] = max(abs(DataVel(1:maxDataTime)));
                [maxVel2 maxVelTime2] = max(abs(DataVel(maxDataTime:end)));
                maxVelTime2 = maxVelTime2+maxDataTime-1;
                
                maxVel = max([maxVel1 maxVel2]);
                DataMarker1 = 3;
                if(DataMarker1<maxVelTime1)
                    while((DataVel(DataMarker1)<0.05*maxVel || DataVel(DataMarker1-1)<0.05*maxVel || DataVel(DataMarker1-2)<0.05*maxVel) && DataMarker1<maxVelTime1)
                        DataMarker1 = DataMarker1+1;
                    end
                end
                CycledMatrix{i,4}=DataMarker1;
                DataMarker2 = maxVelTime2+3;
                if(DataMarker2<length(DataVel)-1)
                    while((abs(DataVel(DataMarker2))>0.05*maxVel || abs(DataVel(DataMarker2-1))>0.05*maxVel || abs(DataVel(DataMarker2-2))>0.05*maxVel) && DataMarker2<length(DataVel)-1)
                        DataMarker2 = DataMarker2+1;
                    end
                end
                %{
                DataMarker2 = length(DataVel)-1;
                if(DataMarker2>maxVelTime2)
                    while(abs(DataVel(DataMarker2))<0.05*maxVel && abs(DataVel(DataMarker2-1))<0.05*maxVel && abs(DataVel(DataMarker2-2))<0.05*maxVel && abs(DataVel(DataMarker2-3))<0.05*maxVel && abs(DataVel(DataMarker2-4))<0.05*maxVel && DataMarker2>maxVelTime2)
                        DataMarker2 = DataMarker2-1;
                    end
                end
                %}
                CycledMatrix{i,5}=DataMarker2;
            end
        end
        xlswrite(strcat(CurrentPath,'\Cycled.xls'), CycledMatrix);
        clear d velTemp CycleQuestion CycledMatrix
        GoButtonCallback 
    else %perform if cycled already %Essentially the primary code 
        %Reading Cycled.xls
        clear CycledData CycledHeader; [CycledData CycledHeader]=xlsread(strcat(CurrentPath,'\Cycled.xls'));        
        NumTrials = size(CycledData,1);
        
        %loading all the data for this trial condition 
        clear Data
        for(i=1:NumTrials)
            FileName = CycledHeader{i,1};
            %%%%%AppropriateColumns = str2num(CycledHeader{i,2}(4:end));
            CheckFileType = FileName(end-2:end);
            if(strcmp(CheckFileType,'txt'))
                d=load([CurrentPath '\' FileName]);
            elseif(strcmp(CheckFileType,'xls'))
                d=xlsread([CurrentPath '\' FileName]);
            end
            %%%%%d = d(:,AppropriateColumns);
            FilterDescription = CycledHeader{i,3};
            if(strcmp(FilterDescription,'None'))
                Data{i}=d;
            else
                FilterParameters = str2num(FilterDescription);
                SamplingFreq = FilterParameters(1,1); FilterOrder = FilterParameters(1,2); FilterType = FilterParameters(1,3); 
                if(FilterType==0)
                    CutoffFreq = FilterParameters(1,4:5); [bFilt,aFilt] = butter(FilterOrder,CutoffFreq/(SamplingFreq/2),'stop');
                elseif(FilterType==1)
                    CutoffFreq = FilterParameters(1,4:5); [bFilt,aFilt] = butter(FilterOrder,CutoffFreq/(SamplingFreq/2),'band');
                elseif(FilterType==2)
                    CutoffFreq = FilterParameters(1,4); [bFilt,aFilt] = butter(FilterOrder,CutoffFreq/(SamplingFreq/2),'low');
                elseif(FilterType==3)
                    CutoffFreq = FilterParameters(1,4); [bFilt,aFilt] = butter(FilterOrder,CutoffFreq/(SamplingFreq/2),'high');
                end
                Data{i}=filtfilt(bFilt,aFilt,d);
            end
        end
        
        %getting the velocity/derivative matrix...NOTE, time is not factored into the equation...the values are just the difference values 
        %NOTE, the last vel value is repeated in order to keep Data and DataVel the same sizes 
        clear DataVel
        for(i=1:size(Data,2))
            for(j=1:size(Data{i},2))
                for(k=1:size(Data{i},1)-1)
                    DataVel{i}(k,j)=Data{i}(k+1,j)-Data{i}(k,j);
                    temp=k;
                end
                DataVel{i}(temp+1,j)=DataVel{i}(temp,j);
            end
        end
        
        %Sets default values
        AlignBy = 1;
        PeriodsChecked = [0 0 0 0];
        CurrentTimepoint=0; set(TimepointSlider,'Value',CurrentTimepoint);
        CurrentTrajectoryMarkers = 1; set(TrajectoryMarker,'Value',CurrentTrajectoryMarkers);
        CurrentTangVelMarker = 1; set(TangentialVelocityDropdown,'Value',CurrentTangVelMarker);
        CurrentXYZ = 1; set(XYZvelocityDropdown,'Value',CurrentXYZ);
        CurrentM1 = 1; set(ApertureM1,'Value',CurrentM1);
        CurrentM2 = 1; set(ApertureM2,'Value',CurrentM2);
        CurrentKinMarkers = 1; set(KinMarkers,'Value',CurrentKinMarkers);

        %Select Trials Table
        set(SelectTrialsTable,'Visible','on','String',CycledHeader(:,1))
    end
end

function SelectTrialsTableCallback(source, eventdata)
    clear CurrentTrials; CurrentTrials = get(source,'Value')';

    ManualCheckboxConfirm

    if(CycledData(CurrentTrials(:,1),AlignBy)==initialize)
        msgbox('NOTE, ''Align By'' has been re-set to Onset #1 because not all selected trials have been cycled properly yet')
        AlignBy=1;
        set(AlignmentDropdown,'Value',AlignBy) 
    end
    
    if(length(CurrentTrials)==1 || isequal(CycledHeader{CurrentTrials,2}))
        clear ColumnRangeArray; ColumnRangeArray = str2num(CycledHeader{CurrentTrials(1,1),2}(1,4:end));
        clear  DevType; DevType = str2num(CycledHeader{CurrentTrials(1,1),2}(1,1));
        if(DevType == 1)
            clear NumMarkers; NumMarkers = size(ColumnRangeArray,2);
        elseif(DevType == 3)
            clear NumMarkers; NumMarkers = size(ColumnRangeArray,2)/3;
        end
        for(i = 1:NumMarkers) 
            MarkerCount{i,1} = strcat('Marker #',num2str(i)); 
        end
        
        %set default values 
        clear CurrentTimepoint; CurrentTimepoint=0;
        
        set(SampleScroll,'Visible','on');
        set(TimepointTextBox,'string',['Align: ' num2str(CurrentTimepoint)]);
        set(TimepointMinTextBox,'string',num2str(-1*size(Data{1},1)));
        set(TimepointMaxTextBox,'string',num2str(size(Data{1},1)));
        set(TimepointSlider,'Max',size(Data{1},1),'Min',-1*size(Data{1},1),'Value',CurrentTimepoint,'SliderStep',[1/(2*size(Data{1},1)) 1/(2*size(Data{1},1))]);
        
        set(TrajectoryMarker,'String',MarkerCount);
        set(TangentialVelocityDropdown,'String',MarkerCount);
        set(ApertureM1,'String',MarkerCount);
        set(ApertureM2,'String',MarkerCount);
        
        % Plot Contol Panel
        set(PlotControlPanel,'Visible','on') 
        
        %Cycling Control Panel
        set(CyclingControlPanel,'Visible','on')

        %Kinematics Control 
        set(KinematicDataPanel,'Visible','on')
        set(KinMarkers,'String',MarkerCount)
        
        %Average Trials Checkbox 
        set(AverageTextBox,'Visible','on') 
        set(AverageCheckbox,'Visible','on') 
        
        plotAll
    else
        msgbox('All selected trials must have the same format')

        %set default values 
        set(PlotControlPanel,'Visible','off') 
        clear CurrentTrajectoryMarkers; CurrentTrajectoryMarkers = 1; set(TrajectoryMarker,'Value',CurrentTrajectoryMarkers);
        clear CurrentTangVelMarker; CurrentTangVelMarker = 1; set(TangentialVelocityDropdown,'Value',CurrentTangVelMarker);
        clear CurrentXYZ; CurrentXYZ = 1; set(XYZvelocityDropdown,'Value',CurrentXYZ);
        clear CurrentM1; CurrentM1 = 1; set(ApertureM1,'Value',CurrentM1);
        clear CurrentM2; CurrentM2 = 1; set(ApertureM2,'Value',CurrentM2);
        
        set(CyclingControlPanel,'Visible','off')
        
        set(KinematicDataPanel,'Visible','off')
        clear CurrentKinMarkers; CurrentKinMarkers = 1; set(KinMarkers,'Value',CurrentKinMarkers);
    end
end

function TimepointSliderCallback(source, eventdata)
    clear CurrentTimepoint; CurrentTimepoint = int16(get(source,'Value'));
    plotTimepoint
end

function ManualTimepointCallback(source,eventdata)
    tempCurrentTimepoint=get(gca,'CurrentPoint');
    clear CurrentTimepoint; CurrentTimepoint = int16(tempCurrentTimepoint(1,1));
    plotTimepoint 
end

function MoveBarCallback(source, eventdata)
    [index value]=ginput(1);
    clear CurrentTimepoint; CurrentTimepoint = int16(index);
    plotTimepoint
end

function plotTimepoint
    set(TimepointTextBox,'string',['Align: ' num2str(CurrentTimepoint) '        Sample: ' num2str(CurrentTimepoint+int16(CycledData(CurrentTrials,AlignBy)'))]);
    set(TimepointSlider,'Value',CurrentTimepoint)
    delete(findobj(s1, 'Tag','VerticalLine')); 
    for(i=1:size(CurrentTrials,1))
        for(j=1:size(CurrentTrajectoryMarkers,1))
            if(DevType==1)                
                plot(s1,[CurrentTimepoint CurrentTimepoint],[min(get(s1,'YTick')) max(get(s1,'YTick'))],'r','Tag','VerticalLine','ButtonDownFcn',@MoveBarCallback)
            elseif(DevType==3)
                plot3(s1,   Data{CurrentTrials(i,1)}(CycledData(CurrentTrials(i,1),AlignBy)+CurrentTimepoint,ColumnRangeArray(1,DevType*CurrentTrajectoryMarkers(j,1)-2)), ... 
                            Data{CurrentTrials(i,1)}(CycledData(CurrentTrials(i,1),AlignBy)+CurrentTimepoint,ColumnRangeArray(1,DevType*CurrentTrajectoryMarkers(j,1)-1)), ...
                            Data{CurrentTrials(i,1)}(CycledData(CurrentTrials(i,1),AlignBy)+CurrentTimepoint,ColumnRangeArray(1,DevType*CurrentTrajectoryMarkers(j,1)-0)), ...
                            'o','MarkerSize',10,'MarkerEdgeColor','k','MarkerFaceColor','r','Tag','VerticalLine'); axis(s1,'equal')
            end
        end
    end
    
    delete(findobj(s2, 'Tag','VerticalLine')); 
    plot(s2,[CurrentTimepoint CurrentTimepoint],[min(get(s2,'YTick')) max(get(s2,'YTick'))],'r','Tag','VerticalLine','ButtonDownFcn',@MoveBarCallback)
    delete(findobj(s3, 'Tag','VerticalLine')); 
    plot(s3,[CurrentTimepoint CurrentTimepoint],[min(get(s3,'YTick')) max(get(s3,'YTick'))],'r','Tag','VerticalLine','ButtonDownFcn',@MoveBarCallback)
    delete(findobj(s4, 'Tag','VerticalLine')); 
    plot(s4,[CurrentTimepoint CurrentTimepoint],[min(get(s4,'YTick')) max(get(s4,'YTick'))],'r','Tag','VerticalLine','ButtonDownFcn',@MoveBarCallback)
    delete(findobj(s5, 'Tag','VerticalLine')); 
    plot(s5,[CurrentTimepoint CurrentTimepoint],[min(get(s5,'YTick')) max(get(s5,'YTick'))],'r','Tag','VerticalLine','ButtonDownFcn',@MoveBarCallback)
end

function AverageCheckboxCallback(source, eventdata)
    %AverageCheckboxChecked = get(AverageCheckbox,'Value');
    if(~isequal(PeriodsChecked,[0 0 0 0]))
        clear PeriodsChecked; PeriodsChecked=[0 0 0 0]; 
        set(ManualCheckbox1,'Value',PeriodsChecked(1,1))
        set(ManualCheckbox2,'Value',PeriodsChecked(1,2))
        set(ManualCheckbox3,'Value',PeriodsChecked(1,3))
        set(ManualCheckbox4,'Value',PeriodsChecked(1,4))
        msgbox('Clipping is not allowed during averaging')
    end
    plotAll
end

function TrajectoryMarkerCallback(source, eventdata)
    SelectedTrajectoryMarkers = get(source,'Value');  
    clear CurrentTrajectoryMarkers; CurrentTrajectoryMarkers = SelectedTrajectoryMarkers';
    plotTrajectory
    plotOnsetsAndOffsets
    plotTimepoint
end 

function plotTrajectory
    delete(get(s1,'Children'))
    set(s1,'zdir','reverse') %added by SA for reach_grasp experiments in stroke
    for(i=1:size(CurrentTrials,1))
        for(j=1:size(CurrentTrajectoryMarkers,1))
            if(get(AverageCheckbox,'Value')==0)
                TotalRange = [];
                for(num=1:size(PeriodsChecked,2))
                    if(PeriodsChecked(1,num)==1)
                        TotalRange = cat(2,TotalRange, [CycledData(CurrentTrials(i,1),num*2-1):CycledData(CurrentTrials(i,1),num*2)]);
                    end
                end  
                if(isempty(TotalRange))
                    TotalRange = 1:size(Data{CurrentTrials(i,1)},1);
                end
                if(DevType==1)
                    Data2Plot = Data{CurrentTrials(i,1)}(TotalRange,ColumnRangeArray(1,DevType*CurrentTrajectoryMarkers(j,1)))';
                    XRange = TotalRange-CycledData(CurrentTrials(i,1),AlignBy); 
                    plot(s1,    XRange, ...
                                Data2Plot, ...
                                'Color',[CurrentTrajectoryMarkers(j,1)/NumMarkers (1-CurrentTrajectoryMarkers(j,1)/NumMarkers) CurrentTrajectoryMarkers(j,1)/NumMarkers/2], ...
                                'ButtonDownFcn',@ManualTimepointCallback); 
                elseif(DevType==3)
                    plot3(s1,   Data{CurrentTrials(i,1)}(TotalRange,ColumnRangeArray(1,DevType*CurrentTrajectoryMarkers(j,1)-2)), ... 
                                Data{CurrentTrials(i,1)}(TotalRange,ColumnRangeArray(1,DevType*CurrentTrajectoryMarkers(j,1)-1)), ...
                                Data{CurrentTrials(i,1)}(TotalRange,ColumnRangeArray(1,DevType*CurrentTrajectoryMarkers(j,1)-0)), ...
                                '.','Color',[CurrentTrajectoryMarkers(j,1)/NumMarkers (1-CurrentTrajectoryMarkers(j,1)/NumMarkers) CurrentTrajectoryMarkers(j,1)/NumMarkers/2]); axis(s1,'equal')
                end
            else
                Data2Plot = Data{CurrentTrials(i,1)}(:,ColumnRangeArray(1,DevType*CurrentTrajectoryMarkers(j,1)))';
                if(i==1)
                    TotalAlign = CycledData(CurrentTrials(i,1),AlignBy);
                    TotalPlots{j}(i,:) = Data2Plot;
                else
                    CurrentAlign = CycledData(CurrentTrials(i,1),AlignBy);
                    CurrentPlot = Data2Plot;

                    NewAlign = min(TotalAlign,CurrentAlign);
                    
                    NumOfPointsAfterAlign = min(size(TotalPlots{j}(:,TotalAlign+1:end),2),size(CurrentPlot(CurrentAlign+1:end),2));   
                    TotalPlots{j} = TotalPlots{j}(:,(TotalAlign-NewAlign+1):(TotalAlign+NumOfPointsAfterAlign));
                    TotalPlots{j}(i,:) = CurrentPlot(1,(CurrentAlign-NewAlign+1):(CurrentAlign+NumOfPointsAfterAlign));
                    TotalAlign = NewAlign;
                end                
            end
            if(~ishold(s1))
                hold(s1)
            end
        end
    end
    
    %The following averages the plots and plots the average if the AverageCheckbox is selected 
    for(j=1:size(CurrentTrajectoryMarkers,1))
        if(get(AverageCheckbox,'Value')==1)
            AveragePlot = mean(TotalPlots{j},1);
            xrange=1-TotalAlign:size(AveragePlot,2)-TotalAlign;
            xrange=[xrange,fliplr(xrange)];
            AveragePlotPlusSTD = AveragePlot+numSTD*std(TotalPlots{j},0,1);
            AveragePlotMinusSTD = AveragePlot-numSTD*std(TotalPlots{j},0,1);
            yplot=[AveragePlotPlusSTD,fliplr(AveragePlotMinusSTD)];
            colorfill=[.9 .9 .9];
            axes(s1); fill(xrange,yplot,colorfill);
            if(~ishold(s1))
                hold(s1)
            end
            plot(s1,1-TotalAlign:size(AveragePlot,2)-TotalAlign,AveragePlot,'LineWidth',2,'Color',[CurrentTrajectoryMarkers(j,1)/NumMarkers (1-CurrentTrajectoryMarkers(j,1)/NumMarkers) CurrentTrajectoryMarkers(j,1)/NumMarkers/2],'ButtonDownFcn',@ManualTimepointCallback) 
        end
    end
end

function TangentialVelocityDropdownCallback(source, eventdata)
    clear CurrentTangVelMarker; CurrentTangVelMarker = get(source,'Value');
    plotTangentialVelocity
    plotXYZvelocity 
    plotOnsetsAndOffsets
    plotTimepoint 
end

function plotTangentialVelocity
    delete(get(s2,'Children'))            
    for(i=1:size(CurrentTrials,1))
        if(DevType==1) 
            clear TangVel; TangVel = DataVel{CurrentTrials(i,1)}(:,ColumnRangeArray(1,DevType*CurrentTangVelMarker));
        elseif(DevType==3) 
            clear TangVel; TangVel = sqrt(  (DataVel{CurrentTrials(i,1)}(:,ColumnRangeArray(1,DevType*CurrentTangVelMarker-2))).^2+ ... 
                                            (DataVel{CurrentTrials(i,1)}(:,ColumnRangeArray(1,DevType*CurrentTangVelMarker-1))).^2+ ... 
                                            (DataVel{CurrentTrials(i,1)}(:,ColumnRangeArray(1,DevType*CurrentTangVelMarker-0))).^2); 
        end
        TangVel = TangVel';
        
        if(get(AverageCheckbox,'Value')==0)
            plot(s2,1-CycledData(CurrentTrials(i,1),AlignBy):size(TangVel,2)-CycledData(CurrentTrials(i,1),AlignBy),TangVel,'Color',[CurrentTangVelMarker/NumMarkers (1-CurrentTangVelMarker/NumMarkers) CurrentTangVelMarker/NumMarkers/2],'ButtonDownFcn',@ManualTimepointCallback) 
            if(~ishold(s2))
                hold(s2)
            end
        else
            if(i==1)
                TotalAlign = CycledData(CurrentTrials(i,1),AlignBy);
                TotalPlots(i,:) = TangVel;
            else
                CurrentAlign = CycledData(CurrentTrials(i,1),AlignBy);
                CurrentPlot = TangVel;
                
                NewAlign = min(TotalAlign,CurrentAlign);
                NumOfPointsAfterAlign = min(size(TotalPlots(:,TotalAlign+1:end),2),size(CurrentPlot(CurrentAlign+1:end),2));   
                TotalPlots = TotalPlots(:,(TotalAlign-NewAlign+1):(TotalAlign+NumOfPointsAfterAlign));
                TotalPlots(i,:) = CurrentPlot(1,(CurrentAlign-NewAlign+1):(CurrentAlign+NumOfPointsAfterAlign));
                TotalAlign = NewAlign;
            end
        end
    end
    
    %The following averages the plots and plots the average if the AverageCheckbox is selected 
    if(get(AverageCheckbox,'Value')==1)
        AveragePlot = mean(TotalPlots,1);
        xrange=1-TotalAlign:size(AveragePlot,2)-TotalAlign;
        xrange=[xrange,fliplr(xrange)];
        AveragePlotPlusSTD = AveragePlot+numSTD*std(TotalPlots,0,1);
        AveragePlotMinusSTD = AveragePlot-numSTD*std(TotalPlots,0,1);
        yplot=[AveragePlotPlusSTD,fliplr(AveragePlotMinusSTD)];
        colorfill=[.9 .9 .9];
        axes(s2); fill(xrange,yplot,colorfill);
        if(~ishold(s2))
            hold(s2)
        end
        plot(s2,1-TotalAlign:size(AveragePlot,2)-TotalAlign,AveragePlot,'LineWidth',2,'Color',[CurrentTangVelMarker/NumMarkers (1-CurrentTangVelMarker/NumMarkers) CurrentTangVelMarker/NumMarkers/2],'ButtonDownFcn',@ManualTimepointCallback) 
    end
end

function XYZvelocityDropdownCallback(source, eventdata)
    clear CurrentXYZ; CurrentXYZ = get(source,'Value');
    plotXYZvelocity
    plotOnsetsAndOffsets
    plotTimepoint
end

function plotXYZvelocity
    if(DevType==3)
        delete(get(s3,'Children'))
        for(i=1:size(CurrentTrials,1))
            clear XYZvel; XYZvel = DataVel{CurrentTrials(i,1)}(:,ColumnRangeArray(1,DevType*CurrentTangVelMarker-3+CurrentXYZ))';
            if(get(AverageCheckbox,'Value')==0)
                plot(s3,1-CycledData(CurrentTrials(i,1),AlignBy):size(XYZvel,2)-CycledData(CurrentTrials(i,1),AlignBy),XYZvel,'Color',[CurrentTangVelMarker/NumMarkers (1-CurrentTangVelMarker/NumMarkers) CurrentTangVelMarker/NumMarkers/2],'ButtonDownFcn',@ManualTimepointCallback)
                if(~ishold(s3))
                    hold(s3)
                end
            else
                if(i==1)
                    TotalAlign = CycledData(CurrentTrials(i,1),AlignBy);
                    TotalPlots(i,:) = XYZvel;
                else
                    CurrentAlign = CycledData(CurrentTrials(i,1),AlignBy);
                    CurrentPlot = XYZvel;
                    
                    NewAlign = min(TotalAlign,CurrentAlign);
                    NumOfPointsAfterAlign = min(size(TotalPlots(:,TotalAlign+1:end),2),size(CurrentPlot(CurrentAlign+1:end),2));
                    TotalPlots = TotalPlots(:,(TotalAlign-NewAlign+1):(TotalAlign+NumOfPointsAfterAlign));
                    TotalPlots(i,:) = CurrentPlot(1,(CurrentAlign-NewAlign+1):(CurrentAlign+NumOfPointsAfterAlign));
                    TotalAlign = NewAlign;
                end
            end
        end
        
        if(get(AverageCheckbox,'Value')==1)
            AveragePlot = mean(TotalPlots,1);
            xrange=1-TotalAlign:size(AveragePlot,2)-TotalAlign;
            xrange=[xrange,fliplr(xrange)];
            AveragePlotPlusSTD = AveragePlot+numSTD*std(TotalPlots,0,1);
            AveragePlotMinusSTD = AveragePlot-numSTD*std(TotalPlots,0,1);
            yplot=[AveragePlotPlusSTD,fliplr(AveragePlotMinusSTD)];
            colorfill=[.9 .9 .9];
            axes(s3); fill(xrange,yplot,colorfill);
            if(~ishold(s3))
                hold(s3)
            end
            plot(s3,1-TotalAlign:size(AveragePlot,2)-TotalAlign,AveragePlot,'LineWidth',2,'Color',[CurrentTangVelMarker/NumMarkers (1-CurrentTangVelMarker/NumMarkers) CurrentTangVelMarker/NumMarkers/2],'ButtonDownFcn',@ManualTimepointCallback)
        end
    end
end

function ApertureM1Callback(source, eventdata)
    clear CurrentM1; CurrentM1 = get(source,'Value');
    if(CurrentM1==CurrentM2)
        msgbox('You must select different markers to obtain an aperture...')
        delete(get(s4,'Children'))            
        delete(get(s5,'Children'))            
    else
        plotAperture
        plotOnsetsAndOffsets
        plotTimepoint
    end
end

function ApertureM2Callback(source, eventdata)
    clear CurrentM2; CurrentM2 = get(source,'Value');
    if(CurrentM1==CurrentM2)
        msgbox('You must select different markers to obtain an aperture...')
        delete(get(s4,'Children'))            
        delete(get(s5,'Children'))            
    else
        plotAperture
        plotOnsetsAndOffsets
        plotTimepoint
    end
end

function plotAperture
    delete(get(s4,'Children'))            
    delete(get(s5,'Children'))            
    for(i=1:size(CurrentTrials,1))
        if(DevType==1)
            clear MarkerAperture; MarkerAperture = Data{CurrentTrials(i,1)}(:,ColumnRangeArray(1,DevType*CurrentM1)) - Data{CurrentTrials(i,1)}(:,ColumnRangeArray(1,DevType*CurrentM2)); 
        elseif(DevType==3) 
            clear MarkerAperture; MarkerAperture = sqrt( ...
                 (Data{CurrentTrials(i,1)}(:,ColumnRangeArray(1,DevType*CurrentM1-2)) - Data{CurrentTrials(i,1)}(:,ColumnRangeArray(1,DevType*CurrentM2-2))).^2 ...                     
                +(Data{CurrentTrials(i,1)}(:,ColumnRangeArray(1,DevType*CurrentM1-1)) - Data{CurrentTrials(i,1)}(:,ColumnRangeArray(1,DevType*CurrentM2-1))).^2 ...
                +(Data{CurrentTrials(i,1)}(:,ColumnRangeArray(1,DevType*CurrentM1-0)) - Data{CurrentTrials(i,1)}(:,ColumnRangeArray(1,DevType*CurrentM2-0))).^2);
        end
        MarkerAperture = MarkerAperture';
        clear MarkerApertureVel; MarkerApertureVel = MarkerAperture(1,2:length(MarkerAperture))-MarkerAperture(1,1:length(MarkerAperture)-1,1);
        MarkerApertureVel(length(MarkerApertureVel)+1)=MarkerApertureVel(length(MarkerApertureVel));
        
        if(get(AverageCheckbox,'Value')==0)
            plot(s4,1-CycledData(CurrentTrials(i,1),AlignBy):size(MarkerAperture,2)-CycledData(CurrentTrials(i,1),AlignBy),MarkerAperture,'Color','y','ButtonDownFcn',@ManualTimepointCallback)
            plot(s5,1-CycledData(CurrentTrials(i,1),AlignBy):size(MarkerApertureVel,2)-CycledData(CurrentTrials(i,1),AlignBy),MarkerApertureVel,'Color','y','ButtonDownFcn',@ManualTimepointCallback) 
            if(~ishold(s4))
                hold(s4)
            end
            if(~ishold(s5))
                hold(s5)
            end
        else
            if(i==1)
                TotalAlign = CycledData(CurrentTrials(i,1),AlignBy);
                TotalPlotsMarkerAperture(i,:) = MarkerAperture;
                TotalPlotsMarkerApertureVel(i,:) = MarkerApertureVel;
            else
                CurrentAlign = CycledData(CurrentTrials(i,1),AlignBy);
                CurrentPlotMarkerAperture = MarkerAperture;
                CurrentPlotMarkerApertureVel = MarkerApertureVel;
                
                NewAlign = min(TotalAlign,CurrentAlign);
                NumOfPointsAfterAlign = min(size(TotalPlotsMarkerAperture(:,TotalAlign+1:end),2),size(CurrentPlotMarkerAperture(CurrentAlign+1:end),2));   
                TotalPlotsMarkerAperture = TotalPlotsMarkerAperture(:,(TotalAlign-NewAlign+1):(TotalAlign+NumOfPointsAfterAlign));
                TotalPlotsMarkerApertureVel = TotalPlotsMarkerApertureVel(:,(TotalAlign-NewAlign+1):(TotalAlign+NumOfPointsAfterAlign));
                TotalPlotsMarkerAperture(i,:) = CurrentPlotMarkerAperture(1,(CurrentAlign-NewAlign+1):(CurrentAlign+NumOfPointsAfterAlign));
                TotalPlotsMarkerApertureVel(i,:) = CurrentPlotMarkerApertureVel(1,(CurrentAlign-NewAlign+1):(CurrentAlign+NumOfPointsAfterAlign));
                TotalAlign = NewAlign;
            end
        end
    end
    
    if(get(AverageCheckbox,'Value')==1)
        AveragePlotMarkerAperture = mean(TotalPlotsMarkerAperture,1);
        xrangeMarkerAperture=1-TotalAlign:size(AveragePlotMarkerAperture,2)-TotalAlign;
        xrangeMarkerAperture=[xrangeMarkerAperture,fliplr(xrangeMarkerAperture)];
        AveragePlotMarkerAperturePlusSTD = AveragePlotMarkerAperture+numSTD*std(TotalPlotsMarkerAperture,0,1);
        AveragePlotMarkerApertureMinusSTD = AveragePlotMarkerAperture-numSTD*std(TotalPlotsMarkerAperture,0,1);
        yplotMarkerAperture=[AveragePlotMarkerAperturePlusSTD,fliplr(AveragePlotMarkerApertureMinusSTD)];
        colorfill=[.9 .9 .9];
        axes(s4); fill(xrangeMarkerAperture,yplotMarkerAperture,colorfill);
        if(~ishold(s4))
            hold(s4)
        end
        plot(s4,1-TotalAlign:size(AveragePlotMarkerAperture,2)-TotalAlign,AveragePlotMarkerAperture,'LineWidth',2,'Color','y','ButtonDownFcn',@ManualTimepointCallback) 

        AveragePlotMarkerApertureVel = mean(TotalPlotsMarkerApertureVel,1);
        xrangeMarkerApertureVel=1-TotalAlign:size(AveragePlotMarkerApertureVel,2)-TotalAlign;
        xrangeMarkerApertureVel=[xrangeMarkerApertureVel,fliplr(xrangeMarkerApertureVel)];
        AveragePlotMarkerApertureVelPlusSTD = AveragePlotMarkerApertureVel+numSTD*std(TotalPlotsMarkerApertureVel,0,1);
        AveragePlotMarkerApertureVelMinusSTD = AveragePlotMarkerApertureVel-numSTD*std(TotalPlotsMarkerApertureVel,0,1);
        yplotMarkerApertureVel=[AveragePlotMarkerApertureVelPlusSTD,fliplr(AveragePlotMarkerApertureVelMinusSTD)];
        colorfill=[.9 .9 .9];
        axes(s5); fill(xrangeMarkerApertureVel,yplotMarkerApertureVel,colorfill);
        if(~ishold(s5))
            hold(s5)
        end
        plot(s5,1-TotalAlign:size(AveragePlotMarkerApertureVel,2)-TotalAlign,AveragePlotMarkerApertureVel,'LineWidth',2,'Color','y','ButtonDownFcn',@ManualTimepointCallback) 
    end
end

function AlignmentDropdownCallback(source, eventdata)
    AlignmentDropdownValue = get(source,'Value');
    AlignmentDropdownStrings = get(source,'String');
    AlignmentDropdownDisplay = AlignmentDropdownStrings{AlignmentDropdownValue,1};
    AlignByTemp = get(source,'Value');
    if(CycledData(CurrentTrials(:,1),AlignByTemp)~=initialize)
        clear AlignBy; AlignBy = AlignByTemp;
    else
        msgbox(['You must first set ' AlignmentDropdownDisplay(1,11:end)])
        set(AlignmentDropdown,'Value',AlignBy) 
    end
    clear CurrentTimepoint; CurrentTimepoint = 0;
    plotAll
end

function ManualOnset1ButtonCallback(source, eventdata)
    if(size(CurrentTrials,1)~=1)
        msgbox('You must select only one trial to manually cycle')
    elseif(CurrentTimepoint+CycledData(CurrentTrials,AlignBy)<1)
        msgbox('Onset time must be after data collection')
    else
        CycledData(CurrentTrials,1) = CurrentTimepoint+CycledData(CurrentTrials,AlignBy);                                             %setting the onset time to the current timepoint 
        xlswrite('Cycled.xls', CycledData(CurrentTrials,1),['D' num2str(CurrentTrials) ':' 'D' num2str(CurrentTrials)]);        %exporting the data to the proper position in the excel file 
        plotAll
    end
end

function ManualOffset1ButtonCallback(source, eventdata)
    if(size(CurrentTrials,1)~=1)
        msgbox('You must select only one trial to manually cycle')
    elseif(CurrentTimepoint+CycledData(CurrentTrials,AlignBy)<1)
        msgbox('Onset time must be after data collection')
    else
        CycledData(CurrentTrials,2) = CurrentTimepoint+CycledData(CurrentTrials,AlignBy); %setting the onset time to the current timepoint 
        xlswrite('Cycled.xls', CycledData(CurrentTrials,2),['E' num2str(CurrentTrials) ':' 'E' num2str(CurrentTrials)]);   
        plotAll
    end
end

function ManualOnset2ButtonCallback(source, eventdata)
    if(size(CurrentTrials,1)~=1)
        msgbox('You must select only one trial to manually cycle')
    elseif(CurrentTimepoint+CycledData(CurrentTrials,AlignBy)<1)
        msgbox('Onset time must be after data collection')
    else
        CycledData(CurrentTrials,3) = CurrentTimepoint+CycledData(CurrentTrials,AlignBy); %setting the onset time to the current timepoint 
        xlswrite('Cycled.xls', CycledData(CurrentTrials,3),['F' num2str(CurrentTrials) ':' 'F' num2str(CurrentTrials)]);         
        plotAll
    end
end

function ManualOffset2ButtonCallback(source, eventdata)
    if(size(CurrentTrials,1)~=1)
        msgbox('You must select only one trial to manually cycle')
    elseif(CurrentTimepoint+CycledData(CurrentTrials,AlignBy)<1)
        msgbox('Onset time must be after data collection')
    else
        CycledData(CurrentTrials,4) = CurrentTimepoint+CycledData(CurrentTrials,AlignBy); %setting the onset time to the current timepoint 
        xlswrite('Cycled.xls', CycledData(CurrentTrials,4),['G' num2str(CurrentTrials) ':' 'G' num2str(CurrentTrials)]);         
        plotAll
    end
end

function ManualOnset3ButtonCallback(source, eventdata)
    if(size(CurrentTrials,1)~=1)
        msgbox('You must select only one trial to manually cycle')
    elseif(CurrentTimepoint+CycledData(CurrentTrials,AlignBy)<1)
        msgbox('Onset time must be after data collection')
    else
        CycledData(CurrentTrials,5) = CurrentTimepoint+CycledData(CurrentTrials,AlignBy); %setting the onset time to the current timepoint 
        xlswrite('Cycled.xls', CycledData(CurrentTrials,5),['H' num2str(CurrentTrials) ':' 'H' num2str(CurrentTrials)]);         
        plotAll
    end
end

function ManualOffset3ButtonCallback(source, eventdata)
    if(size(CurrentTrials,1)~=1)
        msgbox('You must select only one trial to manually cycle')
    elseif(CurrentTimepoint+CycledData(CurrentTrials,AlignBy)<1)
        msgbox('Onset time must be after data collection')
    else
        CycledData(CurrentTrials,6) = CurrentTimepoint+CycledData(CurrentTrials,AlignBy); %setting the onset time to the current timepoint 
        xlswrite('Cycled.xls', CycledData(CurrentTrials,6),['I' num2str(CurrentTrials) ':' 'I' num2str(CurrentTrials)]);         
        plotAll
    end
end

function ManualOnset4ButtonCallback(source, eventdata)
    if(size(CurrentTrials,1)~=1)
        msgbox('You must select only one trial to manually cycle')
    elseif(CurrentTimepoint+CycledData(CurrentTrials,AlignBy)<1)
        msgbox('Onset time must be after data collection')
    else
        CycledData(CurrentTrials,7) = CurrentTimepoint+CycledData(CurrentTrials,AlignBy); %setting the onset time to the current timepoint 
        xlswrite('Cycled.xls', CycledData(CurrentTrials,7),['J' num2str(CurrentTrials) ':' 'J' num2str(CurrentTrials)]);         
        plotAll
    end
end

function ManualOffset4ButtonCallback(source, eventdata)
    if(size(CurrentTrials,1)~=1)
        msgbox('You must select only one trial to manually cycle')
    elseif(CurrentTimepoint+CycledData(CurrentTrials,AlignBy)<1)
        msgbox('Onset time must be after data collection')
    else
        CycledData(CurrentTrials,8) = CurrentTimepoint+CycledData(CurrentTrials,AlignBy); %setting the onset time to the current timepoint 
        xlswrite('Cycled.xls', CycledData(CurrentTrials,8),['K' num2str(CurrentTrials) ':' 'K' num2str(CurrentTrials)]);         
        plotAll
    end
end

function ManualCheckboxCallback(source, eventdata)
    ManualCheckboxConfirm
    plotAll
end

function ManualCheckboxConfirm
    CheckboxAllowed=1;
    
    CheckboxValue(1,1) = get(ManualCheckbox1,'Value');
    CheckboxValue(1,2) = get(ManualCheckbox2,'Value');
    CheckboxValue(1,3) = get(ManualCheckbox3,'Value');
    CheckboxValue(1,4) = get(ManualCheckbox4,'Value');
    
    for(i=1:4)
        if(CheckboxValue(1,i) == 1)
            for(j=1:2)
                for(k=1:size(CurrentTrials,1))
                    if(CycledData(CurrentTrials(k,1),i*j)==initialize)
                        CheckboxAllowed=0;
                    end
                end
            end
        end
    end
    
    if(CheckboxAllowed == 1)
        clear PeriodsChecked; PeriodsChecked=CheckboxValue; 
    else
        clear PeriodsChecked; PeriodsChecked=[0 0 0 0]; 
        set(ManualCheckbox1,'Value',PeriodsChecked(1,1))
        set(ManualCheckbox2,'Value',PeriodsChecked(1,2))
        set(ManualCheckbox3,'Value',PeriodsChecked(1,3))
        set(ManualCheckbox4,'Value',PeriodsChecked(1,4))
        msgbox('You must set the corresponding onset and offset for the selected trials before clipping')
    end
end

function plotOnsetsAndOffsets
    for(num=1:size(PeriodsChecked,2))
        if(PeriodsChecked(1,num)==1)
            delete(findobj(s1, 'Tag',['OnsetPoint' num2str(num)])); 
            for(i=1:size(CurrentTrials,1))
                for(j=1:size(CurrentTrajectoryMarkers,1))
                    if(CycledData(CurrentTrials(i,1),2*num-1)~=initialize)
                        if(DevType==1)
                            %plot(s1,    Data{CurrentTrials(i,1)}(CycledData(CurrentTrials(i,1),2*num-1),ColumnRangeArray(1,DevType*CurrentTrajectoryMarkers(j,1))), ...
                            %            'o','MarkerSize',10,'MarkerEdgeColor','k','MarkerFaceColor',BarColors{num},'Tag',['OnsetPoint' num2str(num)]);
                        elseif(DevType==3)
                            plot3(s1,   Data{CurrentTrials(i,1)}(CycledData(CurrentTrials(i,1),2*num-1),ColumnRangeArray(1,DevType*CurrentTrajectoryMarkers(j,1)-2)), ... 
                                        Data{CurrentTrials(i,1)}(CycledData(CurrentTrials(i,1),2*num-1),ColumnRangeArray(1,DevType*CurrentTrajectoryMarkers(j,1)-1)), ...
                                        Data{CurrentTrials(i,1)}(CycledData(CurrentTrials(i,1),2*num-1),ColumnRangeArray(1,DevType*CurrentTrajectoryMarkers(j,1)-0)), ...
                                        'o','MarkerSize',10,'MarkerEdgeColor','k','MarkerFaceColor',BarColors{num},'Tag',['OnsetPoint' num2str(num)]); axis(s1,'equal')
                        end
                    end
                end
            end

            delete(findobj(s1, 'Tag',['OffsetPoint' num2str(num)])); 
            for(i=1:size(CurrentTrials,1))
                for(j=1:size(CurrentTrajectoryMarkers,1))
                    if(CycledData(CurrentTrials(i,1),2*num)~=initialize)
                        if(DevType==1)
                            %plot(s1,    Data{CurrentTrials(i,1)}(CycledData(CurrentTrials(i,1),2*num),ColumnRangeArray(1,DevType*CurrentTrajectoryMarkers(j,1))), ...
                            %            'o','MarkerSize',10,'MarkerEdgeColor','k','MarkerFaceColor',BarColors{num},'Tag',['OffsetPoint' num2str(num)]);
                        elseif(DevType==3)
                            plot3(s1,   Data{CurrentTrials(i,1)}(CycledData(CurrentTrials(i,1),2*num),ColumnRangeArray(1,DevType*CurrentTrajectoryMarkers(j,1)-2)), ... 
                                        Data{CurrentTrials(i,1)}(CycledData(CurrentTrials(i,1),2*num),ColumnRangeArray(1,DevType*CurrentTrajectoryMarkers(j,1)-1)), ...
                                        Data{CurrentTrials(i,1)}(CycledData(CurrentTrials(i,1),2*num),ColumnRangeArray(1,DevType*CurrentTrajectoryMarkers(j,1)-0)), ...
                                        'o','MarkerSize',10,'MarkerEdgeColor','k','MarkerFaceColor',BarColors{num},'Tag',['OffsetPoint' num2str(num)]); axis(s1,'equal')
                        end
                    end
                end
            end
        end
        
        if(size(CurrentTrials,1)==1 && ~isnan(CycledData(CurrentTrials(1,1),2*num-1)) && CycledData(CurrentTrials(1,1),2*num-1)>0)
            if(DevType==1)
                delete(findobj(s1, 'Tag',['OnsetLine' num2str(num)])); 
                plot(s1,[CycledData(CurrentTrials(1,1),2*num-1)-CycledData(CurrentTrials(1,1),AlignBy) CycledData(CurrentTrials(1,1),2*num-1)-CycledData(CurrentTrials(1,1),AlignBy)],[min(get(s1,'YTick')) max(get(s1,'YTick'))],BarColors{num},'LineWidth',2,'Tag','OnsetLine')
            end
            delete(findobj(s2, 'Tag',['OnsetLine' num2str(num)])); 
            plot(s2,[CycledData(CurrentTrials(1,1),2*num-1)-CycledData(CurrentTrials(1,1),AlignBy) CycledData(CurrentTrials(1,1),2*num-1)-CycledData(CurrentTrials(1,1),AlignBy)],[min(get(s2,'YTick')) max(get(s2,'YTick'))],BarColors{num},'LineWidth',2,'Tag','OnsetLine')
            delete(findobj(s3, 'Tag',['OnsetLine' num2str(num)])); 
            plot(s3,[CycledData(CurrentTrials(1,1),2*num-1)-CycledData(CurrentTrials(1,1),AlignBy) CycledData(CurrentTrials(1,1),2*num-1)-CycledData(CurrentTrials(1,1),AlignBy)],[min(get(s3,'YTick')) max(get(s3,'YTick'))],BarColors{num},'LineWidth',2,'Tag','OnsetLine')
            delete(findobj(s4, 'Tag',['OnsetLine' num2str(num)])); 
            plot(s4,[CycledData(CurrentTrials(1,1),2*num-1)-CycledData(CurrentTrials(1,1),AlignBy) CycledData(CurrentTrials(1,1),2*num-1)-CycledData(CurrentTrials(1,1),AlignBy)],[min(get(s4,'YTick')) max(get(s4,'YTick'))],BarColors{num},'LineWidth',2,'Tag','OnsetLine')
            delete(findobj(s5, 'Tag',['OnsetLine' num2str(num)])); 
            plot(s5,[CycledData(CurrentTrials(1,1),2*num-1)-CycledData(CurrentTrials(1,1),AlignBy) CycledData(CurrentTrials(1,1),2*num-1)-CycledData(CurrentTrials(1,1),AlignBy)],[min(get(s5,'YTick')) max(get(s5,'YTick'))],BarColors{num},'LineWidth',2,'Tag','OnsetLine')
        end
        
        if(size(CurrentTrials,1)==1 && ~isnan(CycledData(CurrentTrials(1,1),2*num)) && CycledData(CurrentTrials(1,1),2*num)>0)
            if(DevType==1)
                delete(findobj(s1, 'Tag',['OffsetLine' num2str(num)])); 
                plot(s1,[CycledData(CurrentTrials(1,1),2*num)-CycledData(CurrentTrials(1,1),AlignBy) CycledData(CurrentTrials(1,1),2*num)-CycledData(CurrentTrials(1,1),AlignBy)],[min(get(s1,'YTick')) max(get(s1,'YTick'))],BarColors{num},'LineWidth',2,'Tag','OffsetLine')
            end
            delete(findobj(s2, 'Tag',['OffsetLine' num2str(num)])); 
            plot(s2,[CycledData(CurrentTrials(1,1),2*num)-CycledData(CurrentTrials(1,1),AlignBy) CycledData(CurrentTrials(1,1),2*num)-CycledData(CurrentTrials(1,1),AlignBy)],[min(get(s2,'YTick')) max(get(s2,'YTick'))],BarColors{num},'LineWidth',2,'Tag','OffsetLine')
            delete(findobj(s3, 'Tag',['OffsetLine' num2str(num)])); 
            plot(s3,[CycledData(CurrentTrials(1,1),2*num)-CycledData(CurrentTrials(1,1),AlignBy) CycledData(CurrentTrials(1,1),2*num)-CycledData(CurrentTrials(1,1),AlignBy)],[min(get(s3,'YTick')) max(get(s3,'YTick'))],BarColors{num},'LineWidth',2,'Tag','OffsetLine')
            delete(findobj(s4, 'Tag',['OffsetLine' num2str(num)])); 
            plot(s4,[CycledData(CurrentTrials(1,1),2*num)-CycledData(CurrentTrials(1,1),AlignBy) CycledData(CurrentTrials(1,1),2*num)-CycledData(CurrentTrials(1,1),AlignBy)],[min(get(s4,'YTick')) max(get(s4,'YTick'))],BarColors{num},'LineWidth',2,'Tag','OffsetLine')
            delete(findobj(s5, 'Tag',['OffsetLine' num2str(num)])); 
            plot(s5,[CycledData(CurrentTrials(1,1),2*num)-CycledData(CurrentTrials(1,1),AlignBy) CycledData(CurrentTrials(1,1),2*num)-CycledData(CurrentTrials(1,1),AlignBy)],[min(get(s5,'YTick')) max(get(s5,'YTick'))],BarColors{num},'LineWidth',2,'Tag','OffsetLine')
        end
    end
end

function plotKinematicsTable
    KinematicData=[];
    for(i=1:size(CurrentTrials,1))
        for(num=1:NumberOfOnsets)
            if(CycledData(CurrentTrials(i,1),num*2-1)~=initialize && CycledData(CurrentTrials(i,1),num*2)~=initialize)
                if(DevType==1) 
                    clear DataArray; DataArray = Data{CurrentTrials(i,1)}(CycledData(CurrentTrials(i,1),num*2-1):CycledData(CurrentTrials(i,1),num*2),ColumnRangeArray(1,DevType*CurrentKinMarkers(1,1)));
                    clear TangVel; TangVel = DataVel{CurrentTrials(i,1)}(CycledData(CurrentTrials(i,1),num*2-1):CycledData(CurrentTrials(i,1),num*2),ColumnRangeArray(1,DevType*CurrentKinMarkers(1,1)));
                elseif(DevType==3) 
                    clear DataArray; DataArray = sqrt(  (Data{CurrentTrials(i,1)}(CycledData(CurrentTrials(i,1),num*2-1):CycledData(CurrentTrials(i,1),num*2),ColumnRangeArray(1,DevType*CurrentKinMarkers(1,1)-2))).^2+ ... 
                                                        (Data{CurrentTrials(i,1)}(CycledData(CurrentTrials(i,1),num*2-1):CycledData(CurrentTrials(i,1),num*2),ColumnRangeArray(1,DevType*CurrentKinMarkers(1,1)-1))).^2+ ... 
                                                        (Data{CurrentTrials(i,1)}(CycledData(CurrentTrials(i,1),num*2-1):CycledData(CurrentTrials(i,1),num*2),ColumnRangeArray(1,DevType*CurrentKinMarkers(1,1)-0))).^2); 
                    clear TangVel; TangVel = sqrt(      (DataVel{CurrentTrials(i,1)}(CycledData(CurrentTrials(i,1),num*2-1):CycledData(CurrentTrials(i,1),num*2),ColumnRangeArray(1,DevType*CurrentKinMarkers(1,1)-2))).^2+ ... 
                                                        (DataVel{CurrentTrials(i,1)}(CycledData(CurrentTrials(i,1),num*2-1):CycledData(CurrentTrials(i,1),num*2),ColumnRangeArray(1,DevType*CurrentKinMarkers(1,1)-1))).^2+ ... 
                                                        (DataVel{CurrentTrials(i,1)}(CycledData(CurrentTrials(i,1),num*2-1):CycledData(CurrentTrials(i,1),num*2),ColumnRangeArray(1,DevType*CurrentKinMarkers(1,1)-0))).^2); 
                end
                DataArray = DataArray';
                TangVel = TangVel';
                [PTP]=abs(max(DataArray)-min(DataArray)); %peak-to-peak 
                [PV,TPV]=max(abs(TangVel)); %peak velocity and the time to peak velocity (time is recorded as the amount of time AFTER onset)
                TPV=TPV-1;
                TAPV = 10*(CycledData(CurrentTrials(i,1),num*2)-CycledData(CurrentTrials(i,1),num*2-1)-TPV);
                KinematicData(i,num*4-3) = PTP;
                KinematicData(i,num*4-2) = PV;
                KinematicData(i,num*4-1) = TPV;
                KinematicData(i,num*4-0) = TAPV;
            else
                KinematicData(i,num*4-3) = initialize;
                KinematicData(i,num*4-2) = initialize;
                KinematicData(i,num*4-1) = initialize;
                KinematicData(i,num*4-0) = initialize;
            end
        end
    end
    ColumnNames = {     ['M' num2str(CurrentKinMarkers(1,1)) '_PTP1'],['M' num2str(CurrentKinMarkers(1,1)) '_PV1'],['M' num2str(CurrentKinMarkers(1,1)) '_TPV1'],['M' num2str(CurrentKinMarkers(1,1)) '_TAPV1'], ...
                        ['M' num2str(CurrentKinMarkers(1,1)) '_PTP2'],['M' num2str(CurrentKinMarkers(1,1)) '_PV2'],['M' num2str(CurrentKinMarkers(1,1)) '_TPV2'],['M' num2str(CurrentKinMarkers(1,1)) '_TAPV2'], ... 
                        ['M' num2str(CurrentKinMarkers(1,1)) '_PTP3'],['M' num2str(CurrentKinMarkers(1,1)) '_PV3'],['M' num2str(CurrentKinMarkers(1,1)) '_TPV3'],['M' num2str(CurrentKinMarkers(1,1)) '_TAPV3'], ... 
                        ['M' num2str(CurrentKinMarkers(1,1)) '_PTP4'],['M' num2str(CurrentKinMarkers(1,1)) '_PV4'],['M' num2str(CurrentKinMarkers(1,1)) '_TPV4'],['M' num2str(CurrentKinMarkers(1,1)) '_TAPV4'], ... 
                        'Onset1','Offset1','Onset2','Offset2','Onset3','Offset3','Onset4','Offset4'};
    set(KinematicsTable,'RowName',CycledHeader(CurrentTrials,1),'ColumnName',ColumnNames,'Data',cat(2,KinematicData,CycledData(CurrentTrials,:)));
end

function KinMarkersCallback(source, eventdata)
    SelectedKinMarkers = get(source,'Value');  
    clear CurrentKinMarkers; CurrentKinMarkers = SelectedKinMarkers';
    plotKinematicsTable
end 

function plotAll
    plotTrajectory
    plotTangentialVelocity
    plotXYZvelocity
    plotAperture
    plotOnsetsAndOffsets
    clear CurrentTimepoint; CurrentTimepoint = 0; plotTimepoint    
    plotKinematicsTable
end

function KinSaveCallback(source, eventdata)
    DataToSave = get(KinematicsTable,'Data')
    ProperDirectory = cd;
    mkdir('OutputData');
    cd([ProperDirectory '\OutputData']);
    [filename, pathname] = uiputfile({'*.xls'},'Save Current Kinematic Data','output1.xls'); 
    xlswrite([pathname filename],DataToSave);  
    cd(ProperDirectory);
end

end

