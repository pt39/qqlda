clear all
subject='DBHAT';
s='C:\LDA_analysis_summer2012\LDA_Results_subj\EVERYTHING\HAT\' ;
dir=[s subject]
cd(dir);
%%

CVS=1;

    i
 for ka=3:3
     clear sheetValid1
     clear sheetValid2
     clear sheetValid3
     clear sheetValid4
 
     cd(dir);
switch ka
    
                   
                    case 1
                         Object1 = ['bigcircle'];
                         Object2 = ['smallcube'];
                         excelname='bigcircle smallcube.xlsx';
                         
                    case 2
                         Object1 = ['bigcircle'];
                         Object2 = ['smallcircle'];
                         excelname='bigcircle smallcircle.xlsx';
                         
                    case 3
                         Object1 = ['bigcube'];
                         Object2 = ['bigcircle'];
                         excelname='bigcube bigcircle.xlsx';
                         
                    case 4
                         Object1 = ['smallcube'];
                         Object2 = ['smallcircle'];
                         excelname='smallcube smallcircle.xlsx';
                         
                    case 5
                         Object1 = ['smallcube'];
                         Object2 = ['bigcube'];
                         excelname='smallcube bigcube.xlsx';
    
                          
                     case 6
                         Object1 = ['bigcube'];
                         Object2 = ['smallcircle'];
                         excelname='bigcube smallcircle.xlsx';
end

%%
[A,B] = xlsfinfo(excelname);
if CVS==2  
    
%
sheetValid1 = any(strcmp(B, 'preright'))
if sheetValid1==1
[preImpaired]=xlsread(excelname,'preright','BP2:BP400');
else
    
end
%
sheetValid2 = any(strcmp(B, 'postright'))
if sheetValid2==1
[postImpaired]=xlsread(excelname,'postright','BP2:BP400');
end

[A3,B3] = xlsfinfo(excelname);
sheetValid3 = any(strcmp(B, 'preleft'))
if sheetValid3==1
[preUnimpaired]=xlsread(excelname,'preleft','BP2:BP400');
end

[A4,B4] = xlsfinfo(excelname);
sheetValid4 = any(strcmp(B, 'postleft'))
if sheetValid4==1
[postUnimpaired]=xlsread(excelname,'postleft','BP2:BP400');
end


else

sheetValid1 = any(strcmp(B, 'preleft'))
if sheetValid1==1
[preImpaired]=xlsread(excelname,'preleft','BP2:BP400');
end


sheetValid2 = any(strcmp(B, 'postleft'))
if sheetValid2==1
[postImpaired]=xlsread(excelname,'postleft','BP2:BP400');
end


sheetValid3 = any(strcmp(B, 'preright'))
if sheetValid3==1
[preUnimpaired]=xlsread(excelname,'preright','BP2:BP400');
end


sheetValid4 = any(strcmp(B, 'postright'))
if sheetValid4==1
[postUnimpaired]=xlsread(excelname,'postright','BP2:BP400');
end
end
%%
%first we copy and paste all the values into one spreadsheet and then using
%another code we can calculate the average.
index=1:1:500;

cd(s);
sprName=strcat('AVG2',excelname);

a1=isempty(preImpaired)
a2=isempty(postImpaired)
a3=isempty(preUnimpaired)
a4=isempty(postUnimpaired)
%%
%comment this part if you don't need to save.

xlswrite(sprName,index',subject,'A2')
if a1==1
else
xlswrite(sprName,preImpaired,subject,'B2')
end
if a2==1
else
xlswrite(sprName,postImpaired,subject,'C2')
end
if a3==1
else
xlswrite(sprName,preUnimpaired,subject,'D2')
end
if a4==1
else
xlswrite(sprName,postUnimpaired,subject,'E2')
end

 end

 
 