%
function fingcr=SynFinger(OriginalData1,pathname1,timenorm,ia, object)
%ia=2 is pretest
% ia=3 is post
    noftrials = size(OriginalData1,3);
    [data txt]=xlsread('R:\LDA_backup\data\LDA\scripts\RTG_merged_12_2_2013');
    a=find(pathname1 == '\');
    subj = pathname1(a(end)+1:end);
    
    b =a(end-1)+1;
    c = find(pathname1(b:end)== ' ' );
    c = c(1)-2;
    group = pathname1(b:b+c);
    
    if (strcmp(group, 'RTG'))
        group = 'Controls';
    end
    
    if (ia ==2)
        test = 'pretest';
    else
        test = 'posttest';
    end
    
    names = {txt{2:end,1}};
    
    subjectStart = 1; 
    while(~strcmp(names{subjectStart}, subj))
        subjectStart = subjectStart + 1;
    end
   
    groupStart = subjectStart;
    while(~strcmp(names{groupStart}, group))
        groupStart= groupStart + 1;
    end
    
    testStart = groupStart;
    while(~strcmp(names{testStart}, test))
        testStart= testStart + 1;
    end
    
    shapeStart = testStart;
    while(~strcmp(names{shapeStart}, object))
        shapeStart= shapeStart + 1;
    end
    
    for i = 0:noftrials-1
       %clean data
       fingerOnset1 = data(shapeStart+i, 1);
       fingerOnset2 = data(shapeStart+i, 3);
       fprintf('%f, %f\n', fingerOnset1, fingerOnset2);
    end
    
end