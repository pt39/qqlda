function average_compare(pathname)

clc
close all


valName =input('What variable you want to compare? ( 1 Elbow Angle; 2 Trunk Movement; 3 Finger Angle (index MCP)):');
object=input('Which object you want to compare? (1 bigcube; 2 bigcircle; 3 smallcube; 4 smallcircle)');

group=input('Group name please (1 Control Group Cycled; 2 HAS cycled; 3 HAT cycled) : ');

switch group
    case 1
        groupDir=[pathname '\Control Group Cycled'];
    case 2
        groupDir=[pathname '\HAS cycled'];
    case 3
        groupDir=[pathname '\HAT cycled'];
end


subjectList=ls(groupDir);
display('Please input subject name as matrix format, etc: [1 2 3]');
questionDis=['subject name please ('];
for subjectNum=3:size(subjectList,1)
    questionDis=[questionDis num2str(subjectNum-2) ' ' strtrim(subjectList(subjectNum,:)) ';'];
end

questionDis=[questionDis ')'];

subjectName=input(questionDis);

EndPoint=input('Plot data from: 1 onset1-offset1; 2 onset1-onset2; 3 onset1-offset2; 4 onset1-onset3; 5 onset1-offset3:  ');

PreAcrossSubj=[];
PostAcrossSubj=[];

PreGLAcrossSubj=[];
PostGLAcrossSubj=[];

finalTitleName=[];
for Num=1:size(subjectName,2)
    subjectDir=[groupDir '\' strtrim(subjectList(subjectName(Num)+2,:))];
    
    preDir=[subjectDir '\pretest'];
    postDir=[subjectDir '\posttest'];
    
    handside=ls(preDir);
    
    if (strcmp(handside(3,:),'right'))
        glovePrefix='RCG';
    else
        glovePrefix='LCG';
    end
    
    
    preDir=[preDir '\' handside(3,:)];
    postDir=[postDir '\' handside(3,:)];
    
    %load pre calibration file
    PreCaliFileList=ls([preDir '\calibration\' glovePrefix '*.txt']);
    PreCalibration1=load([preDir '\calibration\' PreCaliFileList(1,:)]);
    PreCalibration2=load([preDir '\calibration\' PreCaliFileList(2,:)]);
    PreCalibration3=load([preDir '\calibration\' PreCaliFileList(3,:)]);

    %load post calibration file
    PostCaliFileList=ls([preDir '\calibration\' glovePrefix '*.txt']);
    PostCalibration1=load([preDir '\calibration\' PostCaliFileList(1,:)]);
    PostCalibration2=load([preDir '\calibration\' PostCaliFileList(2,:)]);
    PostCalibration3=load([preDir '\calibration\' PostCaliFileList(3,:)]);
        
    
    switch object
        case 1
            preDir=[preDir '\bigcube'];
            postDir=[postDir '\bigcube'];
        case 2
            preDir=[preDir '\bigcircle'];
            postDir=[postDir '\bigcircle'];
        case 3
            preDir=[preDir '\smallcube'];
            postDir=[postDir '\smallcube'];
        case 4
            preDir=[preDir '\smallcircle'];
            postDir=[postDir '\smallcircle'];
    end
    
    % figure title name
    figure
    
    
    PreCyclefileName=[preDir '\Cycled.xls'];
    [Precycle_data, Precycle_header]=xlsread(PreCyclefileName);
    
    PostCyclefileName=[postDir '\Cycled.xls'];
    [Postcycle_data, Postcycle_header]=xlsread(PostCyclefileName);
    
    
    PreData_acrossTrial=[];
    PostData_acrossTrial=[];
    PreCGData_acrossTrial=[];
    PostCGData_acrossTrial=[];
    
    % load pre test
    for PreNOBfileNum=1:size(Precycle_header,1)
        
        invalidFiles=find(Precycle_data(PreNOBfileNum,1:6)==-999999);
        if (isempty(invalidFiles))
            
        
        %load glove data
        PreCGFilename=regexprep(char(Precycle_header(PreNOBfileNum,1)),'NOB',glovePrefix);
        PreCGFile_tmp=load([preDir '\' PreCGFilename]);
        PreCGFile=calibration(PreCGFile_tmp,PreCalibration1,PreCalibration2,PreCalibration3);
        
        %load NOB data
        PreNOBfilename=[preDir '\' char(Precycle_header(PreNOBfileNum,1))];
        PreNOBfile=load(PreNOBfilename);
        
        Preonset1=Precycle_data(PreNOBfileNum,1);
        Preoffset1=Precycle_data(PreNOBfileNum,2);
        Preonset2=Precycle_data(PreNOBfileNum,3);
        Preoffset2=Precycle_data(PreNOBfileNum,4);
        Preonset3=Precycle_data(PreNOBfileNum,5);
        Preoffset3=Precycle_data(PreNOBfileNum,6);
        
        %
        
        switch EndPoint
            case 1
                Preendset=Preoffset1;
            case 2
                Preendset=Preonset2;
            case 3
                Preendset=Preoffset2;
            case 4
                Preendset=Preonset3;
            case 5
                Preendset=Preoffset3;
        end
        
        PresubArry=PreNOBfile(Preonset1:Preendset,:);
        PresubCGArry=PreCGFile(Preonset1:Preendset,:);
        
        if isempty(PreData_acrossTrial) | isequal(size(PresubArry,1),size(PreData_acrossTrial,1))
            PreData_acrossTrial=cat(3,PreData_acrossTrial,PresubArry);
            PreCGData_acrossTrial=cat(3,PreCGData_acrossTrial,PresubCGArry);
        else
            k=size(PresubArry,1)-size(PreData_acrossTrial,1);
            if k<0
                for j=1:abs(k)
                    PresubArry=[PresubArry;PresubArry(end,:)];
                    PresubCGArry=[PresubCGArry;PresubCGArry(end,:)];
                end
                PreData_acrossTrial=cat(3,PreData_acrossTrial,PresubArry);
                PreCGData_acrossTrial=cat(3,PreCGData_acrossTrial,PresubCGArry);
            end
            if k>0
                PresubArry(end-k+1:end,:)=[];
                PresubCGArry(end-k+1:end,:)=[];
                PreData_acrossTrial=cat(3,PreData_acrossTrial,PresubArry);
                PreCGData_acrossTrial=cat(3,PreCGData_acrossTrial,PresubCGArry);
            end
        end
        end
    end
    
    % load post test
    for PostNOBfileNum=1:size(Postcycle_header,1)

        invalidFiles=find(Postcycle_data(PostNOBfileNum,1:6)==-999999);
        if (isempty(invalidFiles))

        
        %load glove data
        PostCGFilename=regexprep(char(Postcycle_header(PostNOBfileNum,1)),'NOB',glovePrefix);
        PostCGFile_tmp=load([postDir '\' PostCGFilename]);
        PostCGFile=calibration(PostCGFile_tmp,PostCalibration1,PostCalibration2,PostCalibration3);

        %load NOB data
        PostNOBfilename=[postDir '\' char(Postcycle_header(PostNOBfileNum,1))];
        PostNOBfile=load(PostNOBfilename);
        
        Postonset1=Postcycle_data(PostNOBfileNum,1);
        Postoffset1=Postcycle_data(PostNOBfileNum,2);
        Postonset2=Postcycle_data(PostNOBfileNum,3);
        Postoffset2=Postcycle_data(PostNOBfileNum,4);
        Postonset3=Postcycle_data(PostNOBfileNum,5);
        Postoffset3=Postcycle_data(PostNOBfileNum,6);
        
        switch EndPoint
            case 1
                Postendset=Postoffset1;
            case 2
                Postendset=Postonset2;
            case 3
                Postendset=Postoffset2;
            case 4
                Postendset=Postonset3;
            case 5
                Postendset=Postoffset3;
        end
        
        PostsubArry=PostNOBfile(Postonset1:Postendset,:);
        PostsubCGArry=PostCGFile(Postonset1:Postendset,:);
        if isempty(PostData_acrossTrial) | isequal(size(PostsubArry,1),size(PostData_acrossTrial,1))
            PostData_acrossTrial=cat(3,PostData_acrossTrial,PostsubArry);
            PostCGData_acrossTrial=cat(3,PostCGData_acrossTrial,PostsubCGArry);
        else
            k=size(PostsubArry,1)-size(PostData_acrossTrial,1);
            if k<0
                for j=1:abs(k)
                    PostsubArry=[PostsubArry;PostsubArry(end,:)];
                    PostsubCGArry=[PostsubCGArry;PostsubCGArry(end,:)];
                end
                PostData_acrossTrial=cat(3,PostData_acrossTrial,PostsubArry);
                PostCGData_acrossTrial=cat(3,PostCGData_acrossTrial,PostsubCGArry);
            end
            if k>0
                PostsubArry(end-k+1:end,:)=[];
                PostsubCGArry(end-k+1:end,:)=[];
                PostData_acrossTrial=cat(3,PostData_acrossTrial,PostsubArry);
                PostCGData_acrossTrial=cat(3,PostCGData_acrossTrial,PostsubCGArry);
            end
        end
        end
    end
    switch valName
        case 1 % elbow angle
            [meanPre_acrossTrial, meanPost_acrossTrial]=Elbow_Angle(PreData_acrossTrial,PostData_acrossTrial);
            catigory='elbow angle';
        case 2 % trunk movement
            [meanPre_acrossTrial, meanPost_acrossTrial]=Trunk_Movement(PreData_acrossTrial,PostData_acrossTrial);
            catigory='Trunk Movement';
        case 3 % finger angle
            [meanPre_acrossTrial, meanPost_acrossTrial]=Finger_Angle(PreCGData_acrossTrial,PostCGData_acrossTrial);
            catigory='Finger Angle (index MCP)';
           
    end
    title(strtrim(subjectList(subjectName(Num)+2,:)));
    finalTitleName = [finalTitleName ', ' strtrim(subjectList(subjectName(Num)+2,:))];
    alpha(0.5)

    
    % merg mean data from multisubjects
    % pre test
    if isempty(PreAcrossSubj) | isequal(size(meanPre_acrossTrial,1),size(PreAcrossSubj,1))
        PreAcrossSubj=cat(3,PreAcrossSubj,meanPre_acrossTrial);
    else
        k=size(meanPre_acrossTrial,1)-size(PreAcrossSubj,1);
        if k<0
            for j=1:abs(k)
                meanPre_acrossTrial=[meanPre_acrossTrial;meanPre_acrossTrial(end,:)];
            end
            PreAcrossSubj=cat(3,PreAcrossSubj,meanPre_acrossTrial);
        end
        if k>0
            meanPre_acrossTrial(end-k+1:end,:)=[];
            PreAcrossSubj=cat(3,PreAcrossSubj,meanPre_acrossTrial);
        end
    end
    
    % post test
    if isempty(PostAcrossSubj) | isequal(size(meanPost_acrossTrial,1),size(PostAcrossSubj,1))
        PostAcrossSubj=cat(3,PostAcrossSubj,meanPost_acrossTrial);
    else
        k=size(meanPost_acrossTrial,1)-size(PostAcrossSubj,1);
        if k<0
            for j=1:abs(k)
                meanPost_acrossTrial=[meanPost_acrossTrial;meanPost_acrossTrial(end,:)];
            end
            PostAcrossSubj=cat(3,PostAcrossSubj,meanPost_acrossTrial);
        end
        if k>0
            meanPost_acrossTrial(end-k+1:end,:)=[];
            PostAcrossSubj=cat(3,PostAcrossSubj,meanPost_acrossTrial);
        end
    end
end


figure
% calculate average cross subjects
% pretest
    Pre_mean_acrossSubj=mean(PreAcrossSubj(:,:),2);
    Pre_std_acrossSubj=std(PreAcrossSubj(:,:),0,2);
    
    Pre_up_acrossSubj=Pre_mean_acrossSubj+Pre_std_acrossSubj;
    Pre_down_acrossSubj=Pre_mean_acrossSubj-Pre_std_acrossSubj;
    
    pre_acrossSubj_x1=1:1:size(Pre_up_acrossSubj);
    pre_acrossSubj_x2=1:1:size(Pre_down_acrossSubj);
    pre_acrossSubj_X = [pre_acrossSubj_x1 fliplr(pre_acrossSubj_x2)];
    pre_acrossSubj_Y = [Pre_up_acrossSubj' fliplr(Pre_down_acrossSubj')];
    fill(pre_acrossSubj_X,pre_acrossSubj_Y,[0.5 0.5 0.5]);
    hold on
    
    
% calculate average cross subjects
% posttest
    Post_mean_acrossSubj=mean(PostAcrossSubj(:,:),2);
    Post_std_acrossSubj=std(PostAcrossSubj(:,:),0,2);
    
    Post_up_acrossSubj=Post_mean_acrossSubj+Post_std_acrossSubj;
    Post_down_acrossSubj=Post_mean_acrossSubj-Post_std_acrossSubj;
    
    Post_acrossSubj_x1=1:1:size(Post_up_acrossSubj);
    Post_acrossSubj_x2=1:1:size(Post_down_acrossSubj);
    Post_acrossSubj_X = [Post_acrossSubj_x1 fliplr(Post_acrossSubj_x2)];
    Post_acrossSubj_Y = [Post_up_acrossSubj' fliplr(Post_down_acrossSubj')];
    fill(Post_acrossSubj_X,Post_acrossSubj_Y,[0.8 0 1]);
    alpha(0.5)
    title([catigory '(' finalTitleName ')']);
