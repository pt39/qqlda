function unImpaired_Finger_Velocity()
clear all
close all
clc

[data text] = xlsread('../results/DataSheet/RTG ALL with average 11_07_2013_imp_unImp.xls','Unimpaired Latest Finger+elbow');

condition_old = [];
figure

for i=1:size(data,1)
    subInit = char(text(i+1,1));
    group_temp = char(text(i+1,2));
    test = char(text(i+1,3));
    hand = char(text(i+1,4));
    object = char(text(i+1,6));
    filename = char(text(i+1,7));
    
    switch group_temp
        case 'HAT'
            group = 'HAT Cycled';
        case 'HAS'
            group = 'HAS Cycled';
        case 'Controls'
            group = 'Control Group Cycled';
    end
    
    onset1 = data(i,32)/10;
    offset1 = data(i,33)/10;
    onset2 = data(i,34)/10;
    offset2 = data(i,35)/10;
    
    condition_new = ['../CYCLED_GoodSide/' group '/' subInit '/' hand '/' object];
    
    if strcmp(condition_old, condition_new)
        hold on
    else
        saveppt('unImpaired_RTGHandPIP_Velocity.ppt');
        close all
        figure;
              
    end
    
    if strcmp(hand,'right')
        handPrefix='RCG';
    else
        handPrefix = 'LCG';
    end
    
    CGfilename = [handPrefix filename(4:end)];
    fileDirectory = ['../CYCLED_GoodSide/' group '/' subInit '/' test '/' hand '/' object '/' CGfilename];
    CGfile = load(fileDirectory);
    [b,a]=butter(2,8/50);
    GBFileData = filtfilt(b,a,CGfile);
    
    FingerVelocity = diff(GBFileData(1:onset2,6))/100;
    
    if (strcmp(test,'pretest'))
        plot(FingerVelocity);
        legend('PIP');
    else
        plot(FingerVelocity,'r');
    end
    title(condition_new);
    condition_old =condition_new;
    
end
    
    
