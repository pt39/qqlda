function [trunkDist ElbowAngleDif ElbowAngleStart ElbowAngleEnd]=trunkElbowMove(data)

trunkDist = sqrt(((data(1,19)-data(end,19))*(data(1,19)-data(end,19)))+((data(1,20)-data(end,20))*(data(1,20)-data(end,20)))+((data(1,21)-data(end,21))*(data(1,21)-data(end,21))));

V_elbow_shoulder=data(:,13:15)-data(:,7:9);
V_elbow_wrist=data(:,1:3)-data(:,7:9);

% magnitude
for (i=1:size(V_elbow_shoulder,1))
    M_elbow_shoulder(i)=norm(V_elbow_shoulder(i,:));
    M_elbow_wrist(i)=norm(V_elbow_wrist(i,:));
end

for (j=1:size(V_elbow_shoulder,1))
    elbowAngle_list(j)=acosd(dot(V_elbow_shoulder(j,:),V_elbow_wrist(j,:))/(M_elbow_shoulder(j)*M_elbow_wrist(j)));
end

ElbowAngleStart=elbowAngle_list(1);
ElbowAngleEnd = elbowAngle_list(end);
ElbowAngleDif=elbowAngle_list(end)-elbowAngle_list(1);
