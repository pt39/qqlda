
function [Pre_mean_FingerAng, Post_mean_FingerAng]=Finger_Angle(PreData_acrossTrial,PostData_acrossTrial)
% calculate trunk movement, distance to start point ( first point)
% 

% pretest

for preCount=1:size(PreData_acrossTrial,3)
    Pre_FingerAng(:,preCount)=PreData_acrossTrial(:,5,preCount);
end

Pre_mean_FingerAng=mean(Pre_FingerAng(:,1:preCount),2);
Pre_std_FingerAng=std(Pre_FingerAng(:,1:preCount),0,2);

Pre_up_FingerAng=Pre_mean_FingerAng+Pre_std_FingerAng;
Pre_down_FingerAng=Pre_mean_FingerAng-Pre_std_FingerAng;

pre_x1=1:1:size(Pre_up_FingerAng);
pre_x2=1:1:size(Pre_down_FingerAng);
pre_X = [pre_x1 fliplr(pre_x2)];
pre_Y = [Pre_up_FingerAng' fliplr(Pre_down_FingerAng')];
fill(pre_X,pre_Y,[0.5 0.5 0.5]);
hold on

% 
% posttest
for postCount=1:size(PostData_acrossTrial,3)
    Post_FingerAng(:,preCount)=PostData_acrossTrial(:,5,postCount);
end

Post_mean_FingerAng=mean(Post_FingerAng(:,1:postCount),2);
Post_std_FingerAng=std(Post_FingerAng(:,1:postCount),0,2);

Post_up_FingerAng=Post_mean_FingerAng+Post_std_FingerAng;
Post_down_FingerAng=Post_mean_FingerAng-Post_std_FingerAng;

post_x1=1:1:size(Post_up_FingerAng);
post_x2=1:1:size(Post_down_FingerAng);
post_X = [post_x1 fliplr(post_x2)];
post_Y = [Post_up_FingerAng' fliplr(Post_down_FingerAng')];
fill(post_X,post_Y,[0.8 0 1]);
