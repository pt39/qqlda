
function HAS_HAT_RTG_kinematics
% load HAS HAT kinematics excel file
clc
[A1_output, A1_headertext] = xlsread('Y:\LDA\HAS HAT NonAveragecycledResult All for Correlation.xls');
count = 1;

%find seperate group data
for groupNum=1:2
    groupind=find(A1_output(:,1)==groupNum); 
    groupData=A1_output(groupind(1:end),:); 
    
    switch groupNum
        case 1
            GroupName='HAT';
        case 2
            GroupName='HAS';
        case 3
            GroupName='Controle';
    end
    
    % find seperate test data
    for testNum=2:3
        testind=find(groupData(:,2)==testNum);
        testData=groupData(testind(1:end),:); 
        
        switch testNum
            case 2
                TestName='preTest';
            case 3
                TestName='postTest';
        end
        
        % find seperate impaired data
        for impairedNum=1:2
            impairedind=find(testData(:,3)==impairedNum);
            impairedData=testData(impairedind(1:end),:);
            
            switch impairedNum
                case 1
                    ImpairedName='Healthy';
                case 2
                    ImpairedName='Impaired';
            end
            
            % find seperate object data
            for objectNum=1:4
                objectind=find(impairedData(:,4)==objectNum);
                objectData=impairedData(objectind(1:end),:); 
                
                switch objectNum
                    case 1
                        ObjectName='bigcube';
                    case 2
                        ObjectName='bigcircle';
                    case 3
                        ObjectName='smallcube';
                    case 4
                        ObjectName='smallcircle';
                end
                
                % find each subject
                NumList=unique(objectData(:,5));
                for subjectNum=NumList(1):NumList(end)
                    subjectind=find(objectData(:,5)==subjectNum);
                    if size(subjectind,1)>0
                        subjectData=objectData(subjectind(1:end),:);
                        
                        
                        % calculate correlation between each parameter
                        sheetname=[GroupName TestName ImpairedName ObjectName num2str(subjectNum)]
                        correFinal(count).name=sheetname;
                        correFinal(count).group=groupNum;
                        correFinal(count).test=testNum;
                        correFinal(count).impaired=impairedNum;
                        correFinal(count).object=objectNum;
                        correFinal(count).correlationVal(1)=corr(subjectData(:,7),subjectData(:,13));
                        correFinal(count).correlationVal(2)=corr(subjectData(:,7),subjectData(:,15));
                        correFinal(count).correlationVal(3)=corr(subjectData(:,9),subjectData(:,13));
                        correFinal(count).correlationVal(4)=corr(subjectData(:,9),subjectData(:,15));
                        count =count+1;
                    end
                end
            end
            
        end
            
    end
        
end

finalCorrelationsheet={'name' 'group' 'test' 'impaired' 'object' 'ATTPV vs. HAperRT' 'ATTPV vs. HTTPAper' 'ATTDecc vs. HAperRT'	'ATTDecc vs. HTTPAper'};

for countNum=1:count-1
    finalCorrelationsheet= cat(1,finalCorrelationsheet,{correFinal(countNum).name correFinal(countNum).group correFinal(countNum).test correFinal(countNum).impaired correFinal(countNum).object correFinal(countNum).correlationVal(1) correFinal(countNum).correlationVal(2) correFinal(countNum).correlationVal(3) correFinal(countNum).correlationVal(4)});
end

xlswrite('finalCorrelationsheetNew.xls',finalCorrelationsheet);
% caculate correlation between each two parameters
