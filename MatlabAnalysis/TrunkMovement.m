

function TrunkMovement(pathname)
clc
close all

groupList=ls(pathname);

for GroupNum=3:size(groupList,1)
    groupDir=[pathname '\' strtrim(groupList(GroupNum,:))];
    
    subjectList = ls(groupDir);
    
    for subjectNum=3:size(subjectList,1)
        subjectDir=[groupDir '\' strtrim(subjectList(subjectNum,:))];
        
        for testNum=1:3
            switch testNum
                case 1
                    testDir=[subjectDir '\pretest'];
                case 2
                    testDir=[subjectDir '\posttest'];
            end
            for handNum=1:2
                switch handNum
                    case 1
                        handDir=[testDir '\left'];
                    case 2
                        handDir=[testDir '\right'];
                end
                
                for objectNum = 1:4
                    switch objectNum
                        case 1
                            objectDir=[handDir '\bigcube'];
                        case 2
                            objectDir=[handDir '\bigcircle'];
                        case 3
                            objectDir=[handDir '\smallcube'];
                        case 4
                            objectDir=[handDir '\smallcircle'];
                    end
                    
                    CyclefileName=[objectDir '\Cycled.xls'];
                    [cycle_data, cycle_header]=xlsread(CyclefileName);
                    
                    for NOBfileNum=1:size(cycle_header,1)
                        NOBfilename=[objectDir '\' char(cycle_header(NOBfileNum,1))];
                        NOBfile=load(NOBfilename);
                        
                        onset1=cycle_data(NOBfileNum,1);
                        offset1=cycle_data(NOBfileNum,2);
                        onset2=cycle_data(NOBfileNum,3);
                        offset2=cycle_data(NOBfileNum,4);
                        
                        subArry=NOBfile(onset1:onset2,:);
                        
                        X=subArry(:,19);
                        Y=subArry(:,20);
                        Z=subArry(:,21);
                        
                        % calculate trunk path length
                        for n=1:length(X)-1
                            A(n)=sqrt((X(n+1)-X(n))^2+(Y(n+1)-Y(n))^2+(Z(n+1)-Z(n))^2);
                        end
                        
                        for n=2:length(X)-1
                            A(n)=A(n-1) + A(n);
                        end
                        
                        A=[A(1); A'];
                        TrunkTrajectoryLength=A(length(X));
                        
                        % calculate trunk move distance
                        TrunkMoveDistance=sqrt((X(1)-X(end))^2+(Y(1)-Y(end))^2+(Z(1)-Z(end))^2);
                        
                        % calculate elbow angle change
                        % Vector shoulder-> elbow
                        V_elbow_shoulder=subArry(:,13:15)-subArry(:,7:9);
                        V_elbow_wrist=subArry(:,1:3)-subArry(:,7:9);
                        
                        % magnitude 
                        for (i=1:size(V_elbow_shoulder,1))
                            M_elbow_shoulder(i)=norm(V_elbow_shoulder(i,:));
                            M_elbow_wrist(i)=norm(V_elbow_wrist(i,:));
                        end
                        
                        for (j=1:size(V_elbow_shoulder,1))
                            elbowAngle(j)=acosd(dot(V_elbow_shoulder(j,:),V_elbow_wrist(j,:))/(M_elbow_shoulder(j)*M_elbow_wrist(j)));
                        end
                        
                    end
                end
            end
        end
        
    end
end