function [dYY]=procfilt(YY, fps)

lw=8;
m=length(YY);

wc=tan(pi*lw/fps);
den=1.0+wc*(wc+sqrt(2));
ca=(wc*wc)/den;
cb=-2*(wc*wc-1)/den;
cc=-(1+wc*(wc-sqrt(2)))/den;
mh=round(m/2)-1;
mmh=m+mh;
m2h=mmh+mh;

xd(1)=0;
for(i=2:m)
   xd(i)=YY(i-1);
end
x0=xd(2);
sx=(xd(m)-x0)/(m-1);

for(i=2:m)
   x(i+mh)=xd(i)-(i-1)*sx-x0;
end

for(i=(mmh+1):m2h)
   x(i)=-x(mmh-(i-mmh+1)+2);
end

for(i=2:mh)
   x(i)=-x(2*mh-i+2);
end

for(i=1:2)
   xd(2)=x(2);
   xd(3)=x(3);
   for(j=4:m2h)
      xd(j)=ca*(x(j)+2*x(j-1)+x(j-2))+cb*xd(j-1)+cc*xd(j-2);
   end
   
   for(j=2:m2h)
      x(j)=xd(m2h+1-j);
   end
end

for(i=mh+1:mmh)
   dYY(i-mh)=x(i)+sx*(i-mh-1)+x0;
end

