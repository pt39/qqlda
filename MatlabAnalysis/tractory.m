function [Duration,Cumpath,tsmooth]=tractory(Yf)

X=Yf(:,1);
Y=Yf(:,2);
Z=Yf(:,3);
SF=100;

% calculate path length
for n=1:length(X)-1
    A(n)=sqrt((X(n+1)-X(n))^2+(Y(n+1)-Y(n))^2+(Z(n+1)-Z(n))^2);
end

for n=2:length(X)-1
    A(n)=A(n-1) + A(n);
end

A=[A(1); A'];
Cumpath=A(length(X));

% calculate movement duration
Duration=(1/SF)*(length(X));

% calculate smoothness
Arm=[X,Y,Z];
If=size(Arm,1);
handdif1=difnum(Arm,3,If,.01,1,1,1);
Tangvel=(sqrt(sum((handdif1.^2)')))';
Tangvel=procfilt(Tangvel,SF);
Tangvel=Tangvel';
handdif2=difnumt(Tangvel,.01);
Acceleration=procfilt(handdif2,SF);
Tangacc=Acceleration';
handdif3=difnumt(Tangacc,.01);
Jerk=procfilt(handdif3,SF);
Tangjerk=Jerk';
q=Tangjerk.^2;
integrq=integrfixed(q,1/SF);
tsmooth=sqrt(.5*integrq*Duration.^5/Cumpath.^2);
