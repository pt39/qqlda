
 function res=NORMALIZE(subjectNAME,LDAcurve)

%%
%find a way to load offset1 for each subject.
[offsets TEXT]=xlsread('offset1AVG_bigcircle.xlsx','HAT');
Indices = find(strcmpi(subjectNAME,TEXT))
    %%
    c=1;%if it is pre impaired then c=1,if post impaired then c=2,pre unimpaired c=3,post unimpaired c=4.
    offset1=offsets(Indices-1,c);
    res = resample(LDAcurve(1:floor(offset1)),100,floor(offset1));
%     a=offset1/100;
%     index1=1:a:offset1
    
 end