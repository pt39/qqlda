function Ix=integrfixed(x, dt)

N=length(x);
Ix=0;
for(i=1:N-1)
     Ix=Ix+dt*(x(i)+0.5*(x(i)-x(i+1)));
end

