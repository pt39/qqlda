close all
clear all

for ka=3:3
     close all
     
 
     
switch ka
                   
                    case 1
                         Object1 = ['bigcircle'];
                         Object2 = ['smallcube'];
                         excelname='AVG2bigcircle smallcube.xlsx';
                         pptname='AVG2bigcircle smallcube';
                         
                    case 2
                         Object1 = ['bigcircle'];
                         Object2 = ['smallcircle'];
                         excelname='AVG2bigcircle smallcircle.xlsx';
                         pptname='AVG2bigcircle smallcircle';
                         
                    case 3
                         Object1 = ['bigcube'];
                         Object2 = ['bigcircle'];
                         excelname='AVG2bigcube bigcircle.xlsx';
                         pptname='AVG2bigcube bigcircle';
                        name1='LDA(control group)_bigcube bigcicle.xlsx'
                    case 4
                         Object1 = ['smallcube'];
                         Object2 = ['smallcircle'];
                         excelname='AVG2smallcube smallcircle.xlsx';
                         pptname='AVG2smallcube smallcircle';
                         
                    case 5
                         Object1 = ['smallcube'];
                         Object2 = ['bigcube'];
                         excelname='AVG2smallcube bigcube.xlsx';
                         pptname='AVG2smallcube bigcube';
                         
                     case 6
                         Object1 = ['bigcube'];
                         Object2 = ['smallcircle'];
                         excelname='AVG2bigcube smallcircle.xlsx';
                          pptname='AVG2bigcube smallcircle';

end

%%
%
%%
%pre impaired
A1=xlsread(excelname,'AN','B2:B250');
A2=xlsread(excelname,'BL','B2:B250');
A3=xlsread(excelname,'DB','B2:B250');
A4=xlsread(excelname,'KM','B2:B250');
A5=xlsread(excelname,'KS','B2:B250');
A6=xlsread(excelname,'MB','B2:B250');
A7=xlsread(excelname,'MD','B2:B250');
A8=xlsread(excelname,'MT','B2:B250');
A9=xlsread(excelname,'RL','B2:B250');




%this part is to concat all coloumns and then
figure(1)
plot(A1,'y','LineWidth',1.5)
hold on
plot(A2,'g','LineWidth',1.5)
hold on
plot(A3,'m','LineWidth',1.5)
hold on
plot(A4,'b','LineWidth',1.5)
hold on
plot(A5,'k','LineWidth',1.5)
hold on
plot(A6,'c','LineWidth',1.5)
hold on
plot(A7,'k--','LineWidth',1.5)
hold on
plot(A8,'m--','LineWidth',1.5)
hold on
plot(A9,'b--','LineWidth',1.5)



 X1 = [A1 A2 A3 A4 A5 A6 A7 A8 A9];
 for i=1:size(X1,1)
     
 
avg_LDA1(i)= mean(X1(i,:));
 end
 avg_preImpaired=avg_LDA1';
 hold on
 plot(avg_preImpaired,'r','LineWidth',3.5)
 legend('AN','BL','DB','KM','KS','MB','MD','MT','RL')
 xlswrite(name1,A1,'PRE IMPAIRED','B2');
 xlswrite(name1,A2,'PRE IMPAIRED','C2');
 xlswrite(name1,A3,'PRE IMPAIRED','D2');
 xlswrite(name1,A4,'PRE IMPAIRED','E2');
 xlswrite(name1,A5,'PRE IMPAIRED','F2');
 xlswrite(name1,A6,'PRE IMPAIRED','G2');
 xlswrite(name1,A7,'PRE IMPAIRED','H2');
 xlswrite(name1,A8,'PRE IMPAIRED','I2');
 xlswrite(name1,A9,'PRE IMPAIRED','J2');
 xlswrite(name1,avg_preImpaired,'PRE IMPAIRED','K2');
 index2=1:1:300
 xlswrite(name1,index2','PRE IMPAIRED','A2');
 
%%
%post impaired

C1=xlsread(excelname,'AN','C2:C250');
C2=xlsread(excelname,'BL','C2:C250');
C3=xlsread(excelname,'DB','C2:C250');
C4=xlsread(excelname,'KM','C2:C250');
C5=xlsread(excelname,'KS','C2:C250');
C6=xlsread(excelname,'MB','C2:C250');
C7=xlsread(excelname,'MD','C2:C250');
C8=xlsread(excelname,'MT','C2:C250');
C9=xlsread(excelname,'RL','C2:C250');



%
figure(2)
plot(C1,'y','LineWidth',1.5)
hold on
plot(C2,'g','LineWidth',1.5)
hold on
plot(C3,'m','LineWidth',1.5)
hold on
plot(C4,'b','LineWidth',1.5)
hold on
plot(C5,'k','LineWidth',1.5)
hold on
plot(C6,'c','LineWidth',1.5)
hold on
plot(C7,'k--','LineWidth',1.5)
hold on
plot(C8,'m--','LineWidth',1.5)
hold on
plot(C9,'b--','LineWidth',1.5)

 X2 = [C1 C2 C3 C4 C5 C6 C7 C8 C9];
 for i=1:size(X2,1)
     
 
avg_LDA2(i)= mean(X2(i,:));
 end
 avg_postImpaired=avg_LDA2';
 hold on
 plot(avg_postImpaired,'r','LineWidth',3.5)
 legend('AN','BL','DB','KM','KS','MB','MD','MT','RL')
 xlswrite(name1,C1,'POST IMPAIRED','B2');
 xlswrite(name1,C2,'POST IMPAIRED','C2');
 xlswrite(name1,C3,'POST IMPAIRED','D2');
 xlswrite(name1,C4,'POST IMPAIRED','E2');
 xlswrite(name1,C5,'POST IMPAIRED','F2');
 xlswrite(name1,C6,'POST IMPAIRED','G2');
 xlswrite(name1,C7,'POST IMPAIRED','H2');
 xlswrite(name1,C8,'POST IMPAIRED','I2');
 xlswrite(name1,C9,'POST IMPAIRED','J2');
 
 xlswrite(name1,avg_postImpaired,'POST IMPAIRED','K2');
 xlswrite(name1,index2','POST IMPAIRED','A2');
 

%%
%PRE UNIMPAIRED
D1=xlsread(excelname,'AN','D2:D250');
D2=xlsread(excelname,'BL','D2:D250');
D3=xlsread(excelname,'DB','D2:D250');
D4=xlsread(excelname,'KM','D2:D250');
D5=xlsread(excelname,'KS','D2:D250');
D6=xlsread(excelname,'MB','D2:D250');
D7=xlsread(excelname,'MD','D2:D250');
D8=xlsread(excelname,'MT','D2:D250');
D9=xlsread(excelname,'RL','D2:D250');



%this part is to concat all coloumns and then
figure(3)
plot(D1,'y','LineWidth',1.5)
hold on
plot(D2,'g','LineWidth',1.5)
hold on
plot(D3,'m','LineWidth',1.5)
hold on
plot(D4,'b','LineWidth',1.5)
hold on
plot(D5,'k','LineWidth',1.5)
hold on
plot(D6,'c','LineWidth',1.5)
hold on
plot(D7,'k--','LineWidth',1.5)
hold on
plot(D8,'m--','LineWidth',1.5)
hold on
plot(D9,'b--','LineWidth',1.5)


 X3 = [D1 D2 D3 D4 D5 D6 D7 D8 D9];
 for i=1:size(X3,1)
     
 
avg_LDA3(i)= mean(X3(i,:));
 end
 avg_preUnimpaired=avg_LDA3';
 hold on
 plot(avg_preUnimpaired,'r','LineWidth',3.5)
 legend('AN','BL','DB','KM','KS','MB','MD','MT','RL')
 xlswrite(name1,D1,'PRE UNIMPAIRED','B2');
 xlswrite(name1,D2,'PRE UNIMPAIRED','C2');
 xlswrite(name1,D3,'PRE UNIMPAIRED','D2');
 xlswrite(name1,D4,'PRE UNIMPAIRED','E2');
 xlswrite(name1,D5,'PRE UNIMPAIRED','F2');
 xlswrite(name1,D6,'PRE UNIMPAIRED','G2');
 xlswrite(name1,D7,'PRE UNIMPAIRED','H2');
 xlswrite(name1,D8,'PRE UNIMPAIRED','I2');
 xlswrite(name1,D9,'PRE UNIMPAIRED','J2');
 
 xlswrite(name1,avg_preUnimpaired,'PRE UNIMPAIRED','K2');
 xlswrite(name1,index2','PRE UNIMPAIRED','A2');
 %%
 %post unimpaired
E1=xlsread(excelname,'AN','E2:E250');
E2=xlsread(excelname,'BL','E2:E250');
E3=xlsread(excelname,'DB','E2:E250');
E4=xlsread(excelname,'KM','E2:E250');
E5=xlsread(excelname,'KS','E2:E250');
E6=xlsread(excelname,'MB','E2:E250');
E7=xlsread(excelname,'MD','E2:E250');
E8=xlsread(excelname,'MT','E2:E250');
E9=xlsread(excelname,'RL','E2:E250');

%this part is to concat all coloumns and then
figure(4)
plot(E1,'y','LineWidth',1.5)
hold on
plot(E2,'g','LineWidth',1.5)
hold on
plot(E3,'m','LineWidth',1.5)
hold on
plot(E4,'b','LineWidth',1.5)
hold on
plot(E5,'k','LineWidth',1.5)
hold on
plot(E6,'c','LineWidth',1.5)
hold on
plot(E7,'k--','LineWidth',1.5)
hold on
plot(E8,'m--','LineWidth',1.5)
hold on
plot(E9,'b--','LineWidth',1.5)


 X4 = [E1 E2 E3 E4 E5 E6 E7 E8 E9];
 for i=1:size(X4,1)
    
 
avg_LDA4(i)= mean(X4(i,:));
 end
 avg_postUnimpaired=avg_LDA4';
 hold on
 plot(avg_postUnimpaired,'r','LineWidth',3.5)
 legend('AN','BL','DB','KM','KS','MB','MD','MT','RL')
 xlswrite(name1,E1,'POST UNIMPAIRED','B2');
 xlswrite(name1,E2,'POST UNIMPAIRED','C2');
 xlswrite(name1,E3,'POST UNIMPAIRED','D2');
 xlswrite(name1,E4,'POST UNIMPAIRED','E2');
 xlswrite(name1,E5,'POST UNIMPAIRED','F2');
 xlswrite(name1,E6,'POST UNIMPAIRED','G2');
 xlswrite(name1,E7,'POST UNIMPAIRED','H2');
 xlswrite(name1,E8,'POST UNIMPAIRED','I2');
 xlswrite(name1,E9,'POST UNIMPAIRED','J2');
 
 xlswrite(name1,avg_postUnimpaired,'POST UNIMPAIRED','K2');
 xlswrite(name1,index2','POST UNIMPAIRED','A2');

%%
index=1:1:500;
xlswrite(excelname,avg_preImpaired,'AVG','B2');
xlswrite(excelname,avg_postImpaired,'AVG','C2');
xlswrite(excelname,avg_preUnimpaired,'AVG','D2');
xlswrite(excelname,avg_postUnimpaired,'AVG','E2');
xlswrite(excelname,index','AVG','A2');

saveppt(pptname,'Pretest impaired hand','-f1')
saveppt(pptname,'posttest impaired hand','-f2')
saveppt(pptname,'pretest unimpaired hand','-f3')  
saveppt(pptname,'posttest unimpaired hand','-f4')


end



