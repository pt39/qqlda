function [ShoulderAngleStart ShoulderAngleEnd ShoulderAngleStart_hori ShoulderAngleEnd_hori ElbowAngleStart ElbowAngleEnd]=ShoulderElbowMove(data)

V_shoulder_trunk=data(:,19:2:21)-data(:,13:2:15);
V_shoulder_elbow=data(:,7:2:9)-data(:,13:2:15);

V_elbow_shoulder=data(:,13:14)-data(:,7:8);
V_elbow_wrist=data(:,1:2)-data(:,7:8);

V_shoulder_trunk_hori=data(:,19:20)-data(:,13:14);
V_shoulder_elbow_hori=data(:,7:8)-data(:,13:14);


% magnitude
for (i=1:size(V_elbow_shoulder,1))
    M_shoulder_trunk(i)=norm(V_shoulder_trunk(i,:));
    M_shoulder_elbow(i)=norm(V_shoulder_elbow(i,:));
    
    M_elbow_shoulder(i)=norm(V_elbow_shoulder(i,:));
    M_elbow_wrist(i)=norm(V_elbow_wrist(i,:));
    
    M_shoulder_trunk_hori(i)=norm(V_shoulder_trunk_hori(i,:));
    M_shoulder_elbow_hori(i)=norm(V_shoulder_elbow_hori(i,:));
end

for (j=1:size(V_elbow_shoulder,1))
    elbowAngle_list(j)=acosd(dot(V_elbow_shoulder(j,:),V_elbow_wrist(j,:))/(M_elbow_shoulder(j)*M_elbow_wrist(j)));
    shoulderAngle_list(j)=acosd(dot(V_shoulder_trunk(j,:),V_shoulder_elbow(j,:))/(M_shoulder_trunk(j)*M_shoulder_elbow(j)));
    shoulderAngle_list_hori(j)=acosd(dot(V_shoulder_trunk_hori(j,:),V_shoulder_elbow_hori(j,:))/(M_shoulder_trunk_hori(j)*M_shoulder_elbow_hori(j)));
end

ElbowAngleStart=elbowAngle_list(1);
ElbowAngleEnd = elbowAngle_list(end);

ShoulderAngleStart=shoulderAngle_list(1);
ShoulderAngleEnd = shoulderAngle_list(end);

ShoulderAngleStart_hori=shoulderAngle_list_hori(1);
ShoulderAngleEnd_hori = shoulderAngle_list_hori(end);