function cycling_result_combine(pathname1)

clc

outputList={'init' 'hand' 'test' 'group' 'object'  'M1_PTP1','M1_PV1','M1_TPV1','M1_TAPV1','M2_PTP2','M2_PV2','M2_TPV2','M2_TAPV2','M3_PTP3','M3_PV3','M3_TPV3','M3_TAPV3','M4_PTP4','M4_PV4','M4_TPV4','M4_TAPV4','Onset1','Offset1','Onset2','Offset2','Onset3','Offset3','Onset4','Offset4'};

for catiList=3:3
    switch catiList
        case 1
            Catigory = ['processed HAS'];
            CatiName = ['HAS'];
        case 2
            Catigory = ['processed HAT'];
            CatiName = ['HAT'];
        case 3
            Catigory = ['Processed Controls'];
            CatiName = ['Controls'];
    end
    
    subject_list = ls([pathname1 '\' Catigory]);
    totalCount=1;
    
    %for j = 3:size(subject_list,1)
    for j =2:size(subject_list,1)
        pathname = [pathname1 '\' Catigory '\' strtrim(subject_list(j,:))];
        for i=1:3
            for m=1:2
                for k=1:5
                    switch m
                        case 1
                            impairedHand = ['left'];
                            glovePre=['LCG'];
                        case 2
                            impairedHand = ['right'];
                            glovePre=['RCG'];
                    end
                    
                    
                    switch i
                        case 1
                            dir = ['baseline'];
                        case 2
                            dir = ['pretest'];
                        case 3
                            dir = ['posttest'];
                    end
                    
                    switch k
                        case 1
                            Object = ['bigcube'];
                        case 2
                            Object = ['bigcircle'];
                        case 3
                            Object = ['smallcube'];
                        case 4
                            Object = ['smallcircle'];
                        case 5
                            Object = ['hugecircle'];
                        case 6
                            Object = ['pentagon'];
                        case 7
                            Object = ['wedge'];
                        case 8
                            Object = ['cylinder'];
                    end
                    
                    dirname = [pathname '\' dir '\' impairedHand '\' Object '\OutputData\*1.xls'];
                    files = ls(dirname);
                    if (size(files,1)>=1)
                        fname = [pathname '\' dir '\' impairedHand '\' Object '\OutputData\' files(1,:)]
                        data_output = xlsread(fname);
                        
                        for rowNum=1:size(data_output,1)
                            if size(data_output,2)>20
                             outputList= cat(1,outputList,{strtrim(subject_list(j,:)) impairedHand dir CatiName Object data_output(rowNum,1) data_output(rowNum,2) data_output(rowNum,3) data_output(rowNum,4) data_output(rowNum,5) data_output(rowNum,6) data_output(rowNum,7) data_output(rowNum,8) data_output(rowNum,9) data_output(rowNum,10) data_output(rowNum,11) data_output(rowNum,12) data_output(rowNum,13) data_output(rowNum,14) data_output(rowNum,15) data_output(rowNum,16) data_output(rowNum,17) data_output(rowNum,18) data_output(rowNum,19) data_output(rowNum,20) data_output(rowNum,21) data_output(rowNum,22) data_output(rowNum,23) data_output(rowNum,24)});
                            else
                             outputList= cat(1,outputList,{strtrim(subject_list(j,:)) impairedHand dir CatiName Object data_output(rowNum,1) data_output(rowNum,2) data_output(rowNum,3) 10*(data_output(rowNum,14)-data_output(rowNum,13)-data_output(rowNum,3)) data_output(rowNum,4) data_output(rowNum,5) data_output(rowNum,6) 10*(data_output(rowNum,16)-data_output(rowNum,15)-data_output(rowNum,6)) data_output(rowNum,7) data_output(rowNum,8) data_output(rowNum,9) 10*(data_output(rowNum,18)-data_output(rowNum,17)-data_output(rowNum,9)) data_output(rowNum,10) data_output(rowNum,11) data_output(rowNum,12) data_output(rowNum,12) data_output(rowNum,13) data_output(rowNum,14) data_output(rowNum,15) data_output(rowNum,16) data_output(rowNum,17) data_output(rowNum,18) data_output(rowNum,19) data_output(rowNum,20)});
                            end                                
                        end
%     outputList= cat(1,outputList,{cycleresult(finalcount).init cycleresult(finalcount).initNum cycleresult(finalcount).test cycleresult(finalcount).hand cycleresult(finalcount).object cycleresult(finalcount).ART cycleresult(finalcount).ATTPV cycleresult(finalcount).APV cycleresult(finalcount).ATTDecc cycleresult(finalcount).ApercTTDecc cyclereuslt(finalcount).ATAPVoffset1 cycleresult(finalcount).ATAPVonset2 cycleresult(finalcount).HAperRT cycleresult(finalcount).HPAper cycleresult(finalcount).HTTPAper cycleresult(finalcount).HpercTTPAper});
                        
                    end
                end
            end
        end
    end
end


% outputList={cycleresult(1).init cycleresult(1).test cycleresult(1).hand cycleresult(1).object cycleresult(1).ART cycleresult(1).ATTPV cycleresult(1).APV cycleresult(1).ATTDecc cycleresult(1).ApercTTDecc cyclereuslt(1).ATAPVoffset1 cycleresult(1).ATAPVonset1 cycleresult(1).HAperRT cycleresult(1).HPAper cycleresult(1).HTTPAper cycleresult(1).HpercTTPAper};

% for finalcount=1:totalCount-1
%     cycleresult(finalcount).init
%     outputList= cat(1,outputList,{cycleresult(finalcount).init cycleresult(finalcount).initNum cycleresult(finalcount).test cycleresult(finalcount).hand cycleresult(finalcount).object cycleresult(finalcount).ART cycleresult(finalcount).ATTPV cycleresult(finalcount).APV cycleresult(finalcount).ATTDecc cycleresult(finalcount).ApercTTDecc cyclereuslt(finalcount).ATAPVoffset1 cycleresult(finalcount).ATAPVonset2 cycleresult(finalcount).HAperRT cycleresult(finalcount).HPAper cycleresult(finalcount).HTTPAper cycleresult(finalcount).HpercTTPAper});
% end

xlswrite([pathname1 '\allCombined_updated.xls'],outputList);

