
function [Pre_mean_TrunkDis, Post_mean_TrunkDis]=Trunk_Movement(PreData_acrossTrial,PostData_acrossTrial)
% calculate trunk movement, distance to start point ( first point)
% 

% pretest

for preCount=1:size(PreData_acrossTrial,3)
    
    for (i=1:size(PreData_acrossTrial,1))
        Pre_TrunkDis(i,preCount)=norm(PreData_acrossTrial(i,19:21,preCount)-PreData_acrossTrial(1,19:21,preCount));
    end
end

Pre_mean_TrunkDis=mean(Pre_TrunkDis(:,1:preCount),2);
Pre_std_TrunkDis=std(Pre_TrunkDis(:,1:preCount),0,2);

Pre_up_TrunkDis=Pre_mean_TrunkDis+Pre_std_TrunkDis;
Pre_down_TrunkDis=Pre_mean_TrunkDis-Pre_std_TrunkDis;

pre_x1=1:1:size(Pre_up_TrunkDis);
pre_x2=1:1:size(Pre_down_TrunkDis);
pre_X = [pre_x1 fliplr(pre_x2)];
pre_Y = [Pre_up_TrunkDis' fliplr(Pre_down_TrunkDis')];
fill(pre_X,pre_Y,[0.5 0.5 0.5]);
hold on

% 
% posttest
for postCount=1:size(PostData_acrossTrial,3)
    
    for (i=1:size(PostData_acrossTrial,1))
        Post_TrunkDis(i,postCount)=norm(PostData_acrossTrial(i,19:21,postCount)-PostData_acrossTrial(1,19:21,postCount));
    end
end

Post_mean_TrunkDis=mean(Post_TrunkDis(:,1:postCount),2);
Post_std_TrunkDis=std(Post_TrunkDis(:,1:postCount),0,2);

Post_up_TrunkDis=Post_mean_TrunkDis+Post_std_TrunkDis;
Post_down_TrunkDis=Post_mean_TrunkDis-Post_std_TrunkDis;

post_x1=1:1:size(Post_up_TrunkDis);
post_x2=1:1:size(Post_down_TrunkDis);
post_X = [post_x1 fliplr(post_x2)];
post_Y = [Post_up_TrunkDis' fliplr(Post_down_TrunkDis')];
fill(post_X,post_Y,[0.8 0 1]);
