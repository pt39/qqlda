function varargout = Sync_LDA(varargin)
% SYNC_LDA MATLAB code for Sync_LDA.fig
%      SYNC_LDA, by itself, creates a new SYNC_LDA or raises the existing
%      singleton*.
%
%      H = SYNC_LDA returns the handle to a new SYNC_LDA or the handle to
%      the existing singleton*.
%
%      SYNC_LDA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SYNC_LDA.M with the given input arguments.
%
%      SYNC_LDA('Property','Value',...) creates a new SYNC_LDA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Sync_LDA_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Sync_LDA_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Sync_LDA

% Last Modified by GUIDE v2.5 09-May-2014 12:16:04

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Sync_LDA_OpeningFcn, ...
                   'gui_OutputFcn',  @Sync_LDA_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% End initialization code - DO NOT EDIT


% --- Executes just before Sync_LDA is made visible.
function Sync_LDA_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Sync_LDA (see VARARGIN)

% Choose default command line output for Sync_LDA
handles.output = hObject;

popupmenu1_Callback(hObject, eventdata, handles)
popupmenu8_Callback(hObject, eventdata, handles)
popupmenu9_Callback(hObject, eventdata, handles)
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Sync_LDA wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Sync_LDA_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
global impaired;
global group;
global subjInit;
global test;
global hand;

impaired = get(handles.popupmenu1,'Value');

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3


% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu4.
function popupmenu4_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu4


% --- Executes during object creation, after setting all properties.
function popupmenu4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Hand_Callback(hObject, eventdata, handles)
% hObject    handle to Hand (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Hand as text
%        str2double(get(hObject,'String')) returns contents of Hand as a double


% --- Executes during object creation, after setting all properties.
function Hand_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Hand (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu5.
function popupmenu5_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu5 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu5
% get varibles
global fileDirectory;
global Data;
global Data1;
global Data2;
global SyncData;


Data=[];Data1=[];Data2=[];

Object1_index_selected = get(handles.popupmenu5,'Value');
Object1_list = get(handles.popupmenu5,'String');
Object1 = strtrim(Object1_list(Object1_index_selected,:));

Hands=get(handles.Hand,'String');

FinalDirectory=[fileDirectory char(Object1) '/'];

% load NOB and CG data
ExcelFileName_A1=[FinalDirectory 'Cycled.xls'];
[A1_output, A1_headertext] = xlsread(ExcelFileName_A1);
Numtrials=size(A1_output,1);

index_selected = get(handles.list_Object1,'Value');
list = get(handles.list_Object1,'String');
Object1_PlotVal = strtrim(list{index_selected}); 

for (i=1:size(A1_output,1))
    
    A1_startindex_1 = A1_output(i,1);
    A1_endindex_1 = A1_output(i,2); % movement one
    
    % save onset offset information for each file
    indexRecord(i,:)=A1_output(i,1:6);
    BadIndex=find(indexRecord(i,:)==-999999);
    for (kkk=1:size(BadIndex,2))
        indexRecord(i,BadIndex(kkk))=0;
    end
    
    NOBfilenam = [FinalDirectory char(A1_headertext(i,1))];
    
    if exist(NOBfilenam)
        NOBData = load(NOBfilenam);
        
        
        switch Hands
            case 'left'
                GBfilename = regexprep(char(A1_headertext(i,1)),'NOB','LCG');
            case 'right'
                GBfilename = regexprep(char(A1_headertext(i,1)),'NOB','RCG');
                
        end
        fileG2=[FinalDirectory GBfilename];
        GBdata=load(fileG2);
        
        
        Data = NOBData(:,:);
        DataFB = GBdata(:,:);
        
        
        if isempty(Data1) | isequal(size(Data,1),size(Data1,1))
            Data1=cat(3,Data1,Data);
        else
            k=size(Data,1)-size(Data1,1);
            if k<0
                for j=1:abs(k)
                    Data=[Data;Data(end,:)];
                end
                Data1=cat(3,Data1,Data);
            end
            if k>0
                Data(end-k+1:end,:)=[];
                Data1=cat(3,Data1,Data);
            end
        end
        
        if isempty(Data2) | isequal(size(DataFB,1),size(Data2,1))
            Data2=cat(3,Data2,DataFB);
        else
            k=size(DataFB,1)-size(Data2,1);
            if k<0
                for j=1:abs(k)
                    DataFB=[DataFB;DataFB(end,:)];
                end
                Data2=cat(3,Data2,DataFB);
            end
            if k>0
                DataFB(end-k+1:end,:)=[];
                Data2=cat(3,Data2,DataFB);
            end
        end
    end
end



wrist=[];
indexMPJ=[];
axes(handles.axes1);
for l=1:size(Data1,3)
    Arm=Data1(:,1:3,l);
    [b,a]=butter(2,8/50);
    Armf=filtfilt(b,a,Arm);
    If=size(Armf,1);
    handdif1=difnum(Armf,3,If,.01,1,1,1);
    Tangvel=(sqrt(sum((handdif1.^2)')))';
    Tangvel=procfilt(Tangvel,100);
    Tangvel=Tangvel';
    
    
    %             wrist=[wrist,Data1(:,1,l)];
    wrist=[wrist,Tangvel];
    
    indexPIPt=filtfilt(b,a,Data2(:,6,l)); 
    indexMPJ=[indexMPJ,indexPIPt];
    index_orig=1:1:(size(Tangvel(10:end),1));
    index_1=index_orig-A1_output(l,1)+10;

    if isempty(SyncData)
    switch Object1_PlotVal
        case 'wrist Velocity'
            plot(index_1,Tangvel(10:end),'Color','k','LineWidth',2);
            hold on
            axis([-100,500,0,40])
            plot(A1_output(l,2)-A1_output(l,1),Tangvel(A1_output(l,2)),'r*')
        case 'index_MPJ'
            plot(index_1,Data2(10:end,5,l),'Color','k','LineWidth',2);
            hold on
        case 'index_PIJ'
            plot(index_1,Data2(10:end,6,l),'Color','k','LineWidth',2);
            hold on
        case 'index_DIJ'
            plot(index_1,Data2(10:end,7,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_MPJ'
            plot(index_1,Data2(10:end,8,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_PIJ'
            plot(index_1,Data2(10:end,9,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_DIJ'
            plot(index_1,Data2(10:end,10,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_MPJ'
            plot(index_1,Data2(10:end,11,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_PIJ'
            plot(index_1,Data2(10:end,12,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_DIJ'
            plot(index_1,Data2(10:end,13,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_MPJ'
            plot(index_1,Data2(10:end,14,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_PIJ'
            plot(index_1,Data2(10:end,15,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_DIJ'
            plot(index_1,Data2(10:end,16,l),'Color','k','LineWidth',2);
            hold on
    end
    else
        switch Object1_PlotVal
        case 'wrist Velocity'
            plot(index_1,Tangvel(10:end),'Color','k','LineWidth',2);
            hold on
            axis([-100,500,0,40])
            plot(A1_output(l,2)-A1_output(l,1),Tangvel(A1_output(l,2)),'r*')
        case 'index_MPJ'
            plot(index_1,SyncData(:,5,l),'Color','k','LineWidth',2);
            hold on
        case 'index_PIJ'
            plot(index_1,SyncData(:,6,l),'Color','k','LineWidth',2);
            hold on
        case 'index_DIJ'
            plot(index_1,SyncData(:,7,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_MPJ'
            plot(index_1,SyncData(:,8,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_PIJ'
            plot(index_1,SyncData(:,9,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_DIJ'
            plot(index_1,SyncData(:,10,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_MPJ'
            plot(index_1,SyncData(:,11,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_PIJ'
            plot(index_1,SyncData(:,12,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_DIJ'
            plot(index_1,SyncData(:,13,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_MPJ'
            plot(index_1,SyncData(:,14,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_PIJ'
            plot(index_1,SyncData(:,15,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_DIJ'
            plot(index_1,SyncData(:,16,l),'Color','k','LineWidth',2);
            hold on
        end   
    end

end


        
        
        
        
% --- Executes during object creation, after setting all properties.
function popupmenu5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu6.
function popupmenu6_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu6 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu6
global fileDirectory;
global Data_obj2;
global Data1_obj2;
global Data2_obj2;

Data_obj2=[];Data1_obj2=[];Data2_obj2=[];

Object2_index_selected = get(handles.popupmenu6,'Value');
Object2_list = get(handles.popupmenu6,'String');
Object2 = strtrim(Object2_list(Object2_index_selected,:));

Hands=get(handles.Hand,'String');

FinalDirectory=[fileDirectory char(Object2) '/'];

% load NOB and CG data
ExcelFileName_A2=[FinalDirectory 'Cycled.xls'];
[A2_output, A2_headertext] = xlsread(ExcelFileName_A2);
Numtrials=size(A2_output,1);

index_selected = get(handles.list_Object2,'Value');
list = get(handles.list_Object2,'String');
Object2_PlotVal = strtrim(list{index_selected}); 

for (i=1:size(A2_output,1))
    
    A2_startindex_1 = A2_output(i,1);
    A2_endindex_1 = A2_output(i,2); % movement one
    
    % save onset offset information for each file
    indexRecord(i,:)=A2_output(i,1:6);
    BadIndex=find(indexRecord(i,:)==-999999);
    for (kkk=1:size(BadIndex,2))
        indexRecord(i,BadIndex(kkk))=0;
    end
    
    NOBfilenam = [FinalDirectory char(A2_headertext(i,1))];
    
    if exist(NOBfilenam)
        NOBData = load(NOBfilenam);
        
        
        switch Hands
            case 'left'
                GBfilename = regexprep(char(A2_headertext(i,1)),'NOB','LCG');
            case 'right'
                GBfilename = regexprep(char(A2_headertext(i,1)),'NOB','RCG');
                
        end
        fileG2=[FinalDirectory GBfilename];
        GBdata=load(fileG2);
        
        
        Data_obj2 = NOBData(:,:);
        DataFB = GBdata(:,:);
        
        
        if isempty(Data1_obj2) | isequal(size(Data_obj2,1),size(Data1_obj2,1))
            Data1_obj2=cat(3,Data1_obj2,Data_obj2);
        else
            k=size(Data_obj2,1)-size(Data1_obj2,1);
            if k<0
                for j=1:abs(k)
                    Data_obj2=[Data_obj2;Data_obj2(end,:)];
                end
                Data1_obj2=cat(3,Data1_obj2,Data_obj2);
            end
            if k>0
                Data_obj2(end-k+1:end,:)=[];
                Data1_obj2=cat(3,Data1_obj2,Data_obj2);
            end
        end
        
        if isempty(Data2_obj2) | isequal(size(DataFB,1),size(Data2_obj2,1))
            Data2_obj2=cat(3,Data2_obj2,DataFB);
        else
            k=size(DataFB,1)-size(Data2_obj2,1);
            if k<0
                for j=1:abs(k)
                    DataFB=[DataFB;DataFB(end,:)];
                end
                Data2_obj2=cat(3,Data2_obj2,DataFB);
            end
            if k>0
                DataFB(end-k+1:end,:)=[];
                Data2_obj2=cat(3,Data2_obj2,DataFB);
            end
        end
    end
end



wrist=[];
indexMPJ=[];
axes(handles.axes2);
for l=1:size(Data1_obj2,3)
    Arm=Data1_obj2(:,1:3,l);
    [b,a]=butter(2,8/50);
    Armf=filtfilt(b,a,Arm);
    If=size(Armf,1);
    handdif1=difnum(Armf,3,If,.01,1,1,1);
    Tangvel=(sqrt(sum((handdif1.^2)')))';
    Tangvel=procfilt(Tangvel,100);
    Tangvel=Tangvel';
    
    
    %             wrist=[wrist,Data1(:,1,l)];
    wrist=[wrist,Tangvel];
    
    indexPIPt=filtfilt(b,a,Data2_obj2(:,6,l)); 
    indexMPJ=[indexMPJ,indexPIPt];
    index_orig=1:1:(size(Tangvel(10:end),1));
    index_1=index_orig-A2_output(l,1)+10;

    switch Object2_PlotVal
        case 'wrist Velocity'
            plot(index_1,Tangvel(10:end),'Color','k','LineWidth',2);
            hold on
            axis([-100,500,0,40])
            plot(A2_output(l,2)-A2_output(l,1),Tangvel(A2_output(l,2)),'r*')
        case 'index_MPJ'
            plot(index_1,Data2_obj2(10:end,5,l),'Color','k','LineWidth',2);
            hold on
        case 'index_PIJ'
            plot(index_1,Data2_obj2(10:end,6,l),'Color','k','LineWidth',2);
            hold on
        case 'index_DIJ'
            plot(index_1,Data2_obj2(10:end,7,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_MPJ'
            plot(index_1,Data2_obj2(10:end,8,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_PIJ'
            plot(index_1,Data2_obj2(10:end,9,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_DIJ'
            plot(index_1,Data2_obj2(10:end,10,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_MPJ'
            plot(index_1,Data2_obj2(10:end,11,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_PIJ'
            plot(index_1,Data2_obj2(10:end,12,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_DIJ'
            plot(index_1,Data2_obj2(10:end,13,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_MPJ'
            plot(index_1,Data2_obj2(10:end,14,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_PIJ'
            plot(index_1,Data2_obj2(10:end,15,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_DIJ'
            plot(index_1,Data2_obj2(10:end,16,l),'Color','k','LineWidth',2);
            hold on
    end

end



% --- Executes during object creation, after setting all properties.
function popupmenu6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in list_Object1.
function list_Object1_Callback(hObject, eventdata, handles)
% hObject    handle to list_Object1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns list_Object1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from list_Object1
global fileDirectory;
global SyncData;
global Data1;
global Data2;
global Data;
SyncData
Data=[];Data1=[];Data2=[];

Object1_index_selected = get(handles.popupmenu5,'Value');
Object1_list = get(handles.popupmenu5,'String');
Object1 = strtrim(Object1_list(Object1_index_selected,:));

Hands=get(handles.Hand,'String');
FinalDirectory=[fileDirectory char(Object1) '/'];

% load NOB and CG data
ExcelFileName_A1=[FinalDirectory 'Cycled.xls'];
[A1_output, A1_headertext] = xlsread(ExcelFileName_A1);
Numtrials=size(A1_output,1);

index_selected = get(handles.list_Object1,'Value');
list = get(handles.list_Object1,'String');
Object1_PlotVal = strtrim(list{index_selected}); 

for (i=1:size(A1_output,1))
    
    A1_startindex_1 = A1_output(i,1);
    A1_endindex_1 = A1_output(i,2); % movement one
    
    % save onset offset information for each file
    indexRecord(i,:)=A1_output(i,1:6);
    BadIndex=find(indexRecord(i,:)==-999999);
    for (kkk=1:size(BadIndex,2))
        indexRecord(i,BadIndex(kkk))=0;
    end
    
    NOBfilenam = [FinalDirectory char(A1_headertext(i,1))];
    
    if exist(NOBfilenam)
        NOBData = load(NOBfilenam);
        
        
        switch Hands
            case 'left'
                GBfilename = regexprep(char(A1_headertext(i,1)),'NOB','LCG');
            case 'right'
                GBfilename = regexprep(char(A1_headertext(i,1)),'NOB','RCG');
                
        end
        fileG2=[FinalDirectory GBfilename];
        GBdata=load(fileG2);
        
        
        Data = NOBData(:,:);
        DataFB = GBdata(:,:);
        
        
        if isempty(Data1) | isequal(size(Data,1),size(Data1,1))
            Data1=cat(3,Data1,Data);
        else
            k=size(Data,1)-size(Data1,1);
            if k<0
                for j=1:abs(k)
                    Data=[Data;Data(end,:)];
                end
                Data1=cat(3,Data1,Data);
            end
            if k>0
                Data(end-k+1:end,:)=[];
                Data1=cat(3,Data1,Data);
            end
        end
        
        if isempty(Data2) | isequal(size(DataFB,1),size(Data2,1))
            Data2=cat(3,Data2,DataFB);
        else
            k=size(DataFB,1)-size(Data2,1);
            if k<0
                for j=1:abs(k)
                    DataFB=[DataFB;DataFB(end,:)];
                end
                Data2=cat(3,Data2,DataFB);
            end
            if k>0
                DataFB(end-k+1:end,:)=[];
                Data2=cat(3,Data2,DataFB);
            end
        end
    end
end



wrist=[];
indexMPJ=[];
axes(handles.axes1);
hold off
for l=1:size(Data1,3)
    Arm=Data1(:,1:3,l);
    [b,a]=butter(2,8/50);
    Armf=filtfilt(b,a,Arm);
    If=size(Armf,1);
    handdif1=difnum(Armf,3,If,.01,1,1,1);
    Tangvel=(sqrt(sum((handdif1.^2)')))';
    Tangvel=procfilt(Tangvel,100);
    Tangvel=Tangvel';
    
    
    %             wrist=[wrist,Data1(:,1,l)];
    wrist=[wrist,Tangvel];
    
    indexPIPt=filtfilt(b,a,Data2(:,6,l)); 
    Data2(:,5:16,l)=filtfilt(b,a,Data2(:,5:16,l));
    indexMPJ=[indexMPJ,indexPIPt];
    index_orig=1:1:(size(Tangvel(10:end),1));
    index_1=index_orig-A1_output(l,1)+10;


    if isempty(SyncData)
    switch Object1_PlotVal
        case 'wrist Velocity'
            plot(index_1,Tangvel(10:end),'Color','k','LineWidth',2);
            hold on
            axis([-100,500,0,40])
            plot(A1_output(l,2)-A1_output(l,1),Tangvel(A1_output(l,2)),'r*')
        case 'index_MPJ'
            plot(index_1,Data2(10:end,5,l),'Color','k','LineWidth',2);
            hold on
        case 'index_PIJ'
            plot(index_1,Data2(10:end,6,l),'Color','k','LineWidth',2);
            hold on
        case 'index_DIJ'
            plot(index_1,Data2(10:end,7,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_MPJ'
            plot(index_1,Data2(10:end,8,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_PIJ'
            plot(index_1,Data2(10:end,9,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_DIJ'
            plot(index_1,Data2(10:end,10,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_MPJ'
            plot(index_1,Data2(10:end,11,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_PIJ'
            plot(index_1,Data2(10:end,12,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_DIJ'
            plot(index_1,Data2(10:end,13,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_MPJ'
            plot(index_1,Data2(10:end,14,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_PIJ'
            plot(index_1,Data2(10:end,15,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_DIJ'
            plot(index_1,Data2(10:end,16,l),'Color','k','LineWidth',2);
            hold on
    end
    else
        index_2=11:1:(size(SyncData,1)+10);
        switch Object1_PlotVal
        case 'wrist Velocity'
            plot(index_1,Tangvel(10:end),'Color','k','LineWidth',2);
            hold on
            axis([-100,500,0,40])
            plot(A1_output(l,2)-A1_output(l,1),Tangvel(A1_output(l,2)),'r*')
        case 'index_MPJ'
            plot(index_2,SyncData(:,5,l),'Color','k','LineWidth',2);
            hold on
        case 'index_PIJ'
            plot(index_2,SyncData(:,6,l),'Color','k','LineWidth',2);
            hold on
        case 'index_DIJ'
            plot(index_2,SyncData(:,7,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_MPJ'
            plot(index_2,SyncData(:,8,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_PIJ'
            plot(index_2,SyncData(:,9,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_DIJ'
            plot(index_2,SyncData(:,10,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_MPJ'
            plot(index_2,SyncData(:,11,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_PIJ'
            plot(index_2,SyncData(:,12,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_DIJ'
            plot(index_2,SyncData(:,13,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_MPJ'
            plot(index_2,SyncData(:,14,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_PIJ'
            plot(index_2,SyncData(:,15,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_DIJ'
            plot(index_2,SyncData(:,16,l),'Color','k','LineWidth',2);
            hold on
        end   
    end


end


% --- Executes during object creation, after setting all properties.
function list_Object1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to list_Object1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox2.
function listbox2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox2


% --- Executes during object creation, after setting all properties.
function listbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global SyncData;

SyncData=[];
global fileDirectory;
global Data1;
global Data2;
global Data;
Data=[];Data1=[];Data2=[];

Object1_index_selected = get(handles.popupmenu5,'Value');
Object1_list = get(handles.popupmenu5,'String');
Object1 = strtrim(Object1_list(Object1_index_selected,:));

Hands=get(handles.Hand,'String');

FinalDirectory=[fileDirectory char(Object1) '/'];

% load NOB and CG data
ExcelFileName_A1=[FinalDirectory 'Cycled.xls'];
[A1_output, A1_headertext] = xlsread(ExcelFileName_A1);
Numtrials=size(A1_output,1);

index_selected = get(handles.list_Object1,'Value');
list = get(handles.list_Object1,'String');
Object1_PlotVal = strtrim(list{index_selected}); 

for (i=1:size(A1_output,1))
    
    A1_startindex_1 = A1_output(i,1);
    A1_endindex_1 = A1_output(i,2); % movement one
    
    % save onset offset information for each file
    indexRecord(i,:)=A1_output(i,1:6);
    BadIndex=find(indexRecord(i,:)==-999999);
    for (kkk=1:size(BadIndex,2))
        indexRecord(i,BadIndex(kkk))=0;
    end
    
    NOBfilenam = [FinalDirectory char(A1_headertext(i,1))];
    
    if exist(NOBfilenam)
        NOBData = load(NOBfilenam);
        
        
        switch Hands
            case 'left'
                GBfilename = regexprep(char(A1_headertext(i,1)),'NOB','LCG');
            case 'right'
                GBfilename = regexprep(char(A1_headertext(i,1)),'NOB','RCG');
                
        end
        fileG2=[FinalDirectory GBfilename];
        GBdata=load(fileG2);
        
        
        Data = NOBData(:,:);
        DataFB = GBdata(:,:);
        
        
        if isempty(Data1) | isequal(size(Data,1),size(Data1,1))
            Data1=cat(3,Data1,Data);
        else
            k=size(Data,1)-size(Data1,1);
            if k<0
                for j=1:abs(k)
                    Data=[Data;Data(end,:)];
                end
                Data1=cat(3,Data1,Data);
            end
            if k>0
                Data(end-k+1:end,:)=[];
                Data1=cat(3,Data1,Data);
            end
        end
        
        if isempty(Data2) | isequal(size(DataFB,1),size(Data2,1))
            Data2=cat(3,Data2,DataFB);
        else
            k=size(DataFB,1)-size(Data2,1);
            if k<0
                for j=1:abs(k)
                    DataFB=[DataFB;DataFB(end,:)];
                end
                Data2=cat(3,Data2,DataFB);
            end
            if k>0
                DataFB(end-k+1:end,:)=[];
                Data2=cat(3,Data2,DataFB);
            end
        end
    end
end



wrist=[];
indexMPJ=[];
axes(handles.axes1);
hold off

for l=1:size(Data1,3)
    Arm=Data1(:,1:3,l);
    [b,a]=butter(2,8/50);
    Armf=filtfilt(b,a,Arm);
    If=size(Armf,1);
    handdif1=difnum(Armf,3,If,.01,1,1,1);
    Tangvel=(sqrt(sum((handdif1.^2)')))';
    Tangvel=procfilt(Tangvel,100);
    Tangvel=Tangvel';
    
    
    %             wrist=[wrist,Data1(:,1,l)];
    wrist=[wrist,Tangvel];
    
    indexPIPt=filtfilt(b,a,Data2(:,6,l)); 
    Data2(:,5:16,l)=filtfilt(b,a,Data2(:,5:16,l));
    indexMPJ=[indexMPJ,indexPIPt];
    index_orig=1:1:(size(Tangvel(10:end),1));
    index_1=index_orig-A1_output(l,1)+10;


    if isempty(SyncData)
    switch Object1_PlotVal
        case 'wrist Velocity'
            plot(index_1,Tangvel(10:end),'Color','k','LineWidth',2);
            hold on
            axis([-100,500,0,40])
            plot(A1_output(l,2)-A1_output(l,1),Tangvel(A1_output(l,2)),'r*')
        case 'index_MPJ'
            plot(index_1,Data2(10:end,5,l),'Color','k','LineWidth',2);
            hold on
        case 'index_PIJ'
            plot(index_1,Data2(10:end,6,l),'Color','k','LineWidth',2);
            hold on
        case 'index_DIJ'
            plot(index_1,Data2(10:end,7,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_MPJ'
            plot(index_1,Data2(10:end,8,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_PIJ'
            plot(index_1,Data2(10:end,9,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_DIJ'
            plot(index_1,Data2(10:end,10,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_MPJ'
            plot(index_1,Data2(10:end,11,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_PIJ'
            plot(index_1,Data2(10:end,12,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_DIJ'
            plot(index_1,Data2(10:end,13,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_MPJ'
            plot(index_1,Data2(10:end,14,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_PIJ'
            plot(index_1,Data2(10:end,15,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_DIJ'
            plot(index_1,Data2(10:end,16,l),'Color','k','LineWidth',2);
            hold on
    end
    else
        index_2=11:1:(size(SyncData,1)+10);
        switch Object1_PlotVal
        case 'wrist Velocity'
            plot(index_1,Tangvel(10:end),'Color','k','LineWidth',2);
            hold on
            axis([-100,500,0,40])
            plot(A1_output(l,2)-A1_output(l,1),Tangvel(A1_output(l,2)),'r*')
        case 'index_MPJ'
            plot(index_2,SyncData(:,5,l),'Color','k','LineWidth',2);
            hold on
        case 'index_PIJ'
            plot(index_2,SyncData(:,6,l),'Color','k','LineWidth',2);
            hold on
        case 'index_DIJ'
            plot(index_2,SyncData(:,7,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_MPJ'
            plot(index_2,SyncData(:,8,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_PIJ'
            plot(index_2,SyncData(:,9,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_DIJ'
            plot(index_2,SyncData(:,10,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_MPJ'
            plot(index_2,SyncData(:,11,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_PIJ'
            plot(index_2,SyncData(:,12,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_DIJ'
            plot(index_2,SyncData(:,13,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_MPJ'
            plot(index_2,SyncData(:,14,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_PIJ'
            plot(index_2,SyncData(:,15,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_DIJ'
            plot(index_2,SyncData(:,16,l),'Color','k','LineWidth',2);
            hold on
        end   
    end


end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Data2;
global Data1;
global SyncData;

index_selected = get(handles.list_Object1,'Value');
list = get(handles.list_Object1,'String');
Object1_PlotVal = strtrim(list{index_selected}); 


SyncData=[];
index_selected = get(handles.list_Object1,'Value');
[SyncData] = Synabsolutethreshold_singleObj( Data2,size(Data2,3),index_selected)
hold off
for l=1:size(Data2,3)

        index_2=11:1:(size(SyncData,1)+10);
        switch Object1_PlotVal
        case 'index_MPJ'
            plot(index_2,SyncData(:,5,l),'Color','k','LineWidth',2);
            hold on
        case 'index_PIJ'
            plot(index_2,SyncData(:,6,l),'Color','k','LineWidth',2);
            hold on
        case 'index_DIJ'
            plot(index_2,SyncData(:,7,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_MPJ'
            plot(index_2,SyncData(:,8,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_PIJ'
            plot(index_2,SyncData(:,9,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_DIJ'
            plot(index_2,SyncData(:,10,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_MPJ'
            plot(index_2,SyncData(:,11,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_PIJ'
            plot(index_2,SyncData(:,12,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_DIJ'
            plot(index_2,SyncData(:,13,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_MPJ'
            plot(index_2,SyncData(:,14,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_PIJ'
            plot(index_2,SyncData(:,15,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_DIJ'
            plot(index_2,SyncData(:,16,l),'Color','k','LineWidth',2);
            hold on
        end   
    end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global SyncData;
global Data2;
global fileDirectory;

Object1_index_selected = get(handles.popupmenu5,'Value');
Object1_list = get(handles.popupmenu5,'String');
Object1 = strtrim(Object1_list(Object1_index_selected,:));

Hands=get(handles.Hand,'String');

FinalDirectory=[fileDirectory char(Object1) '/'];

for fileNum=1:size(SyncData,3)
    savedData=SyncData(:,:,fileNum);
    save([FinalDirectory,'SyncCGData', num2str(fileNum), '.txt'],'savedData','-ASCII')
    savedData=[];
end




% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in listbox3.
function listbox3_Callback(hObject, eventdata, handles)
% hObject    handle to listbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox3


% --- Executes during object creation, after setting all properties.
function listbox3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in listbox4.
function listbox4_Callback(hObject, eventdata, handles)
% hObject    handle to listbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox4


% --- Executes during object creation, after setting all properties.
function listbox4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in popupmenu8.
function popupmenu8_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu8 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu8
global impaired;
global impairFolder;
global groupFolder;
global fileDirectory;
global SyncData;
global SyncData_obj2;
global Data;
global Data1;
global Data2;
global Data_obj2;
global Data1_obj2;
global Data2_obj2;

SyncData=[];
SyncData_obj2=[];
Data=[];
Data1=[];
Data2=[];
Data_obj2=[];
Data1_obj2=[];
Data2_obj2=[];

test = get(handles.popupmenu8,'Value');
switch test
    case 1
        testFolder='pretest';
    case 2
        testFolder='posttest';
end
subjDirectory=['../' impairFolder '/' groupFolder '/'];
% get varibles
subject_index_selected = get(handles.popupmenu3,'Value');
subject_list = get(handles.popupmenu3,'String');
subject = strtrim(subject_list(subject_index_selected,:));

handDirectory=[subjDirectory subject '/' testFolder '/'];
hand=ls(handDirectory);
set(handles.Hand,'String',hand(3,:));
fileDirectory=[handDirectory strtrim(hand(3,:)) '/'];

% --- Executes during object creation, after setting all properties.
function popupmenu8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton13.
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton14.
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global SyncData_obj2;

SyncData_obj2=[];
global fileDirectory;
global Data_obj2;
global Data1_obj2;
global Data2_obj2;

Data_obj2=[];Data1_obj2=[];Data2_obj2=[];

Object2_index_selected = get(handles.popupmenu6,'Value');
Object2_list = get(handles.popupmenu6,'String');
Object2 = strtrim(Object2_list(Object2_index_selected,:));

Hands=get(handles.Hand,'String');

FinalDirectory=[fileDirectory char(Object2) '/'];

% load NOB and CG data
ExcelFileName_A2=[FinalDirectory 'Cycled.xls'];
[A2_output, A2_headertext] = xlsread(ExcelFileName_A2);
Numtrials=size(A2_output,1);

index_selected = get(handles.list_Object2,'Value');
list = get(handles.list_Object2,'String');
Object2_PlotVal = strtrim(list{index_selected}); 

for (i=1:size(A2_output,1))
    
    A2_startindex_1 = A2_output(i,1);
    A2_endindex_1 = A2_output(i,2); % movement one
    
    % save onset offset information for each file
    indexRecord(i,:)=A2_output(i,1:6);
    BadIndex=find(indexRecord(i,:)==-999999);
    for (kkk=1:size(BadIndex,2))
        indexRecord(i,BadIndex(kkk))=0;
    end
    
    NOBfilenam = [FinalDirectory char(A2_headertext(i,1))];
    
    if exist(NOBfilenam)
        NOBData = load(NOBfilenam);
        
        
        switch Hands
            case 'left'
                GBfilename = regexprep(char(A2_headertext(i,1)),'NOB','LCG');
            case 'right'
                GBfilename = regexprep(char(A2_headertext(i,1)),'NOB','RCG');
                
        end
        fileG2=[FinalDirectory GBfilename];
        GBdata=load(fileG2);
        
        
        Data_obj2 = NOBData(:,:);
        DataFB = GBdata(:,:);
        
        
        if isempty(Data1_obj2) | isequal(size(Data_obj2,1),size(Data1_obj2,1))
            Data1_obj2=cat(3,Data1_obj2,Data_obj2);
        else
            k=size(Data_obj2,1)-size(Data1_obj2,1);
            if k<0
                for j=1:abs(k)
                    Data_obj2=[Data_obj2;Data_obj2(end,:)];
                end
                Data1_obj2=cat(3,Data1_obj2,Data_obj2);
            end
            if k>0
                Data_obj2(end-k+1:end,:)=[];
                Data1_obj2=cat(3,Data1_obj2,Data_obj2);
            end
        end
        
        if isempty(Data2_obj2) | isequal(size(DataFB,1),size(Data2_obj2,1))
            Data2_obj2=cat(3,Data2_obj2,DataFB);
        else
            k=size(DataFB,1)-size(Data2_obj2,1);
            if k<0
                for j=1:abs(k)
                    DataFB=[DataFB;DataFB(end,:)];
                end
                Data2_obj2=cat(3,Data2_obj2,DataFB);
            end
            if k>0
                DataFB(end-k+1:end,:)=[];
                Data2_obj2=cat(3,Data2_obj2,DataFB);
            end
        end
    end
end



wrist=[];
indexMPJ=[];
axes(handles.axes2);
hold off
for l=1:size(Data1_obj2,3)
    Arm=Data1_obj2(:,1:3,l);
    [b,a]=butter(2,8/50);
    Armf=filtfilt(b,a,Arm);
    If=size(Armf,1);
    handdif1=difnum(Armf,3,If,.01,1,1,1);
    Tangvel=(sqrt(sum((handdif1.^2)')))';
    Tangvel=procfilt(Tangvel,100);
    Tangvel=Tangvel';
    
    
    %             wrist=[wrist,Data1(:,1,l)];
    wrist=[wrist,Tangvel];
    
    indexPIPt=filtfilt(b,a,Data2_obj2(:,6,l)); 
    indexMPJ=[indexMPJ,indexPIPt];
    Data2_obj2(:,5:16,l)=filtfilt(b,a,Data2_obj2(:,5:16,l)); 
    index_orig=1:1:(size(Tangvel(10:end),1));
    index_1=index_orig-A2_output(l,1)+10;

    if isempty(SyncData_obj2)
    switch Object2_PlotVal
        case 'wrist Velocity'
            plot(index_1,Tangvel(10:end),'Color','k','LineWidth',2);
            hold on
            axis([-100,500,0,40])
            plot(A1_output(l,2)-A1_output(l,1),Tangvel(A1_output(l,2)),'r*')
        case 'index_MPJ'
            plot(index_1,Data2_obj2(10:end,5,l),'Color','k','LineWidth',2);
            hold on
        case 'index_PIJ'
            plot(index_1,Data2_obj2(10:end,6,l),'Color','k','LineWidth',2);
            hold on
        case 'index_DIJ'
            plot(index_1,Data2_obj2(10:end,7,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_MPJ'
            plot(index_1,Data2_obj2(10:end,8,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_PIJ'
            plot(index_1,Data2_obj2(10:end,9,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_DIJ'
            plot(index_1,Data2_obj2(10:end,10,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_MPJ'
            plot(index_1,Data2_obj2(10:end,11,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_PIJ'
            plot(index_1,Data2_obj2(10:end,12,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_DIJ'
            plot(index_1,Data2_obj2(10:end,13,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_MPJ'
            plot(index_1,Data2_obj2(10:end,14,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_PIJ'
            plot(index_1,Data2_obj2(10:end,15,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_DIJ'
            plot(index_1,Data2_obj2(10:end,16,l),'Color','k','LineWidth',2);
            hold on
    end
    else
        index_2=11:1:(size(SyncData_obj2,1)+10);
        switch Object2_PlotVal
        case 'wrist Velocity'
            plot(index_1,Tangvel(10:end),'Color','k','LineWidth',2);
            hold on
            axis([-100,500,0,40])
            plot(A1_output(l,2)-A1_output(l,1),Tangvel(A1_output(l,2)),'r*')
        case 'index_MPJ'
            plot(index_2,SyncData_obj2(:,5,l),'Color','k','LineWidth',2);
            hold on
        case 'index_PIJ'
            plot(index_2,SyncData_obj2(:,6,l),'Color','k','LineWidth',2);
            hold on
        case 'index_DIJ'
            plot(index_2,SyncData_obj2(:,7,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_MPJ'
            plot(index_2,SyncData_obj2(:,8,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_PIJ'
            plot(index_2,SyncData_obj2(:,9,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_DIJ'
            plot(index_2,SyncData_obj2(:,10,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_MPJ'
            plot(index_2,SyncData_obj2(:,11,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_PIJ'
            plot(index_2,SyncData_obj2(:,12,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_DIJ'
            plot(index_2,SyncData_obj2(:,13,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_MPJ'
            plot(index_2,SyncData_obj2(:,14,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_PIJ'
            plot(index_2,SyncData_obj2(:,15,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_DIJ'
            plot(index_2,SyncData_obj2(:,16,l),'Color','k','LineWidth',2);
            hold on
        end   
    end


end

% --- Executes on button press in pushbutton15.
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Data2_obj2;
global SyncData_obj2;

index_selected = get(handles.list_Object2,'Value');
list = get(handles.list_Object2,'String');
Object2_PlotVal = strtrim(list{index_selected}); 

SyncData_obj2=[];
index_selected = get(handles.list_Object2,'Value');
[SyncData_obj2] = Synabsolutethreshold_singleObj( Data2_obj2,size(Data2_obj2,3),index_selected)
hold off
for l=1:size(Data2_obj2,3)

        index_2=11:1:(size(SyncData_obj2,1)+10);
        switch Object2_PlotVal
        case 'index_MPJ'
            plot(index_2,SyncData_obj2(:,5,l),'Color','k','LineWidth',2);
            hold on
        case 'index_PIJ'
            plot(index_2,SyncData_obj2(:,6,l),'Color','k','LineWidth',2);
            hold on
        case 'index_DIJ'
            plot(index_2,SyncData_obj2(:,7,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_MPJ'
            plot(index_2,SyncData_obj2(:,8,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_PIJ'
            plot(index_2,SyncData_obj2(:,9,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_DIJ'
            plot(index_2,SyncData_obj2(:,10,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_MPJ'
            plot(index_2,SyncData_obj2(:,11,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_PIJ'
            plot(index_2,SyncData_obj2(:,12,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_DIJ'
            plot(index_2,SyncData_obj2(:,13,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_MPJ'
            plot(index_2,SyncData_obj2(:,14,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_PIJ'
            plot(index_2,SyncData_obj2(:,15,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_DIJ'
            plot(index_2,SyncData_obj2(:,16,l),'Color','k','LineWidth',2);
            hold on
        end   
    end

% --- Executes on button press in pushbutton16.
function pushbutton16_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global SyncData_obj2;
global Data2_obj2;
global fileDirectory;

Object2_index_selected = get(handles.popupmenu6,'Value');
Object2_list = get(handles.popupmenu6,'String');
Object2 = strtrim(Object2_list(Object2_index_selected,:));

Hands=get(handles.Hand,'String');

FinalDirectory=[fileDirectory char(Object2) '/'];

for fileNum=1:size(SyncData_obj2,3)
    savedData=SyncData_obj2(:,:,fileNum);
    save([FinalDirectory,'SyncData', num2str(fileNum), '.txt'],'savedData','-ASCII');
    savedData=[];
end


% --- Executes on button press in pushbutton19.
function pushbutton19_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global fileDirectory;

Object1_index_selected = get(handles.popupmenu5,'Value');
Object1_list = get(handles.popupmenu5,'String');
Object1 = strtrim(Object1_list(Object1_index_selected,:));
Object2_index_selected = get(handles.popupmenu6,'Value');
Object2_list = get(handles.popupmenu6,'String');
Object2 = strtrim(Object2_list(Object2_index_selected,:));


select_index_selected = get(handles.list_Average,'Value');
select_list = get(handles.list_Average,'String');
AverageVal = strtrim(select_list(select_index_selected,:));

% figure_children=get(handles.axes3,'Children');
% children_axes=findall(figure_children,'Type','axes');
h = figure;
set(h,'Visible','off');
tempaxes=findall(handles.axes3,'Type','axes');
copyobj(tempaxes, h);
legend(char(Object1),char(Object2));
title(char(AverageVal));
hgsave(h, [fileDirectory, char(Object1),'vs', char(Object2), '-', char(AverageVal),'.fig']);

% --- Executes on button press in pushbutton22.
function pushbutton22_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in popupmenu9.
function popupmenu9_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu9 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu9
global impaired;
global impairFolder;
global groupFolder;
global test;
global hand;

group = get(handles.popupmenu9,'Value');

switch impaired
    case 1
        impairFolder='CYCLED_IMPAIREDSIDE';
    case 2
        impairFolder='CYCLED_GoodSide';
end

switch group
    case 1
        groupFolder='Control Group Cycled';
    case 2
        groupFolder='HAS Cycled';
    case 3
        groupFolder='HAT Cycled';
end


subjDirectory=['../' impairFolder '/' groupFolder '/'];
subjList=ls(subjDirectory);

set(handles.popupmenu3,'String',subjList(3:end,:));

% --- Executes during object creation, after setting all properties.
function popupmenu9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in plot_Object1.
function plot_Object1_Callback(hObject, eventdata, handles)
% hObject    handle to plot_Object1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in plot_Object2.
function plot_Object2_Callback(hObject, eventdata, handles)
% hObject    handle to plot_Object2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in plot_Average.
function plot_Average_Callback(hObject, eventdata, handles)
% hObject    handle to plot_Average (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Data2;
global Data2_obj2;
global SyncData;
global SyncData_obj2;

Object1_index_selected = get(handles.popupmenu5,'Value');
Object1_list = get(handles.popupmenu5,'String');
Object1 = strtrim(Object1_list(Object1_index_selected,:));
Object2_index_selected = get(handles.popupmenu6,'Value');
Object2_list = get(handles.popupmenu6,'String');
Object2 = strtrim(Object2_list(Object2_index_selected,:));


axes(handles.axes3);
index_selected = get(handles.list_Average,'Value');
if isempty(SyncData)
    Average_obj1=mean(Data2(:,index_selected+4,:),3);
else
    Average_obj1=mean(SyncData(:,index_selected+4,:),3);
end

plot(Average_obj1,'k');
hold on
if isempty(SyncData_obj2)
    Average_obj2=mean(Data2_obj2(:,index_selected+4,:),3);
else
    Average_obj2=mean(SyncData_obj2(:,index_selected+4,:),3);
end
plot(Average_obj2,'r');
legend(char(Object1),char(Object2));
hold off

% --- Executes on button press in plot_LDA.
function plot_LDA_Callback(hObject, eventdata, handles)
% hObject    handle to plot_LDA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in list_Object2.
function list_Object2_Callback(hObject, eventdata, handles)
% hObject    handle to list_Object2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns list_Object2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from list_Object2
global fileDirectory;
global Data_obj2;
global Data1_obj2;
global Data2_obj2;
global SyncData_obj2;

Data_obj2=[];Data1_obj2=[];Data2_obj2=[];

Object2_index_selected = get(handles.popupmenu6,'Value');
Object2_list = get(handles.popupmenu6,'String');
Object2 = strtrim(Object2_list(Object2_index_selected,:));

Hands=get(handles.Hand,'String');

FinalDirectory=[fileDirectory char(Object2) '/'];

% load NOB and CG data
ExcelFileName_A2=[FinalDirectory 'Cycled.xls'];
[A2_output, A2_headertext] = xlsread(ExcelFileName_A2);
Numtrials=size(A2_output,1);

index_selected = get(handles.list_Object2,'Value');
list = get(handles.list_Object2,'String');
Object2_PlotVal = strtrim(list{index_selected}); 

for (i=1:size(A2_output,1))
    
    A2_startindex_1 = A2_output(i,1);
    A2_endindex_1 = A2_output(i,2); % movement one
    
    % save onset offset information for each file
    indexRecord(i,:)=A2_output(i,1:6);
    BadIndex=find(indexRecord(i,:)==-999999);
    for (kkk=1:size(BadIndex,2))
        indexRecord(i,BadIndex(kkk))=0;
    end
    
    NOBfilenam = [FinalDirectory char(A2_headertext(i,1))];
    
    if exist(NOBfilenam)
        NOBData = load(NOBfilenam);
        
        
        switch Hands
            case 'left'
                GBfilename = regexprep(char(A2_headertext(i,1)),'NOB','LCG');
            case 'right'
                GBfilename = regexprep(char(A2_headertext(i,1)),'NOB','RCG');
                
        end
        fileG2=[FinalDirectory GBfilename];
        GBdata=load(fileG2);
        
        
        Data_obj2 = NOBData(:,:);
        DataFB = GBdata(:,:);
        
        
        if isempty(Data1_obj2) | isequal(size(Data_obj2,1),size(Data1_obj2,1))
            Data1_obj2=cat(3,Data1_obj2,Data_obj2);
        else
            k=size(Data_obj2,1)-size(Data1_obj2,1);
            if k<0
                for j=1:abs(k)
                    Data_obj2=[Data_obj2;Data_obj2(end,:)];
                end
                Data1_obj2=cat(3,Data1_obj2,Data_obj2);
            end
            if k>0
                Data_obj2(end-k+1:end,:)=[];
                Data1_obj2=cat(3,Data1_obj2,Data_obj2);
            end
        end
        
        if isempty(Data2_obj2) | isequal(size(DataFB,1),size(Data2_obj2,1))
            Data2_obj2=cat(3,Data2_obj2,DataFB);
        else
            k=size(DataFB,1)-size(Data2_obj2,1);
            if k<0
                for j=1:abs(k)
                    DataFB=[DataFB;DataFB(end,:)];
                end
                Data2_obj2=cat(3,Data2_obj2,DataFB);
            end
            if k>0
                DataFB(end-k+1:end,:)=[];
                Data2_obj2=cat(3,Data2_obj2,DataFB);
            end
        end
    end
end



wrist=[];
indexMPJ=[];
axes(handles.axes2);
hold off
for l=1:size(Data1_obj2,3)
    Arm=Data1_obj2(:,1:3,l);
    [b,a]=butter(2,8/50);
    Armf=filtfilt(b,a,Arm);
    If=size(Armf,1);
    handdif1=difnum(Armf,3,If,.01,1,1,1);
    Tangvel=(sqrt(sum((handdif1.^2)')))';
    Tangvel=procfilt(Tangvel,100);
    Tangvel=Tangvel';
    
    
    %             wrist=[wrist,Data1(:,1,l)];
    wrist=[wrist,Tangvel];
    
    indexPIPt=filtfilt(b,a,Data2_obj2(:,6,l)); 
    indexMPJ=[indexMPJ,indexPIPt];
    Data2_obj2(:,5:16,l)=filtfilt(b,a,Data2_obj2(:,5:16,l)); 
    index_orig=1:1:(size(Tangvel(10:end),1));
    index_1=index_orig-A2_output(l,1)+10;

    if isempty(SyncData_obj2)
    switch Object2_PlotVal
        case 'wrist Velocity'
            plot(index_1,Tangvel(10:end),'Color','k','LineWidth',2);
            hold on
            axis([-100,500,0,40])
            plot(A1_output(l,2)-A1_output(l,1),Tangvel(A1_output(l,2)),'r*')
        case 'index_MPJ'
            plot(index_1,Data2_obj2(10:end,5,l),'Color','k','LineWidth',2);
            hold on
        case 'index_PIJ'
            plot(index_1,Data2_obj2(10:end,6,l),'Color','k','LineWidth',2);
            hold on
        case 'index_DIJ'
            plot(index_1,Data2_obj2(10:end,7,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_MPJ'
            plot(index_1,Data2_obj2(10:end,8,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_PIJ'
            plot(index_1,Data2_obj2(10:end,9,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_DIJ'
            plot(index_1,Data2_obj2(10:end,10,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_MPJ'
            plot(index_1,Data2_obj2(10:end,11,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_PIJ'
            plot(index_1,Data2_obj2(10:end,12,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_DIJ'
            plot(index_1,Data2_obj2(10:end,13,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_MPJ'
            plot(index_1,Data2_obj2(10:end,14,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_PIJ'
            plot(index_1,Data2_obj2(10:end,15,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_DIJ'
            plot(index_1,Data2_obj2(10:end,16,l),'Color','k','LineWidth',2);
            hold on
    end
    else
        index_2=11:1:(size(SyncData_obj2,1)+10);
        switch Object2_PlotVal
        case 'wrist Velocity'
            plot(index_1,Tangvel(10:end),'Color','k','LineWidth',2);
            hold on
            axis([-100,500,0,40])
            plot(A1_output(l,2)-A1_output(l,1),Tangvel(A1_output(l,2)),'r*')
        case 'index_MPJ'
            plot(index_2,SyncData_obj2(:,5,l),'Color','k','LineWidth',2);
            hold on
        case 'index_PIJ'
            plot(index_2,SyncData_obj2(:,6,l),'Color','k','LineWidth',2);
            hold on
        case 'index_DIJ'
            plot(index_2,SyncData_obj2(:,7,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_MPJ'
            plot(index_2,SyncData_obj2(:,8,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_PIJ'
            plot(index_2,SyncData_obj2(:,9,l),'Color','k','LineWidth',2);
            hold on
        case 'middle_DIJ'
            plot(index_2,SyncData_obj2(:,10,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_MPJ'
            plot(index_2,SyncData_obj2(:,11,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_PIJ'
            plot(index_2,SyncData_obj2(:,12,l),'Color','k','LineWidth',2);
            hold on
        case 'ring_DIJ'
            plot(index_2,SyncData_obj2(:,13,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_MPJ'
            plot(index_2,SyncData_obj2(:,14,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_PIJ'
            plot(index_2,SyncData_obj2(:,15,l),'Color','k','LineWidth',2);
            hold on
        case 'pinky_DIJ'
            plot(index_2,SyncData_obj2(:,16,l),'Color','k','LineWidth',2);
            hold on
        end   
    end


end

% --- Executes during object creation, after setting all properties.
function list_Object2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to list_Object2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in list_Average.
function list_Average_Callback(hObject, eventdata, handles)
% hObject    handle to list_Average (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns list_Average contents as cell array
%        contents{get(hObject,'Value')} returns selected item from list_Average
global Data2;
global Data2_obj2;
global SyncData;
global SyncData_obj2;

Object1_index_selected = get(handles.popupmenu5,'Value');
Object1_list = get(handles.popupmenu5,'String');
Object1 = strtrim(Object1_list(Object1_index_selected,:));
Object2_index_selected = get(handles.popupmenu6,'Value');
Object2_list = get(handles.popupmenu6,'String');
Object2 = strtrim(Object2_list(Object2_index_selected,:));


axes(handles.axes3);
index_selected = get(handles.list_Average,'Value');
if isempty(SyncData)
    Average_obj1=mean(Data2(:,index_selected+4,:),3);
else
    Average_obj1=mean(SyncData(:,index_selected+4,:),3);
end

plot(Average_obj1,'k');
hold on
if isempty(SyncData_obj2)
    Average_obj2=mean(Data2_obj2(:,index_selected+4,:),3);
else
    Average_obj2=mean(SyncData_obj2(:,index_selected+4,:),3);
end
plot(Average_obj2,'r');
legend(char(Object1),char(Object2));
hold off

% --- Executes during object creation, after setting all properties.
function list_Average_CreateFcn(hObject, eventdata, handles)
% hObject    handle to list_Average (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in list_LDA.
function list_LDA_Callback(hObject, eventdata, handles)
% hObject    handle to list_LDA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns list_LDA contents as cell array
%        contents{get(hObject,'Value')} returns selected item from list_LDA
global SyncData;
global SyncData_obj2;
global Data2;
global Data2_obj2;

index_selected = get(handles.list_LDA,'Value');
if isempty(SyncData)
    DataObj1=Data2(:,index_selected+5,:);
else
    DataObj1=SyncData(:,index_selected+5,:);
end
if isempty(SyncData_obj2)
    DataObj2=Data2_obj2(:,index_selected+5,:);
else
    DataObj2=SyncData_obj2(:,index_selected+5,:);
end

[Errorsfilt] = Sync_LDA_Classification(DataObj1,DataObj2,size(DataObj1,3),size(DataObj2,3));
[Errorsfilt2] = Sync_SVM_Classification(DataObj1,DataObj2,size(DataObj1,3),size(DataObj2,3));
axes(handles.axes4);
hold off
plot(Errorsfilt,'g','linewidth',2)
hold on
plot(Errorsfilt2, 'r', 'linewidth', 2);
legend('LDA', 'SVM');
xlim([0 150]);


% --- Executes during object creation, after setting all properties.
function list_LDA_CreateFcn(hObject, eventdata, handles)
% hObject    handle to list_LDA (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
