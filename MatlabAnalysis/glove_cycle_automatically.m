function glove_cycle_automatically(pathname1)
%b:bad trial, p:pass, f:failed
%j:subject, k:shape, i:time

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  glove_cycle_automatically('../CYCLED_IMPAIREDSIDE')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc
close all

% fingSYNCdata={'init' 'group'	'test'	'hand' 'affected' 'object'	'fileName' 'finger_point1' 'finger_point2' 'amplitude1' 'amplitude2'};

for catiList=2:2
    switch catiList
        case 1
            Catigory = ['HAS cycled'];
            CatiName = ['HAS'];
        case 2
            Catigory = ['HAT cycled'];
            CatiName = ['HAT'];
        case 3
            Catigory = ['Control Group Cycled'];
            CatiName = ['Controls'];
    end
    
    subject_list = ls([pathname1 '\' Catigory]);
    totalCount=1;
    
    %for j = 3:size(subject_list,1)
    for j =10:10 %skip (BG), 7(JB),15(NK), 3:size(subject_list,1)
        
        pathname = [pathname1 '\' Catigory '\' strtrim(subject_list(j,:))];
        for i=2:3
            %             for m=1:2
            for k=1:4
                %                     switch m
                %                         case 1
                %                             impairedHand = ['left'];
                %                             glovePre=['LCG'];
                %                         case 2
                %                             impairedHand = ['right'];
                %                             glovePre=['RCG'];
                %                     end
                %
                
                switch i
                    case 1
                        dir = ['baseline'];
                    case 2
                        dir = ['pretest'];
                    case 3
                        dir = ['posttest'];
                end
                
                
                tempname = [pathname '\' dir];
                handName=ls(tempname);
                impairedHand = handName(3,:);
                if (strcmp(impairedHand,'right'))
                    glovePrefix='RCG';
                else
                    glovePrefix='LCG';
                end
                
                preDir=[pathname '\' dir '\' impairedHand];
                
                %load pre calibration file
                PreCaliFileList=ls([preDir '\calibration\' glovePrefix '*.txt']);
                PreCalibration1=load([preDir '\calibration\' PreCaliFileList(1,:)]);
                PreCalibration2=load([preDir '\calibration\' PreCaliFileList(2,:)]);
                PreCalibration3=load([preDir '\calibration\' PreCaliFileList(3,:)]);
                
                
                switch k
                    case 1
                        preDir=[preDir '\bigcube'];
                        objName='bigcube';
                    case 2
                        preDir=[preDir '\bigcircle'];
                        objName='bigcircle';
                    case 3
                        preDir=[preDir '\smallcube'];
                        objName='smallcube';
                    case 4
                        preDir=[preDir '\smallcircle'];
                        objName='smallcircle';
                end
                
                
                PreCyclefileName=[preDir '\Cycled.xls'];
                [Precycle_data, Precycle_header]=xlsread(PreCyclefileName);
                
                
                
                PreData_acrossTrial=[];
                PostData_acrossTrial=[];
                PreCGData_acrossTrial=[];
                PostCGData_acrossTrial=[];
                
                % load pre test
                for PreNOBfileNum=1:size(Precycle_header,1)
                    
                    invalidFiles=find(Precycle_data(PreNOBfileNum,1:6)==-999999);
                    if (isempty(invalidFiles))
                        
                        
                        %load glove data
                        PreCGFilename=regexprep(char(Precycle_header(PreNOBfileNum,1)),'NOB',glovePrefix);
                        PreCGFile_tmp=load([preDir '\' PreCGFilename]);
                        PreCGFile=calibration(PreCGFile_tmp,PreCalibration1,PreCalibration2,PreCalibration3);
                        
                        %load NOB data
                        PreNOBfilename=[preDir '\' char(Precycle_header(PreNOBfileNum,1))]
                        PreNOBfile=load(PreNOBfilename);
                        
                        Preonset1=Precycle_data(PreNOBfileNum,1);
                        Preoffset1=Precycle_data(PreNOBfileNum,2);
                        Preonset2=Precycle_data(PreNOBfileNum,3);
                        Preoffset2=Precycle_data(PreNOBfileNum,4);
                        Preonset3=Precycle_data(PreNOBfileNum,5);
                        Preoffset3=Precycle_data(PreNOBfileNum,6);
                        st=[];
                        %                         st=cat(1,st,{strtrim(subject_list(j,:)) CatiName dir impairedHand 'affected' objName char(Precycle_header(PreNOBfileNum,1))});
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        PreCGfileMPJ=PreCGFile(:,6);
                        PreCGfile1=PreCGfileMPJ;
                        [maxtab, mintab]=peakdet(PreCGfile1, .05);
                        [amplitude2, index1]=min(PreCGfile1(Preoffset2:end));
                        figure('units','normalized','outerposition',[0 0.3 0.3 0.6])
                        %subplot(2,1,1)
                        plot(PreNOBfile(:,2),'k')
                        title('arm position in y axis')
                        
                        
                        figure('units','normalized','outerposition',[0.3 0.3 0.3 0.6])
                        %subplot(2,1,2)
                        plot(PreCGfileMPJ)
                        title('finger movement column6')
                        
                        % plot(maxtab(:,1),maxtab(:,2),'r*')
                        hold on
                        plot(index1+Preoffset2,amplitude2,'r*')
                        plot(Preoffset2,PreCGfileMPJ(Preoffset2),'^g')
                        finger_point1=Preoffset2;
                        finger_point2=index1+Preoffset2;
                        amplitude1=PreCGfileMPJ(Preoffset2);
                        
                        pause(1);
                        
                        [keyIsDown,secs,keyCode] = KbCheck;
                        while (~(keyCode(80) || keyCode(70)|| keyCode(66))) % detect 'p' or 'f' press
                            [keyIsDown,secs,keyCode] = KbCheck;
                        end
                        
                        
                        if (keyCode(80))%p
                            
                            %finger_onset1= maxtab(1,1);
                            
%                            finger_point2= mintab(1,1);
%                            amplitude2=mintab(1,2);
%                            finger_point3= Preonset2;
%                            amplitude3=PreCGfileMPJ(Preonset2);
                            
                            close all
                            
                        elseif (keyCode(70)) %f
                            % subplot(2,1,2)
                            [x,y] = ginput(2);
                            finger_point1= x(1);
                            amplitude1=y(1);
                            finger_point2= x(2);
                            amplitude2=y(2);
                            figure('units','normalized','outerposition',[0.6 0.3 0.3 0.6])
                            %subplot(2,1,2)
                            plot(PreCGfileMPJ)
                            title('finger movement column6')
                            
                            % plot(maxtab(:,1),maxtab(:,2),'r*')
                            hold on
                            plot(finger_point2,amplitude2,'r*')
                            plot(finger_point1,amplitude1,'^g')
                            pause(2)
                            
                            close all
                            
                        else
                            finger_point1= 9999999;
                            amplitude1=9999999;
                            finger_point2= 9999999;
                            amplitude2=9999999;
                            finger_point3= 9999999;
                            amplitude3=9999999;
                            
                            close all
                        end
                        
                        %                          fingSYNCdata=cat(1,fingSYNCdata,{strtrim(subject_list(j,:)) CatiName dir impairedHand 'affected' objName char(Precycle_header(PreNOBfileNum,1)) finger_point1 finger_point2  amplitude1 amplitude2})
                        fingSYNCdata={strtrim(subject_list(j,:)) CatiName dir impairedHand 'affected' objName char(Precycle_header(PreNOBfileNum,1)) finger_point1 finger_point2  amplitude1 amplitude2};
                        
                        xlsappend([pathname1 '\Synchronized Finger Data_affected_HAT_2ndMove gf.xls'],fingSYNCdata)
                        
                        
                    end
                    
                end
            end
        end
    end
end
end


%
