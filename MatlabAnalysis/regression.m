

% [betaPCR10,a11,b11,c11,d11]=maryregression(spreadsheetName,sheetName,clinicalColumn)
clear
close all
clc
%this function can be used to visualize linear regression fit.

spreadsheetName='modeling-sprdsht-12pred-inc';
sheetName='predictions (12 predictors)';
clinicalColumn=1;
%%
dataset=spreadsheetName;
sheet=sheetName;

data=xlsread(dataset,sheet);

% a11=data(25:43,18:19);
% b11=data(25:43,34:40);
% c11=data(25:43,46:50);
% d11=data(25:43,75:82);
%kinematicdata=[a11 b11 c11 d11];
%
%kinematicdata=[data(:,5:9) data(:,11)]  
% 
 kinematicdata=data(:,4:end);
clinicaldata=data(:,clinicalColumn);

%%



%%

 X1=kinematicdata;
 stdr = std(X1);%...
 s=size(X1);
 n=s(1,1);
 X= X1./repmat(stdr,n,1);
 [PCALoadings,PCAScores,PCAVar] = princomp(X);
 y=clinicaldata;

 betaPCR10 = regress(y-mean(y), PCAScores);
 betaPCR10 = PCALoadings*betaPCR10;
 betaPCR10 = [mean(y) - mean(X)*betaPCR10; betaPCR10];
 yfitPCR10 = [ones(n,1) X]*betaPCR10;
  %axis([-150 150 -150 150])
 hold on

% axis([-120 20 -120 20])
 plot(y,yfitPCR10,'g*'); 
d='actual scores';
title(strcat(dataset, sheet))
ylabel('scores predicted by the model')
xlabel(d)
hold on
P = polyfit(y,yfitPCR10,1)
 %plot(P)
 display(P(1))
 li=polyval(P,y);
 hold on
plot(y,li,'r--')
PCAScores=[ones(size(PCAScores,1),1) PCAScores];
[b,bint,r,rint,stats] = regress(y-mean(y), PCAScores);
 betaPCR10=betaPCR10';

 





