clear all
close all

[value text] =xlsread('..\..\results\DataSheet\RTG_clinic_Kinematics_merged_5_6_2014.xlsx','Kinematics_clinic_no120');

% find pre 
prematrix=value(find(value(:,3)==1),:);
postmatrix=value(find(value(:,3)==2),:);
% find post-pre
diff21matrix=value(find(value(:,3)==21),:);
% find post-pre%
diff211matrix=value(find(value(:,3)==211),:);


% find each group
RTP=value(find(value(:,1)==3),:);
HAT=value(find(value(:,1)==2),:);
HAS=value(find(value(:,1)==1),:);

RTP_pre=RTP(find(RTP(:,3)==1),:);
RTP_post=RTP(find(RTP(:,3)==2),:);
RTP_21=RTP(find(RTP(:,3)==21),:);
RTP_211=RTP(find(RTP(:,3)==211),:);

HAT_pre=HAT(find(HAT(:,3)==1),:);
HAT_post=HAT(find(HAT(:,3)==2),:);
HAT_21=HAT(find(HAT(:,3)==21),:);
HAT_211=HAT(find(HAT(:,3)==211),:);

HAS_pre=HAS(find(HAS(:,3)==1),:);
HAS_post=HAS(find(HAS(:,3)==2),:);
HAS_21=HAS(find(HAS(:,3)==21),:);
HAS_211=HAS(find(HAS(:,3)==211),:);

left=0;
buttom=0;
heigth=1;
width=0.8;
MarkerSize=40;
fontsize=12;

for num1=5:size(RTP,2)
    for num2=5:size(RTP,2)
        [r1, p1]=corrcoef(prematrix(:,num1),prematrix(:,num2));
        [r2, p2]=corrcoef(postmatrix(:,num1),postmatrix(:,num2));
        if (p1(2,1)<=0.05)
            CorrelationOutput1(num1+1,num2+1)=p1(2,1);
        else
            CorrelationOutput1(num1+1,num2+1)='\';
        end
        if (p2(2,1)<=0.05)
            CorrelationOutput2(num1+1,num2+1)=p2(2,1);
        else
            CorrelationOutput2(num1+1,num2+1)='\';
        end

        [r21, p21]=corrcoef(diff21matrix(:,num1),diff21matrix(:,num2));
        if (p21(2,1)<=0.05)
            CorrelationOutput3(num1+1,num2+1)=p21(2,1);
        else
            CorrelationOutput3(num1+1,num2+1)='\';
        end
        
        [r211, p211]=corrcoef(diff211matrix(:,num1),diff211matrix(:,num2));
        if (p211(2,1)<=0.05)
            CorrelationOutput4(num1+1,num2+1)=p211(2,1);
        else
            CorrelationOutput4(num1+1,num2+1)='\';
        end
    end
    
end

xlswrite(['I:\RTG\results\Figures\PlotResults\correlationOutput.xls'],CorrelationOutput1,'pretest');
xlswrite(['I:\RTG\results\Figures\PlotResults\correlationOutput.xls'],CorrelationOutput2,'posttest');
xlswrite(['I:\RTG\results\Figures\PlotResults\correlationOutput.xls'],CorrelationOutput3,'post_pre');
xlswrite(['I:\RTG\results\Figures\PlotResults\correlationOutput.xls'],CorrelationOutput4,'post-pre%');
