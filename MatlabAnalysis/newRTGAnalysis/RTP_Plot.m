clear all
close all

[value text] =xlsread('..\..\results\DataSheet\RTG_clinic_Kinematics_merged_5_6_2014.xlsx','Kinematics_clinic_no120');

% find each group
RTP=value(find(value(:,1)==3),:);
HAT=value(find(value(:,1)==2),:);
HAS=value(find(value(:,1)==1),:);

RTP_pre=RTP(find(RTP(:,3)==1),:);
RTP_post=RTP(find(RTP(:,3)==2),:);
RTP_21=RTP(find(RTP(:,3)==21),:);
RTP_211=RTP(find(RTP(:,3)==211),:);

HAT_pre=HAT(find(HAT(:,3)==1),:);
HAT_post=HAT(find(HAT(:,3)==2),:);
HAT_21=HAT(find(HAT(:,3)==21),:);
HAT_211=HAT(find(HAT(:,3)==211),:);

HAS_pre=HAS(find(HAS(:,3)==1),:);
HAS_post=HAS(find(HAS(:,3)==2),:);
HAS_21=HAS(find(HAS(:,3)==21),:);
HAS_211=HAS(find(HAS(:,3)==211),:);

left=0;
buttom=0;
heigth=1;
width=0.8;
MarkerSize=40;
fontsize=12;

for columNum=5:size(RTP,2)
    figure('units','normalized','outerposition',[0 0 1 1])
    subplot(121)
    plot(RTP_21(:,4),RTP_21(:,columNum),'.b','MarkerSize',MarkerSize);
    hold on
    plot(HAT_21(:,4),HAT_21(:,columNum),'.m','MarkerSize',MarkerSize);
    plot(HAS_21(:,4),HAS_21(:,columNum),'.g','MarkerSize',MarkerSize);
    title('post-pre diff','fontsize',fontsize);
    ylabel(['change ' text(1,columNum+4)],'fontsize',fontsize);
    
    subplot(122)
    plot(RTP_211(:,4),RTP_211(:,columNum),'.b','MarkerSize',MarkerSize);
    hold on
    plot(HAT_211(:,4),HAT_211(:,columNum),'.m','MarkerSize',MarkerSize);
    plot(HAS_211(:,4),HAS_211(:,columNum),'.g','MarkerSize',MarkerSize);
    title('post-pre diff percent','fontsize',fontsize);
    ylabel(['change ' text(1,columNum+4)],'fontsize',fontsize);
    legend('RTP','HAT','HAS');
    saveppt('I:\RTG\results\Figures\PlotResults\RTP_diff_Plot.ppt');
    
%     pause
    close all
    
end

for columNum=5:size(RTP,2)
    figure('units','normalized','outerposition',[0 0 1 1])
    subplot(121)
    plot(RTP_pre(:,columNum),RTP_21(:,columNum),'.b','MarkerSize',MarkerSize);
    hold on
    plot(HAT_pre(:,columNum),HAT_21(:,columNum),'.m','MarkerSize',MarkerSize);
    plot(HAS_pre(:,columNum),HAS_21(:,columNum),'.g','MarkerSize',MarkerSize);
    title('post-pre diff','fontsize',fontsize);
    ylabel(['change ' text(1,columNum+4)],'fontsize',fontsize);
    
    subplot(122)
    plot(RTP_pre(:,columNum),RTP_211(:,columNum),'.b','MarkerSize',MarkerSize);
    hold on
    plot(HAT_pre(:,columNum),HAT_211(:,columNum),'.m','MarkerSize',MarkerSize);
    plot(HAS_pre(:,columNum),HAS_211(:,columNum),'.g','MarkerSize',MarkerSize);
    title('post-pre diff percent','fontsize',fontsize);
    ylabel(['change ' text(1,columNum+4)],'fontsize',fontsize);
    legend('RTP','HAT','HAS');
    saveppt('I:\RTG\results\Figures\PlotResults\RTP_diff_PlotOverPre.ppt');
    
%     pause
    close all
    
end

for columNum=5:size(RTP,2)
    figure('units','normalized','outerposition',[0 0 1 1])
    subplot(121)
    plot(RTP_21(:,2),RTP_21(:,columNum),'.b','MarkerSize',MarkerSize);
    hold on
    plot(HAT_21(:,2),HAT_21(:,columNum),'.m','MarkerSize',MarkerSize);
    plot(HAS_21(:,2),HAS_21(:,columNum),'.g','MarkerSize',MarkerSize);
    title('post-pre diff','fontsize',fontsize);
    xlim([0 3]);
    ylabel(['change ' text(1,columNum+4)],'fontsize',fontsize);
    
    subplot(122)
    plot(RTP_211(:,2),RTP_211(:,columNum),'.b','MarkerSize',MarkerSize);
    hold on
    plot(HAT_211(:,2),HAT_211(:,columNum),'.m','MarkerSize',MarkerSize);
    plot(HAS_211(:,2),HAS_211(:,columNum),'.g','MarkerSize',MarkerSize);
    xlim([0 3]);
    title('post-pre diff percent','fontsize',fontsize);
    ylabel(['change ' text(1,columNum+4)],'fontsize',fontsize);
    legend('RTP','HAT','HAS');
    saveppt('I:\RTG\results\Figures\PlotResults\RTP_diff_PlotOverHand.ppt');
    
%     pause
    close all
    
end