function [SynData] = SynFun( maryData,numberOfTrialsOBJ1,numberOfTrialsOBJ2)
%this is a function to synchronize 10 trials of each object .
for j=1:20
    w1=maryData(3,j,3);
    for i=1:numberOfTrialsOBJ1
        if maryData(3,j,i)>w1
            d1=maryData(3,j,i)-w1;
            maryData(:,j,i)=maryData(:,j,i)-d1;
        else
            d2=w1-maryData(3,j,i);
            maryData(:,j,i)=maryData(:,j,i)+d2;
        end
    end
end
for j=1:20
    w1=maryData(3,j,3);
    for i=numberOfTrialsOBJ1+1:numberOfTrialsOBJ2+numberOfTrialsOBJ1
        if maryData(3,j,i)>w1
            d1=maryData(3,j,i)-w1;
            maryData(:,j,i)=maryData(:,j,i)-d1;
        else
            d2=w1-maryData(3,j,i);
            maryData(:,j,i)=maryData(:,j,i)+d2;
        end
    end
end
        
ntotal=numberOfTrialsOBJ1+numberOfTrialsOBJ2;
% figure(10)
t=0:499;
t=t';
position=maryData(:,6,:);
If=size(position,1);
%figure(11)
for i=1:numberOfTrialsOBJ1
fingdif=difnum(position(:,i),1,If,.01,1,1,1);

% hold on    

% plot(fingdif)  
% pause(0.1)
Pvel(i)=(0.15*min(fingdif));
indices11 = find(fingdif<Pvel(i));
indices1(i)=min(indices11);

end

%m1=min(indices1);

Data1=[];
Data1M=[];
%first object
%figure(10)
for i=1:numberOfTrialsOBJ1
    
   
k=indices1(i);
% B1=position(1:500,i);
B1=maryData(1:max(size(maryData)),:,i);
tB1=t(1:max(size(maryData)));
e=tB1-abs(k);
%     hold on
%         plot(e,B1,'r')
        %pause(1)
%         if abs(k)>100
%         CM=B1(abs(k)-100:end,:);
%         elseif abs(k)>90
%         CM=B1(abs(k)-90:end,:);
%         elseif abs(k)>80
%         CM=B1(abs(k)-80:end,:);
%         elseif abs(k)>70
%         CM=B1(abs(k)-70:end,:);
        
%         if abs(k)>60
%         CM=B1(abs(k)-60:end,:);
%         elseif abs(k)>55
%         CM=B1(abs(k)-55:end,:);
%         elseif abs(k)>45
%         CM=B1(abs(k)-45:end,:);
%         elseif abs(k)>40
%         CM=B1(abs(k)-40:end,:);
%         if abs(k)>35
%         CM=B1(abs(k)-35:end,:);
%         elseif abs(k)>30
%            CM=B1(abs(k)-30:end,:); 
        if abs(k)>25
           CM=B1(abs(k)-25:end,:); 
        elseif abs(k)>20
           CM=B1(abs(k)-20:end,:);   
           
                  
        elseif abs(k)>15
        CM=B1(abs(k)-15:end,:);
        elseif abs(k)>10
        CM=B1(abs(k)-10:end,:);
        elseif abs(k)>5
        CM=B1(abs(k)-5:end,:);
        
        else
        CM=B1(abs(k)+1:end,:);
        end
        
if isempty(Data1M) | isequal(size(CM,1),size(Data1M,1))
    Data1M=cat(3,Data1M,CM);
    else
        k=size(CM,1)-size(Data1M,1);
        if k<0
            for j=1:abs(k)
                CM=[CM;CM(end,:)];
            end        
            Data1M=cat(3,Data1M,CM);
        end
        if k>0
            CM(end-k+1:end,:)=[];
            Data1M=cat(3,Data1M,CM);
        end
end 
end

%first object
figure(13) 
n2=numberOfTrialsOBJ1+1;
 for i=n2:ntotal
fingdif=difnum(position(:,i),1,If,.01,1,1,1);
%Tangvel=(sqrt(sum((fingdif.^2)')))';
% % Tangvel=procfilt(Tangvel,SF);
 
% hold on
% plot(fingdif,'k')
%pause(0.1)

Pvel2(i)=(0.15*min(fingdif));
indices12 = find(fingdif<Pvel2(i));
n3=n2-1;
indices2(i-n3)=min(indices12);

end
% indices2 = find(Pvel2)
% m2=min(indices2);

%figure(14)
 for i=n2:ntotal
    
    m=i;
    k=indices2(i-n3);
    
B=maryData(1:max(size(maryData)),:,i);
tB=t(1:max(size(maryData)));
e=tB-abs(k);
% figure(15)
% plot(Tangvel,'k')
%     hold on
%         plot(e,B,'g')
        %pause(1)
%         if abs(k)>15
%         C=B(abs(k)-15:end,:);
%         else
%           C=B(abs(k)+1:end,:);
%         end
        
%         if abs(k)>100
%         C=B(abs(k)-100:end,:);
%         elseif abs(k)>90
%         C=B(abs(k)-90:end,:);
%         elseif abs(k)>80
%         C=B(abs(k)-80:end,:);
%         elseif abs(k)>770
%         C=B(abs(k)-70:end,:);
%         if abs(k)>60
%         C=B(abs(k)-60:end,:);
%         elseif abs(k)>55
%         C=B(abs(k)-55:end,:);
%         elseif abs(k)>45
%         C=B(abs(k)-45:end,:);
%         elseif abs(k)>40
%         C=B(abs(k)-40:end,:);
%         if abs(k)>35
%         C=B(abs(k)-35:end,:);
%         elseif abs(k)>30
%            C=B(abs(k)-30:end,:); 
        if abs(k)>25
           C=B(abs(k)-25:end,:); 
        elseif abs(k)>20
           C=B(abs(k)-20:end,:);   
           
                  
        elseif abs(k)>15
        C=B(abs(k)-15:end,:);
        elseif abs(k)>10
        C=B(abs(k)-10:end,:);
elseif abs(k)>5
        C=B(abs(k)-5:end,:);
        
        else
        C=B(abs(k)+1:end,:);
        end




if isempty(Data1) | isequal(size(C,1),size(Data1,1))
    Data1=cat(3,Data1,C);
    else
        k=size(C,1)-size(Data1,1);
        if k<0
            for j=1:abs(k)
                C=[C;C(end,:)];
            end        
            Data1=cat(3,Data1,C);
        end
        if k>0
            C(end-k+1:end,:)=[];
            Data1=cat(3,Data1,C);
        end
end 
 end
 g1=max(size(Data1));
 g2=max(size(Data1M));
 g=min(g1,g2);
 if g1>g2
     Data1(g2+1:end,:,:)=[];
 else
     Data1M(g1+1:end,:,:)=[];
 end
 SynData=cat(3,Data1M,Data1)
 
   
 

end

