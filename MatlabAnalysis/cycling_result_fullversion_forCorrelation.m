function cycling_result_fullversion_forCorrelation(pathname1)

clc

subject_list = ls(pathname1);
totalCount=1;
outputList={'init' 'hand' 'test' 'object' 'initNum'  'filename' 'ART' 'ATTPV' 'APV' 'ATTDecc' 'ApercTTDecc' 'ATAPVoffset1' 'ATAPVonset2' 'HAperRT' 'HPAper' 'HTTPAper' 'HpercTTPAper'};

%for j = 3:size(subject_list,1)
for j =3:size(subject_list,1)
    pathname = [pathname1 '\' strtrim(subject_list(j,:))];
    for i=1:3
        for m=1:2
            for k=1:4
                switch m
                    case 1
                        impairedHand = ['left'];
                        glovePre=['LCG'];
                    case 2
                        impairedHand = ['right'];
                        glovePre=['RCG'];
                end
                
                
                switch i
                    case 1
                        dir = ['baseline'];
                    case 2
                        dir = ['pretest'];
                    case 3
                        dir = ['posttest'];
                end
                
                switch k
                    case 1
                        Object = ['bigcube'];
                    case 2
                        Object = ['bigcircle'];
                    case 3
                        Object = ['smallcube'];
                    case 4
                        Object = ['smallcircle'];
                    case 5
                        Object = ['pentagon'];
                    case 6
                        Object = ['hugecircle'];
                    case 7
                        Object = ['wedge'];
                    case 8
                        Object = ['cylinder'];
                end
                
                dirname = [pathname '\' dir '\' impairedHand '\' Object '\OutputData\*1.xls'];
                files = ls(dirname);
                Cycleddirname=[pathname '\' dir '\' impairedHand '\' Object '\Cycled.xls'];
                if (size(files,1)>=1)
                    fname = [pathname '\' dir '\' impairedHand '\' Object '\OutputData\' files(1,:)]
                    data_output = xlsread(fname);
                    
                    [A1_output, A1_headertext] = xlsread(Cycleddirname);
                    
                    % find bad trial
                    [rrr, ccc] = find (data_output(:,1:4) == -999999);
                    rrrr=unique(rrr);
                    for (n=1:length(rrrr))
                        data_output(rrrr(length(rrrr)-n+1),:)= [];
                    end
                    
                    [rrr, ccc] = find (A1_output(:,1:2) == -999999);
                    rrrr=unique(rrr);
                    for (n=1:length(rrrr))
                        A1_output(rrrr(length(rrrr)-n+1),:)= [];
                        A1_headertext(rrrr(length(rrrr)-n+1),:)= [];
                    end
                    
                    if size(A1_output,1)>0
                        for filecount=1:size(A1_output,1)
                            A1_startindex_1 = A1_output(filecount,1);
                            A1_endindex_1 = A1_output(filecount,2); % movement one
                            if (A1_endindex_1==-999999)
                                A1_endindex_1=A1_output(filecount,2);
                            end
                            
                            
                            NOBFilename = [pathname '\' dir '\' impairedHand '\' Object '\' char(A1_headertext(filecount,1))]
                            GBFilename = ['X:\LDA_backup\data\reachGrasp\controlGroup_raw\' strtrim(subject_list(j,:)) '\' dir '\' impairedHand '\' Object '\' regexprep(char(A1_headertext(filecount,1)),'NOB',glovePre)]
                            %                         GBFilename = ['Y:\reachGrasp\' strtrim(subject_list(j,:)) '\' dir '\' impairedHand '\' Object '\' regexprep(char(A1_headertext(filecount,1)),'NOB',glovePre)]
                            savedfilename(filecount,:)=GBFilename(end-7:end-4);
                            NOBFileData1=load(NOBFilename);
                            GBFileData1=load(GBFilename);
                            [b,a]=butter(2,8/50);
                            NOBFileData = filtfilt(b,a,NOBFileData1);
                            GBFileData = filtfilt(b,a,GBFileData1);
                            
                            if (A1_endindex_1 > size(NOBFileData,1))
                                DataNOB = NOBFileData(A1_startindex_1:end,:);
                                DataGB = GBFileData(A1_startindex_1:end,:);
                            else
                                DataNOB = NOBFileData(A1_startindex_1:A1_endindex_1,:);
                                DataGB = GBFileData(A1_startindex_1:A1_endindex_1,:);
                            end
                            
                            % calculate arm data
                            DataNOBVel=diff(DataNOB);
                            [b,a]=butter(2,8/50);
                            DataNOBVelfil=filtfilt(b,a,DataNOBVel);
                            DataNOBAcc=diff(DataNOBVel);
                            [val DataNOBAccIndices]=min(DataNOBAcc(:,1));
                            if (size(DataNOBAccIndices,1)==0)
                                TimetoDecc(filecount)=DataNOBAcc(end,1);
                            else
                                TimetoDecc(filecount)=DataNOBAccIndices(1);
                            end
                            PercentageTTDecc(filecount)=TimetoDecc(filecount)/size(DataNOB,1);
                            
                            
                            % calculate glove data
                            [MAXTAB, MINTAB] = peakdet(DataGB(:,5), 0.1);
                            if (size(MAXTAB,1)==0)
                                TimetoPeakAperture(filecount)=size(DataGB,1);
                                PeakAperture(filecount)=DataGB(end,5);
                            else
                                TimetoPeakAperture(filecount)=MAXTAB(1,1);
                                PeakAperture(filecount)=MAXTAB(1,2);
                            end
                            
                            DataGBVel=diff(GBFileData);
                            [b,a]=butter(2,8/50);
                            DataGBVelfil=filtfilt(b,a,DataGBVel);
                            DataGBVelfilIndices=find(DataGBVelfil(:,5)>(0.1*max(DataGBVelfil(:,5))));
                            ApertureRT(filecount)=DataGBVelfilIndices(1);
                            PercentageTTPAper(filecount)=TimetoPeakAperture(filecount)/size(DataGB,1);
                        end
                        
                        
                        % save in a structure 'cycleresult'
                        
                        cycleresult(totalCount).init=strtrim(subject_list(j,:));
                        cycleresult(totalCount).initNum=j-1; % subjects' number
                        cycleresult(totalCount).test=dir; % pretest 1, posttest 2
                        cycleresult(totalCount).hand=impairedHand;
                        cycleresult(totalCount).object= Object; % bigcube 1, bigcircle 2, smallcube 3, smallcircle 4
                        
                        for mm=1:size(data_output,1)
                            if (size(data_output,2)==20)
                                cycleresult(totalCount).ART=10*(data_output(mm,13));
                                cycleresult(totalCount).ATTPV=10*(data_output(mm,3));
                                cycleresult(totalCount).APV=(data_output(mm,2));
                                cycleresult(totalCount).ATTDecc=10*(TimetoDecc);
                                cycleresult(totalCount).ApercTTDecc=PercentageTTDecc(mm);
                                cyclereuslt(totalCount).ATAPVoffset1=(data_output(mm,14)-data_output(mm,13)-data_output(mm,3));
                                cycleresult(totalCount).ATAPVonset2=10*(data_output(mm,15)-data_output(mm,13)-data_output(mm,3));
                                
                            else
                                cycleresult(totalCount).ART=10*(data_output(mm,17));
                                cycleresult(totalCount).ATTPV=10*(data_output(mm,3));
                                cycleresult(totalCount).APV=(data_output(mm,2));
                                cycleresult(totalCount).ATTDecc=10*(TimetoDecc(mm));
                                cycleresult(totalCount).ApercTTDecc=(PercentageTTDecc(mm));
                                cyclereuslt(totalCount).ATAPVoffset1=(data_output(mm,4));
                                cycleresult(totalCount).ATAPVonset2=10*(data_output(mm,19)-data_output(mm,17)-data_output(mm,3));
                            end
                            
                            cycleresult(totalCount).HAperRT=10*(ApertureRT(mm));
                            cycleresult(totalCount).HPAper=(PeakAperture(mm));
                            cycleresult(totalCount).HTTPAper=10*(TimetoPeakAperture(mm));
                            cycleresult(totalCount).HpercTTPAper=(PercentageTTPAper(mm));
                            cycleresult(totalCount).filename = savedfilename(mm,:);
                            
                            outputList= cat(1,outputList,{cycleresult(totalCount).init cycleresult(totalCount).hand cycleresult(totalCount).test cycleresult(totalCount).object ...
                                cycleresult(totalCount).initNum cycleresult(totalCount).filename cycleresult(totalCount).ART cycleresult(totalCount).ATTPV cycleresult(totalCount).APV ... 
                                cycleresult(totalCount).ATTDecc cycleresult(totalCount).ApercTTDecc cyclereuslt(totalCount).ATAPVoffset1 cycleresult(totalCount).ATAPVonset2 cycleresult(totalCount).HAperRT ...
                                cycleresult(totalCount).HPAper cycleresult(totalCount).HTTPAper cycleresult(totalCount).HpercTTPAper});
                        end
                        totalCount=totalCount+1;
                        
                        clear TimetoDecc;
                        clear PercentageTTDecc;
                        clear ApertureRT;
                        clear PeakAperture;
                        clear TimetoPeakAperture;
                        clear PercentageTTPAper;
                    else
                        % save in a structure 'cycleresult'
                        
                        cycleresult(totalCount).init=strtrim(subject_list(j,:));
                        cycleresult(totalCount).initNum=j-1;
                        cycleresult(totalCount).test=i; % pretest 1, posttest 2
                        cycleresult(totalCount).hand=impairedHand;
                        cycleresult(totalCount).filename = '';
                        cycleresult(totalCount).object=k; % bigcube 1, bigcircle 2, smallcube 3, smallcircle 4
                        
                        cycleresult(totalCount).ART=0;
                        cycleresult(totalCount).ATTPV=0;
                        cycleresult(totalCount).APV=0;
                        cycleresult(totalCount).ATTDecc=0;
                        cycleresult(totalCount).ApercTTDecc=0;
                        cyclereuslt(totalCount).ATAPVoffset1=0;
                        cycleresult(totalCount).ATAPVonset2=0;
                        
                        cycleresult(totalCount).HAperRT=0;
                        cycleresult(totalCount).HPAper=0;
                        cycleresult(totalCount).HTTPAper=0;
                        cycleresult(totalCount).HpercTTPAper=0;
                        
                            outputList= cat(1,outputList,{cycleresult(totalCount).init cycleresult(totalCount).hand cycleresult(totalCount).test cycleresult(totalCount).object ...
                                cycleresult(totalCount).initNum cycleresult(totalCount).filename cycleresult(totalCount).ART cycleresult(totalCount).ATTPV cycleresult(totalCount).APV ... 
                                cycleresult(totalCount).ATTDecc cycleresult(totalCount).ApercTTDecc cyclereuslt(totalCount).ATAPVoffset1 cycleresult(totalCount).ATAPVonset2 cycleresult(totalCount).HAperRT ...
                                cycleresult(totalCount).HPAper cycleresult(totalCount).HTTPAper cycleresult(totalCount).HpercTTPAper});
                        totalCount=totalCount+1;
                    end
                end
            end
        end
    end
end


% outputList={cycleresult(1).init cycleresult(1).test cycleresult(1).hand cycleresult(1).object cycleresult(1).ART cycleresult(1).ATTPV cycleresult(1).APV cycleresult(1).ATTDecc cycleresult(1).ApercTTDecc cyclereuslt(1).ATAPVoffset1 cycleresult(1).ATAPVonset1 cycleresult(1).HAperRT cycleresult(1).HPAper cycleresult(1).HTTPAper cycleresult(1).HpercTTPAper};

% for finalcount=1:totalCount-1
%     cycleresult(finalcount).init
%     outputList= cat(1,outputList,{cycleresult(finalcount).init cycleresult(finalcount).initNum cycleresult(finalcount).test cycleresult(finalcount).hand cycleresult(finalcount).object cycleresult(finalcount).ART cycleresult(finalcount).ATTPV cycleresult(finalcount).APV cycleresult(finalcount).ATTDecc cycleresult(finalcount).ApercTTDecc cyclereuslt(finalcount).ATAPVoffset1 cycleresult(finalcount).ATAPVonset2 cycleresult(finalcount).HAperRT cycleresult(finalcount).HPAper cycleresult(finalcount).HTTPAper cycleresult(finalcount).HpercTTPAper});
% end

xlswrite([pathname1 '\control NonAveragecycledResult All for Correlation.xls'],outputList);

