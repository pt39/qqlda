
function [Pre_mean_elbowAngle, Post_mean_elbowAngle]=Elbow_Angle(PreData_acrossTrial,PostData_acrossTrial)
% calculate elbow angle change
% Vector shoulder-> elbow

% pretest

for preCount=1:size(PreData_acrossTrial,3)
    pre_V_elbow_shoulder=PreData_acrossTrial(:,13:15,preCount)-PreData_acrossTrial(:,7:9,preCount);
    pre_V_elbow_wrist=PreData_acrossTrial(:,1:3,preCount)-PreData_acrossTrial(:,7:9,preCount);
    
    % magnitude
    for (i=1:size(pre_V_elbow_shoulder,1))
        pre_M_elbow_shoulder(i)=norm(pre_V_elbow_shoulder(i,:));
        pre_M_elbow_wrist(i)=norm(pre_V_elbow_wrist(i,:));
    end
    
    for (j=1:size(pre_V_elbow_shoulder,1))
        pre_elbowAngle(j,preCount)=acosd(dot(pre_V_elbow_shoulder(j,:),pre_V_elbow_wrist(j,:))/(pre_M_elbow_shoulder(j)*pre_M_elbow_wrist(j)));
    end
end

Pre_mean_elbowAngle=mean(pre_elbowAngle(:,1:preCount),2);
Pre_std_elbowAngle=std(pre_elbowAngle(:,1:preCount),0,2);

Pre_up_elbowAngle=Pre_mean_elbowAngle+Pre_std_elbowAngle;
Pre_down_elbowAngle=Pre_mean_elbowAngle-Pre_std_elbowAngle;

pre_x1=1:1:size(Pre_up_elbowAngle);
pre_x2=1:1:size(Pre_down_elbowAngle);
pre_X = [pre_x1 fliplr(pre_x2)];
pre_Y = [Pre_up_elbowAngle' fliplr(Pre_down_elbowAngle')];
fill(pre_X,pre_Y,[0.5 0.5 0.5]);
hold on

% 
% posttest
for postCount=1:size(PostData_acrossTrial,3)
    post_V_elbow_shoulder=PostData_acrossTrial(:,13:15,postCount)-PostData_acrossTrial(:,7:9,postCount);
    post_V_elbow_wrist=PostData_acrossTrial(:,1:3,postCount)-PostData_acrossTrial(:,7:9,postCount);
    
    % magnitude
    for (i=1:size(post_V_elbow_shoulder,1))
        post_M_elbow_shoulder(i)=norm(post_V_elbow_shoulder(i,:));
        post_M_elbow_wrist(i)=norm(post_V_elbow_wrist(i,:));
    end
    
    for (j=1:size(post_V_elbow_shoulder,1))
        post_elbowAngle(j,postCount)=acosd(dot(post_V_elbow_shoulder(j,:),post_V_elbow_wrist(j,:))/(post_M_elbow_shoulder(j)*post_M_elbow_wrist(j)));
    end
end

Post_mean_elbowAngle=mean(post_elbowAngle(:,1:postCount),2);
Post_std_elbowAngle=std(post_elbowAngle(:,1:postCount),0,2);

Post_up_elbowAngle=Post_mean_elbowAngle+Post_std_elbowAngle;
Post_down_elbowAngle=Post_mean_elbowAngle-Post_std_elbowAngle;

post_x1=1:1:size(Post_up_elbowAngle);
post_x2=1:1:size(Post_down_elbowAngle);
post_X = [post_x1 fliplr(post_x2)];
post_Y = [Post_up_elbowAngle' fliplr(Post_down_elbowAngle')];
fill(post_X,post_Y,[0.8 0 1]);
