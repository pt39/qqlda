function unImpaired_wrist_Velocity()
clear all
close all
clc

[data text] = xlsread('../results/DataSheet/RTG ALL with average 11_07_2013_imp_unImp.xls','Unimpaired Latest Finger+elbow');

condition_old = [];
figure

for i=1:size(data,1)
    subInit = char(text(i+1,1));
    group_temp = char(text(i+1,2));
    test = char(text(i+1,3));
    hand = char(text(i+1,4));
    object = char(text(i+1,6));
    filename = char(text(i+1,7));
    
    switch group_temp
        case 'HAT'
            group = 'HAT Cycled';
        case 'HAS'
            group = 'HAS Cycled';
        case 'Controls'
            group = 'Control Group Cycled';
    end
    
    onset1 = data(i,32)/10;
    offset1 = data(i,33)/10;
    onset2 = data(i,34)/10;
    offset2 = data(i,35)/10;
    
    condition_new = ['../CYCLED_GoodSide/' group '/' subInit '/' hand '/' object];
    
    if strcmp(condition_old, condition_new)
        hold on
    else
        saveppt('unImpaired_RTGWrisrVelocity.ppt');
        close all
        figure;
              
    end
 
    % wrist
    fileDirectory = ['../CYCLED_GoodSide/' group '/' subInit '/' test '/' hand '/' object '/' filename];
    NOBfile = load(fileDirectory);
    
    Arm = [NOBfile(1:onset2,4),NOBfile(1:onset2,2),NOBfile(1:onset2,3)];
    If = size(Arm,1);
    handdif1=difnum(Arm,3,If,.01,1,1,1);
    Tangvel=(sqrt(sum((handdif1.^2)')))';
    Tangvel=procfilt(Tangvel,100);
    Tangvel=Tangvel';
    
    if strcmp(test,'pretest')
        plot(Tangvel);
    else
        plot(Tangvel,'r');
    end
    
    
    title(condition_new);
    condition_old =condition_new;

    
end