function import_fingercycling()
clear all
close all
clc

[data text] = xlsread('R:\LDA_backup\data\LDA\scripts\RTG_merged_12_2_2013.xls','Unimpaired Latest Finger+elbow');

condition_old = [];
figure

%for i=1139:size(data,1)
for i=1:30
    
st=[];
fdata=[];
    subInit = char(text(i+1,1));
    group_temp = char(text(i+1,2));
    test = char(text(i+1,3));
    hand = char(text(i+1,4));
    object = char(text(i+1,6));
    filename = char(text(i+1,7));
            
    switch group_temp
        case 'HAT'
            group = 'HAT Cycled';
        case 'HAS'
            group = 'HAS Cycled';
        case 'Controls'
            group = 'Control Group Cycled';
    end
   
   dir=['R:\LDA_backup\data\reachGrasp(processed)\CYCLED_GoodSide\' group '\' subInit '\' test '\' hand '\' object]
   cd(dir)
       xlswrite('fingercycle1.xls',text(1,1:25));
        
    onset1 = data(i,13)/10;
    offset1 = data(i,14)/10;
    onset2 = data(i,15)/10;
    offset2 = data(i,16)/10;
    fdata=data(i,:);
    condition_new = ['R:\LDA_backup\data\reachGrasp(processed)\CYCLED_GoodSide\' group '\' subInit '\' test '\' hand '\' object];
    
    st=cat(1,st,{subInit group test hand 'unaffected' object filename});
    fdata1=cat(2,st,{fdata(1) fdata(2) fdata(3) fdata(4) fdata(5) fdata(6) fdata(7) fdata(8) fdata(9) fdata(10) fdata(11) fdata(12) fdata(13) fdata(14) fdata(15) fdata(16) fdata(17) fdata(18)});          

    if strcmp(condition_old, condition_new)
     xlsappend('fingercycle1.xls',fdata1);

    else
        %saveppt('UnImpaired_RTGWrisrTrajectory.ppt');
        close all
%         figure;
              
    end
%     fileDirectory = ['R:\LDA_backup\data\reachGrasp(processed)\CYCLED_GoodSide\' group '\' subInit '\' test '\' hand '\' object '\' filename];
%     NOBfile = load(fileDirectory);
%     if strcmp(test,'pretest')
%         plot(NOBfile(onset1:onset2,4), NOBfile(onset1:onset2,2));
%     else
%         plot(NOBfile(onset1:onset2,4), NOBfile(onset1:onset2,2),'r');
%     end
   % title(condition_new);
    condition_old =condition_new;
    
end
    
    
