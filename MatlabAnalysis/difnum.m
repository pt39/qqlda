%%%%fingdif=difnum(position,3,If,.01,1,1,1);
%%%%If=size(position,1);

function [deriv]=difnum(f,S,I,deltat,s1,s2,s3)

for s=1:S
   
   if s==1
      n=s1;
   elseif s==2
      n=s2;
   elseif s==3 
      n=s3;
   end
   
   
   r0=0;
   
   for j=1:n
      r0=r0+j^2;
   end
   
   R=2*deltat*r0;
   i1=n+1;
   i2=I-n;
   
   for i=i1:i2
      r5=0;
      for j=1:n
         r5=r5+j*(f(i+j,s)-f(i-j,s));
      end
      
      V(i)=r5/R;
   end
   
   for i=1:i1-1
      V(i)=V(i1);
   end
   
   for i=i2+1:I
      V(i)=V(i2);
   end
   
   for i=1:I
      deriv(i,s)=(V(i))';
   end
      
end

         
        