function [SynData] = Synabsolutethreshold_singleObj( maryData,numberOfTrialsOBJ1,index_selected)
%this is a function to synchronize 10 trials of each object .
%%
%h=37;

h=ginput(1);
% maryData(150:end,:,2)=[];
% maryData(1:150,:,2)=maryData(150:300,:,2);
for j=1:numberOfTrialsOBJ1
    w1=maryData(3,j,3);
    for i=1:numberOfTrialsOBJ1
        if maryData(3,j,i)>w1
            d1=maryData(3,j,i)-w1;
            maryData(:,j,i)=maryData(:,j,i)-d1;
        else
            d2=w1-maryData(3,j,i);
            maryData(:,j,i)=maryData(:,j,i)+d2;
            
        end
    end
    % maryData(:,j,10)=[];
end
%%
ntotal=numberOfTrialsOBJ1;
% figure(10)
t=0:max(size(maryData));
t=t';
position=maryData(:,index_selected+3,:); % sync based on selected joint angle
If=size(position,1);
%figure(11)
for i=1:numberOfTrialsOBJ1
    
    
    indices11 = find(position(:,:,i)>h(1,2));
    indices1(i)=min(indices11);
    
end

%m1=min(indices1);

Data1=[];
Data1M=[];
%first object
%figure(10)
for i=1:numberOfTrialsOBJ1
    
    
    k=indices1(i);
    % B1=position(1:500,i);
    B1=maryData(1:max(size(maryData)),:,i);
    tB1=t(1:max(size(maryData)));
    e=tB1-abs(k);
    
    
    if abs(k)>15
        CM=B1(abs(k)-15:end,:);
    elseif abs(k)>10
        CM=B1(abs(k)-10:end,:);
    elseif abs(k)>5
        CM=B1(abs(k)-5:end,:);
        
    else
        CM=B1(abs(k)+1:end,:);
    end
    
    if isempty(Data1M) | isequal(size(CM,1),size(Data1M,1))
        Data1M=cat(3,Data1M,CM);
    else
        k=size(CM,1)-size(Data1M,1);
        if k<0
            for j=1:abs(k)
                CM=[CM;CM(end,:)];
            end
            Data1M=cat(3,Data1M,CM);
        end
        if k>0
            CM(end-k+1:end,:)=[];
            Data1M=cat(3,Data1M,CM);
        end
    end
end
% g1=max(size(Data1));
% g2=max(size(Data1M));
% g=min(g1,g2);
% if g1>g2
%     Data1(g2+1:end,:,:)=[];
% else
%     Data1M(g1+1:end,:,:)=[];
% end
% SynData=cat(3,Data1M,Data1)
SynData=Data1M;



end

