%
function [syndata,fingerOnset1,fingerOnset2] =SynFinger(OriginalData1,pathname1,timenorm,ia, object)
%ia=2 is pretest
% ia=3 is post
    noftrials = size(OriginalData1,3);
    [data txt]=xlsread('R:\LDA_backup\data\LDA\scripts\RTG_merged_12_2_2013');
    a=find(pathname1 == '\');
    subj = pathname1(a(end)+1:end);
    
    b =a(end-1)+1;
    c = find(pathname1(b:end)== ' ' );
    c = c(1)-2;
    group = pathname1(b:b+c);
    
    if (strcmp(group, 'RTG'))
        group = 'Controls';
    end
    
    if (ia ==2)
        test = 'pretest';
    else
        test = 'posttest';
    end
    
    names = {txt{2:end,1}};
    groups = {txt{2:end,2}};
    tests = {txt{2:end,3}};
    objects={txt{2:end,6}};
    
    
    subjectStart = 1; 
    while(~strcmp(names{subjectStart}, subj))
        subjectStart = subjectStart + 1;
    end
   
    groupStart = subjectStart;
    while(~strcmp(groups{groupStart}, group))
        groupStart= groupStart +1;
    end
    
    testStart = groupStart;
    while(~strcmp(tests{testStart}, test))
        testStart= testStart + 1;
    end
    
    shapeStart = testStart;
    while(~strcmp(objects{shapeStart}, object))
        shapeStart= shapeStart + 1;
    end
    maxOnset=max(data(shapeStart:shapeStart+noftrials-1, 1))/10;
    indexnum=20;
    fixLength = size(OriginalData1,1) - (maxOnset-indexnum-1);
    syndata = zeros(fixLength, size(OriginalData1,2), size(OriginalData1,3));
    for i = 0:noftrials-1
       %clean data
       fingerOnset1 = data(shapeStart+i, 1)/10;
       fingerOnset2 = data(shapeStart+i, 3)/10;
       fprintf('%f, %f\n', fingerOnset1, fingerOnset2);
       
       curdata=OriginalData1(:,:,i+1);
       
       curdata=curdata(fingerOnset1-indexnum:end,:);
       
       curdata=curdata(1:fixLength,:);
       syndata(:,:,i+1) = curdata;
       
    end
    
  %synchronizing the data based on fingeronset1
  
    
    
end