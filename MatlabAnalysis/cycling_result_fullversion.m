function cycling_result_fullversion(pathname1)

clc
SF = 100;
subject_list = ls(pathname1);
totalCount=1;
%for j = 3:size(subject_list,1)
for j =2:size(subject_list,1)
    pathname = [pathname1 '\' strtrim(subject_list(j,:))];
    for i=2:3
        for m=1:2
            for k=1:4
                switch m
                    case 1
                        impairedHand = ['left'];
                        glovePre=['LCG'];
                    case 2
                        impairedHand = ['right'];
                        glovePre=['RCG'];
                end
                
                
                switch i
                    case 1
                        dir = ['baseline'];
                    case 2
                        dir = ['pretest'];
                    case 3
                        dir = ['posttest'];
                end
                
                switch k
                    case 1
                        Object = ['bigcube'];
                    case 2
                        Object = ['bigcircle'];
                    case 3
                        Object = ['smallcube'];
                    case 4
                        Object = ['smallcircle'];
                    case 5
                        Object = ['pentagon'];
                    case 6
                        Object = ['hugecircle'];
                    case 7
                        Object = ['wedge'];
                    case 8
                        Object = ['cylinder'];
                end
                
                dirname = [pathname '\' dir '\' impairedHand '\' Object '\OutputData\*1.xls'];
                files = ls(dirname);
                Cycleddirname=[pathname '\' dir '\' impairedHand '\' Object '\Cycled.xls'];
                if (size(files,1)>=1)
                    fname = [pathname '\' dir '\' impairedHand '\' Object '\OutputData\' files(1,:)]
                    data_output = xlsread(fname);
                    
                    [A1_output, A1_headertext] = xlsread(Cycleddirname);
                    
                    % find bad trial
                    [rrr, ccc] = find (data_output(:,1:9) == -999999);
                    rrrr=unique(rrr);
                    for (n=1:length(rrrr))
                        data_output(rrrr(length(rrrr)-n+1),:)= [];
                    end
                    
                    [rrr, ccc] = find (A1_output(:,1:6) == -999999);
                    rrrr=unique(rrr);
                    for (n=1:length(rrrr))
                        A1_output(rrrr(length(rrrr)-n+1),:)= [];
                        A1_headertext(rrrr(length(rrrr)-n+1),:)= [];
                    end
                    
                    if size(A1_output,1)>0
                        for filecount=1:size(A1_output,1)
                            A1_startindex_1 = A1_output(filecount,1);
                            A1_endindex_1 = A1_output(filecount,3); % movement one
                            if (A1_endindex_1==-999999)
                                A1_endindex_1=A1_output(filecount,2);
                            end
                            
                            
                            NOBFilename = [pathname '\' dir '\' impairedHand '\' Object '\' char(A1_headertext(filecount,1))]
                            GBFilename = ['Y:\reachGrasp\' strtrim(subject_list(j,:)) '\' dir '\' impairedHand '\' Object '\' regexprep(char(A1_headertext(filecount,1)),'NOB',glovePre)]
                            %                         GBFilename = ['Y:\reachGrasp\' strtrim(subject_list(j,:)) '\' dir '\' impairedHand '\' Object '\' regexprep(char(A1_headertext(filecount,1)),'NOB',glovePre)]
                            
                            NOBFileData1=load(NOBFilename);
                            GBFileData1=load(GBFilename);
                            [b,a]=butter(2,8/50);
                            NOBFileData = filtfilt(b,a,NOBFileData1);
                            GBFileData = filtfilt(b,a,GBFileData1);
                            
                            if (A1_endindex_1 > size(NOBFileData,1))
                                DataNOB = NOBFileData(A1_startindex_1:end,:);
                                DataGB = GBFileData(A1_startindex_1:end,:);
                            else
                                DataNOB = NOBFileData(A1_startindex_1:A1_endindex_1,:);
                                DataGB = GBFileData(A1_startindex_1:A1_endindex_1,:);
                            end
                            
                            % calculate arm data
                            DataNOBVel=diff(DataNOB);
                            [b,a]=butter(2,8/50);
                            DataNOBVelfil=filtfilt(b,a,DataNOBVel);
                            DataNOBAcc=diff(DataNOBVel);
                            [val DataNOBAccIndices]=min(DataNOBAcc(:,1));
                            if (size(DataNOBAccIndices,1)==0)
                                TimetoDecc(filecount)=DataNOBAcc(end,1);
                            else
                                TimetoDecc(filecount)=DataNOBAccIndices(1);
                            end
                            PercentageTTDecc(filecount)=TimetoDecc(filecount)/size(DataNOB,1);
                            
 
                            X=DataNOB(:,1);
                            Y=DataNOB(:,2);
                            Z=DataNOB(:,3);
                            % calculate path length
                            for n=1:length(X)-1
                                A(n)=sqrt((X(n+1)-X(n))^2+(Y(n+1)-Y(n))^2+(Z(n+1)-Z(n))^2);
                            end
                            
                            for n=2:length(X)-1
                                A(n)=A(n-1) + A(n);
                            end
                            
                            A=[A(1); A'];
                            Cumpath=A(length(X));
                            clear A
                            
                            % calculate movement duration
                            Duration=(1/SF)*(length(X));

                            % calculate smoothness
                            Arm=[DataNOB(:,1),DataNOB(:,2),DataNOB(:,3)];
                            If=size(Arm,1);
                            handdif1=difnum(Arm,3,If,.01,1,1,1);
                            Tangvel=(sqrt(sum((handdif1.^2)')))';
                            Tangvel=procfilt(Tangvel,SF);
                            Tangvel=Tangvel';
                            handdif2=difnumt(Tangvel,.01);
                            Acceleration=procfilt(handdif2,SF);
                            Tangacc=Acceleration';
                            handdif3=difnumt(Tangacc,.01);
                            Jerk=procfilt(handdif3,SF);
                            Tangjerk=Jerk';
                            q=Tangjerk.^2;
                            integrq=integrfixed(q,1/SF);               
                            tsmooth(filecount)=sqrt(.5*integrq*Duration.^5/Cumpath.^2);                            

                            % calculate glove data
                            [MAXTAB, MINTAB] = peakdet(DataGB(:,5), 0.1);
                            if (size(MAXTAB,1)==0)
                                TimetoPeakAperture(filecount)=size(DataGB,1);
                                PeakAperture(filecount)=DataGB(end,5);
                            else
                                TimetoPeakAperture(filecount)=MAXTAB(1,1);
                                PeakAperture(filecount)=MAXTAB(1,2);
                            end
                            
                            DataGBVel=diff(GBFileData);
                            [b,a]=butter(2,8/50);
                            DataGBVelfil=filtfilt(b,a,DataGBVel);
                            DataGBVelfilIndices=find(DataGBVelfil(:,5)>(0.05*max(DataGBVelfil(:,5))));
                            ApertureRT(filecount)=DataGBVelfilIndices(1);
                            PercentageTTPAper(filecount)=TimetoPeakAperture(filecount)/size(DataGB,1);
                            
                        end
                        
                        
                        % save in a structure 'cycleresult'
                        
                        cycleresult(totalCount).init=strtrim(subject_list(j,:));
                        cycleresult(totalCount).initNum=j-1; % subjects' number
                        cycleresult(totalCount).test=i; % pretest 1, posttest 2
                        cycleresult(totalCount).hand=impairedHand;
                        cycleresult(totalCount).object=k; % bigcube 1, bigcircle 2, smallcube 3, smallcircle 4
                        
                        if (size(data_output,2)==20)
                            
                            cycleresult(totalCount).ART=10*mean(data_output(:,13));
                            cycleresult(totalCount).ATTPV=10*mean(data_output(:,3));
                            cycleresult(totalCount).APV=mean(data_output(:,2));
                            cycleresult(totalCount).ATTDecc=10*mean(TimetoDecc);
                            cycleresult(totalCount).ApercTTDecc=mean(PercentageTTDecc);
                            cyclereuslt(totalCount).ATAPVoffset1=mean(data_output(:,14)-data_output(:,13)-data_output(:,3));
                            cycleresult(totalCount).ATAPVonset2=10*mean(data_output(:,15)-data_output(:,13)-data_output(:,3));
                            
                           
                        else
                            
                            cycleresult(totalCount).ART=10*mean(data_output(:,17));
                            cycleresult(totalCount).ATTPV=10*mean(data_output(:,3));
                            cycleresult(totalCount).APV=mean(data_output(:,2));
                            cycleresult(totalCount).ATTDecc=10*mean(TimetoDecc);
                            cycleresult(totalCount).ApercTTDecc=mean(PercentageTTDecc);
                            cyclereuslt(totalCount).ATAPVoffset1=mean(data_output(:,4));
                            cycleresult(totalCount).ATAPVonset2=10*mean(data_output(:,19)-data_output(:,17)-data_output(:,3));
                        end
                        
                        cycleresult(totalCount).Smoothness = mean(tsmooth);
                        cycleresult(totalCount).HAperRT=10*mean(ApertureRT);
                        cycleresult(totalCount).HPAper=mean(PeakAperture);
                        cycleresult(totalCount).HTTPAper=10*mean(TimetoPeakAperture);
                        cycleresult(totalCount).HpercTTPAper=mean(PercentageTTPAper);
                        
                        totalCount=totalCount+1;
                        
                        clear TimetoDecc;
                        clear PercentageTTDecc;
                        clear ApertureRT;
                        clear PeakAperture;
                        clear TimetoPeakAperture;
                        clear PercentageTTPAper;
                        clear tsmooth;
                    else
                        % save in a structure 'cycleresult'
                        
                        cycleresult(totalCount).init=strtrim(subject_list(j,:));
                        cycleresult(totalCount).test=i; % pretest 1, posttest 2
                        cycleresult(totalCount).hand=impairedHand;
                        cycleresult(totalCount).object=k; % bigcube 1, bigcircle 2, smallcube 3, smallcircle 4
                        
                        cycleresult(totalCount).Smoothness = 0;
                        cycleresult(totalCount).ART=0;
                        cycleresult(totalCount).ATTPV=0;
                        cycleresult(totalCount).APV=0;
                        cycleresult(totalCount).ATTDecc=0;
                        cycleresult(totalCount).ApercTTDecc=0;
                        cyclereuslt(totalCount).ATAPVoffset1=0;
                        cycleresult(totalCount).ATAPVonset2=0;
                        
                        cycleresult(totalCount).HAperRT=0;
                        cycleresult(totalCount).HPAper=0;
                        cycleresult(totalCount).HTTPAper=0;
                        cycleresult(totalCount).HpercTTPAper=0;
                        
                        totalCount=totalCount+1;
                    end
                end
            end
        end
    end
end

outputList={'init' 'initNum' 'test' 'hand' 'object' 'ASmoothness' 'ART'  'ATTPV' 'APV' 'ATTDecc' 'ApercTTDecc' 'ATAPVonset2' 'HAperRT' 'HPAper' 'HTTPAper' 'HpercTTPAper'};

% outputList={cycleresult(1).init cycleresult(1).test cycleresult(1).hand cycleresult(1).object cycleresult(1).ART cycleresult(1).ATTPV cycleresult(1).APV cycleresult(1).ATTDecc cycleresult(1).ApercTTDecc cyclereuslt(1).ATAPVoffset1 cycleresult(1).ATAPVonset1 cycleresult(1).HAperRT cycleresult(1).HPAper cycleresult(1).HTTPAper cycleresult(1).HpercTTPAper};

for finalcount=1:totalCount-1
    cycleresult(finalcount).init
    outputList= cat(1,outputList,{cycleresult(finalcount).init cycleresult(finalcount).initNum cycleresult(finalcount).test cycleresult(finalcount).hand cycleresult(finalcount).object mean(cycleresult(finalcount).Smoothness) cycleresult(finalcount).ART cycleresult(finalcount).ATTPV cycleresult(finalcount).APV cycleresult(finalcount).ATTDecc cycleresult(finalcount).ApercTTDecc cycleresult(finalcount).ATAPVonset2 cycleresult(finalcount).HAperRT cycleresult(finalcount).HPAper cycleresult(finalcount).HTTPAper cycleresult(finalcount).HpercTTPAper});
end

xlswrite([pathname1 '\HAT cycledResult All Smooth.xls'],outputList);

