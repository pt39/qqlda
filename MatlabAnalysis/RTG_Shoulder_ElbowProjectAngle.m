function RTG_Shoulder_ElbowProjectAngle(pathname)

clc
close all

affectedside='affected';
SF=100;
handTrajectory={'init'	'hand'	'test'	'group'	'object'	'affected'	'fileName'	'Shoulderonset1' 'Shoulderonset1_hori' 'Elbowonset1' 'Shoulderoffset1' 'Shoulderoffset1_hori' 'Elbowoffset1' 'Shoulderonset2' 'Shoulderonset2_hori' 'Elbowonset2' 'Shoulderoffset2' 'Shoulderoffset2_hori' 'Elbowoffset2' 'Shoulderonset3' 'Shoulderonset3_hori' 'Elbowonset3' 'Shoulderoffset3' 'Shoulderoffset3_hori' 'Elbowoffset3' 'onset1' 'offset1' 'onset2' 'offset2' 'onset3' 'offset3'};

group=input('Group name please (1 Control Group Cycled; 2 HAS cycled; 3 HAT cycled) : ');
switch group
    case 1
        groupDir=[pathname '\Control Group Cycled'];
        groupName='Controls';
    case 2
        groupDir=[pathname '\HAS cycled'];
        groupName='HAS';
    case 3
        groupDir=[pathname '\HAT cycled'];
        groupName='HAT';
end


subjectList=ls(groupDir);
display('Please input subject name as matrix format, etc: [1 2 3]');
questionDis=['subject name please ('];
for subjectNum=3:size(subjectList,1)
    questionDis=[questionDis num2str(subjectNum-2) ' ' strtrim(subjectList(subjectNum,:)) ';'];
end

questionDis=[questionDis ')'];

subjectName=input(questionDis);

% 4 objects  (1 bigcube; 2 bigcircle; 3 smallcube; 4 smallcircle)
for object=1:4
    %object=input('Which object you want to compare? (1 bigcube; 2 bigcircle; 3 smallcube; 4 smallcircle)');
    
    
    
    
    PreAcrossSubj=[];
    PostAcrossSubj=[];
    
    PreGLAcrossSubj=[];
    PostGLAcrossSubj=[];
    
    finalTitleName=[];
    for Num=1:size(subjectName,2)
        subjectDir=[groupDir '\' strtrim(subjectList(subjectName(Num)+2,:))];
        
        preDir=[subjectDir '\pretest'];
        postDir=[subjectDir '\posttest'];
        
        handside=ls(preDir);
        
        if (strcmp(handside(3,:),'right'))
            glovePrefix='RCG';
        else
            glovePrefix='LCG';
        end
        
        
        preDir=[preDir '\' handside(3,:)];
        postDir=[postDir '\' handside(3,:)];
        
%         %load pre calibration file
%         PreCaliFileList=ls([preDir '\calibration\' glovePrefix '*.txt']);
%         PreCalibration1=load([preDir '\calibration\' PreCaliFileList(1,:)]);
%         PreCalibration2=load([preDir '\calibration\' PreCaliFileList(2,:)]);
%         PreCalibration3=load([preDir '\calibration\' PreCaliFileList(3,:)]);
%         
%         %load post calibration file
%         PostCaliFileList=ls([preDir '\calibration\' glovePrefix '*.txt']);
%         PostCalibration1=load([preDir '\calibration\' PostCaliFileList(1,:)]);
%         PostCalibration2=load([preDir '\calibration\' PostCaliFileList(2,:)]);
%         PostCalibration3=load([preDir '\calibration\' PostCaliFileList(3,:)]);
        
        
        switch object
            case 1
                preDir=[preDir '\bigcube'];
                postDir=[postDir '\bigcube'];
                ObjectName='bigcube';
            case 2
                preDir=[preDir '\bigcircle'];
                postDir=[postDir '\bigcircle'];
                ObjectName='bigcircle';
            case 3
                preDir=[preDir '\smallcube'];
                postDir=[postDir '\smallcube'];
                ObjectName='smallcube';
            case 4
                preDir=[preDir '\smallcircle'];
                postDir=[postDir '\smallcircle'];
                ObjectName='smallcircle';
        end
        
        
        PreCyclefileName=[preDir '\Cycled.xls'];
        [Precycle_data, Precycle_header]=xlsread(PreCyclefileName);
        
        PostCyclefileName=[postDir '\Cycled.xls'];
        [Postcycle_data, Postcycle_header]=xlsread(PostCyclefileName);
        
        
        PreData_acrossTrial=[];
        PostData_acrossTrial=[];
        PreCGData_acrossTrial=[];
        PostCGData_acrossTrial=[];
        
        % load pre test
        for PreNOBfileNum=1:size(Precycle_header,1)
            
            invalidFiles=find(Precycle_data(PreNOBfileNum,1:6)==-999999);
            if (isempty(invalidFiles))
                
                
                %load glove data
%                 PreCGFilename=regexprep(char(Precycle_header(PreNOBfileNum,1)),'NOB',glovePrefix);
%                 PreCGFile_tmp=load([preDir '\' PreCGFilename]);
%                 PreCGFile=calibration(PreCGFile_tmp,PreCalibration1,PreCalibration2,PreCalibration3);
                
                %load NOB data
                PreNOBfilename=[preDir '\' char(Precycle_header(PreNOBfileNum,1))]
                PreNOBfile=load(PreNOBfilename);
                
                Preonset1=Precycle_data(PreNOBfileNum,1);
                Preoffset1=Precycle_data(PreNOBfileNum,2);
                Preonset2=Precycle_data(PreNOBfileNum,3);
                Preoffset2=Precycle_data(PreNOBfileNum,4);
                Preonset3=Precycle_data(PreNOBfileNum,5);
                Preoffset3=Precycle_data(PreNOBfileNum,6);
                
                %
                
                % get all variables
                
                % 1 hand pre trajectory***************
                % onset1 - offset1
                preOnset1Offest1Hand=PreNOBfile(Preonset1:Preoffset1,:);
                [ShoulderangleStart1 ShoulderAngleEnd1 ShoulderangleStart1_hori ShoulderAngleEnd1_hori ElbowAngleStart1 ElbowAngleEnd1]=ShoulderElbowMove(preOnset1Offest1Hand);
                
                % offset1 - onset2
                preOffset1Onset2Hand=PreNOBfile(Preoffset1:Preonset2,:);
                if (size(preOffset1Onset2Hand,1)<=1)
                    ShoulderangleStart2=ShoulderAngleEnd1;
                    ShoulderAngleEnd2=ShoulderAngleEnd1;
                    ElbowAngleStart2=ElbowAngleEnd1;
                    ElbowAngleEnd2=ElbowAngleEnd1;
                else
                    [ShoulderangleStart2 ShoulderAngleEnd2 ShoulderangleStart2_hori ShoulderAngleEnd2_hori ElbowAngleStart2 ElbowAngleEnd2]=ShoulderElbowMove(preOffset1Onset2Hand);
                end
                
                % onset2 - offset2
                preOnset2Offset2Hand=PreNOBfile(Preonset2:Preoffset2,:);
                [ShoulderangleStart3 ShoulderAngleEnd3 ShoulderangleStart3_hori ShoulderAngleEnd3_hori ElbowAngleStart3 ElbowAngleEnd3]=ShoulderElbowMove(preOnset2Offset2Hand);
                
                % offset2 - onset3
                preOffset2Onset3Hand=PreNOBfile(Preoffset2:Preonset3,:);
                if (size(preOffset2Onset3Hand,1)<=1)
                    ShoulderangleStart4=ShoulderAngleEnd3;
                    ShoulderAngleEnd4=ShoulderAngleEnd3;
                    ElbowAngleStart4=ElbowAngleEnd3;
                    ElbowAngleEnd4=ElbowAngleEnd3;
                else
                    [ShoulderangleStart4 ShoulderAngleEnd4 ShoulderangleStart4_hori ShoulderAngleEnd4_hori ElbowAngleStart4 ElbowAngleEnd4]=ShoulderElbowMove(preOffset2Onset3Hand);
                end
                
                % onset3 - offset3
                preOnset3Offset3Hand=PreNOBfile(Preonset3:Preoffset3,:);
                if (size(preOnset3Offset3Hand,1)<=1)
                    ShoulderangleStart5=ShoulderAngleEnd4;
                    ShoulderAngleEnd5=ShoulderAngleEnd4;
                    ElbowAngleStart5=ElbowAngleEnd4;
                    ElbowAngleEnd5=ElbowAngleEnd4;
                else
                    [ShoulderangleStart5 ShoulderAngleEnd5 ShoulderangleStart5_hori ShoulderAngleEnd5_hori ElbowAngleStart5 ElbowAngleEnd5]=ShoulderElbowMove(preOnset3Offset3Hand);
                end
                
                handTrajectory=cat(1,handTrajectory,{strtrim(subjectList(subjectName(Num)+2,:)) handside(3,:) 'pretest' groupName ObjectName affectedside char(Precycle_header(PreNOBfileNum,1)) ShoulderangleStart1 ShoulderangleStart1_hori ElbowAngleStart1 ShoulderAngleEnd1 ShoulderAngleEnd1_hori ElbowAngleEnd1 ShoulderAngleEnd2 ShoulderAngleEnd2_hori ElbowAngleEnd2 ShoulderAngleEnd3 ShoulderAngleEnd3_hori ElbowAngleEnd3 ShoulderAngleEnd4 ShoulderAngleEnd4_hori ElbowAngleEnd4 ShoulderAngleEnd5 ShoulderAngleEnd5_hori ElbowAngleEnd5 Preonset1 Preoffset1 Preonset2 Preoffset2 Preonset3 Preoffset3});
                %*********************************
                
                
                
            end
        end
        
        % load post test
        for PostNOBfileNum=1:size(Postcycle_header,1)
            
            invalidFiles=find(Postcycle_data(PostNOBfileNum,1:6)==-999999);
            if (isempty(invalidFiles))
                
                
                %load glove data
%                 PostCGFilename=regexprep(char(Postcycle_header(PostNOBfileNum,1)),'NOB',glovePrefix);
%                 PostCGFile_tmp=load([postDir '\' PostCGFilename]);
%                 PostCGFile=calibration(PostCGFile_tmp,PostCalibration1,PostCalibration2,PostCalibration3);
                
                %load NOB data
                PostNOBfilename=[postDir '\' char(Postcycle_header(PostNOBfileNum,1))]
                PostNOBfile=load(PostNOBfilename);
                
                Postonset1=Postcycle_data(PostNOBfileNum,1);
                Postoffset1=Postcycle_data(PostNOBfileNum,2);
                Postonset2=Postcycle_data(PostNOBfileNum,3);
                Postoffset2=Postcycle_data(PostNOBfileNum,4);
                Postonset3=Postcycle_data(PostNOBfileNum,5);
                Postoffset3=Postcycle_data(PostNOBfileNum,6);
                
                % get all variables
                
                % 1 hand pre trajectory***************
                % onset1 - offset1
                postOnset1Offest1Hand=PostNOBfile(Postonset1:Postoffset1,:);
                [ShoulderangleStart6 ShoulderAngleEnd6 ShoulderangleStart6_hori ShoulderAngleEnd6_hori ElbowAngleStart6 ElbowAngleEnd6]=ShoulderElbowMove(postOnset1Offest1Hand);
                
                % offset1 - onset2
                postOffset1Onset2Hand=PostNOBfile(Postoffset1:Postonset2,:);
                if (size(postOffset1Onset2Hand,1)<=1)
                    ShoulderangleStart7=ShoulderAngleEnd6;
                    ShoulderAngleEnd7=ShoulderAngleEnd6;
                    ElbowAngleStart7=ElbowAngleEnd6;
                    ElbowAngleEnd7=ElbowAngleEnd6;
                else
                    [ShoulderangleStart7 ShoulderAngleEnd7 ShoulderangleStart7_hori ShoulderAngleEnd7_hori ElbowAngleStart7 ElbowAngleEnd7]=ShoulderElbowMove(postOffset1Onset2Hand);
                end
                
                % onset2 - offset2
                postOnset2Offset2Hand=PostNOBfile(Postonset2:Postoffset2,:);
                [ShoulderangleStart8 ShoulderAngleEnd8  ShoulderangleStart8_hori ShoulderAngleEnd8_hori  ElbowAngleStart8 ElbowAngleEnd8]=ShoulderElbowMove(postOnset2Offset2Hand);
                
                % offset2 - onset3
                postOffset2Onset3Hand=PostNOBfile(Postoffset2:Postonset3,:);
                if (size(postOffset2Onset3Hand,1)<=1)
                    ShoulderangleStart9=ShoulderAngleEnd8;
                    ShoulderAngleEnd9=ShoulderAngleEnd8;
                    ElbowAngleStart9=ElbowAngleEnd8;
                    ElbowAngleEnd9=ElbowAngleEnd8;
                else
                    [ShoulderangleStart9 ShoulderAngleEnd9  ShoulderangleStart9_hori ShoulderAngleEnd9_hori  ElbowAngleStart9 ElbowAngleEnd9]=ShoulderElbowMove(postOffset2Onset3Hand);
                end
                
                % onset3 - offset3
                postOnset3Offset3Hand=PostNOBfile(Postonset3:Postoffset3,:);
                if (size(postOnset3Offset3Hand,1)<=1)
                    ShoulderangleStart10=ShoulderAngleEnd9;
                    ShoulderAngleEnd10=ShoulderAngleEnd9;
                    ElbowAngleStart10=ElbowAngleEnd9;
                    ElbowAngleEnd10=ElbowAngleEnd9;
                else
                    [ShoulderangleStart10 ShoulderAngleEnd10 ShoulderangleStart10_hori ShoulderAngleEnd10_hori ElbowAngleStart10 ElbowAngleEnd10]=ShoulderElbowMove(postOnset3Offset3Hand);
                end
                
                handTrajectory=cat(1,handTrajectory,{strtrim(subjectList(subjectName(Num)+2,:)) handside(3,:) 'posttest' groupName ObjectName affectedside char(Postcycle_header(PostNOBfileNum,1)) ShoulderangleStart6 ShoulderangleStart6_hori ElbowAngleStart6 ShoulderAngleEnd6 ShoulderAngleEnd6_hori ElbowAngleEnd6 ShoulderAngleEnd7 ShoulderAngleEnd7_hori ElbowAngleEnd7 ShoulderAngleEnd8 ShoulderAngleEnd8_hori ElbowAngleEnd8 ShoulderAngleEnd9 ShoulderAngleEnd9_hori ElbowAngleEnd9 ShoulderAngleEnd10 ShoulderAngleEnd10_hori ElbowAngleEnd10 Postonset1 Postoffset1 Postonset2 Postoffset2 Postonset3 Postoffset3});
                %*********************************
                
            end
        end
        
    end
    
end

xlswrite([pathname '\' groupName '_affected_shoulderAngle_ElbowAngle_hori.xls'],handTrajectory);
