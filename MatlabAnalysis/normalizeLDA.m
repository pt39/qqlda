% A,B] = xlsfinfo(excelname);
% if CVS==2  
%     
% %
% sheetValid1 = any(strcmp(B, 'preright'))
% if sheetValid1==1
% [preImpaired]=xlsread(excelname,'preright','BP2:BP400');
% else
%     
% end
% %
% sheetValid2 = any(strcmp(B, 'postright'))
% if sheetValid2==1
% [postImpaired]=xlsread(excelname,'postright','BP2:BP400');
% end
% 
% [A3,B3] = xlsfinfo(excelname);
% sheetValid3 = any(strcmp(B, 'preleft'))
% if sheetValid3==1
% [preUnimpaired]=xlsread(excelname,'preleft','BP2:BP400');
% end
% 
% [A4,B4] = xlsfinfo(excelname);
% sheetValid4 = any(strcmp(B, 'postleft'))
% if sheetValid4==1
% [postUnimpaired]=xlsread(excelname,'postleft','BP2:BP400');
% end
% %this scrip is to look at the offset1 of each shape and take the average of
% %all trials and normalize all LDA curves based on the maximum offset1.
%%
%pre impaired
% 
function [offset1avg]=normalizeLDA(subject,t,h)


s='C:\LDA_analysis_summer2012\CYCLED\Control Group Cycled\' ;
if t==1
time='\pretest\';
else
    time='\posttest\';
end
if h==1
    hand='left'
else
    hand='right';
end

dir=[s subject time hand '\bigcircle']
cd(dir);
offset=xlsread('cycled.xls','sheet1','E1:E10');
offset1avg=mean(offset);

end
