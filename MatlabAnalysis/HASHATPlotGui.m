function varargout = HASHATPlotGui(varargin)
% HASHATPLOTGUI MATLAB code for HASHATPlotGui.fig
%      HASHATPLOTGUI, by itself, creates a new HASHATPLOTGUI or raises the existing
%      singleton*.
%
%      H = HASHATPLOTGUI returns the handle to a new HASHATPLOTGUI or the handle to
%      the existing singleton*.
%
%      HASHATPLOTGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in HASHATPLOTGUI.M with the given input arguments.
%
%      HASHATPLOTGUI('Property','Value',...) creates a new HASHATPLOTGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before HASHATPlotGui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to HASHATPlotGui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help HASHATPlotGui

% Last Modified by GUIDE v2.5 25-Jul-2012 15:03:34

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @HASHATPlotGui_OpeningFcn, ...
                   'gui_OutputFcn',  @HASHATPlotGui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before HASHATPlotGui is made visible.
function HASHATPlotGui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to HASHATPlotGui (see VARARGIN)

% Choose default command line output for HASHATPlotGui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes HASHATPlotGui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = HASHATPlotGui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global ExcelFileName_A1

[A1_FileName,A1_PathName,FilterIndex]= uigetfile ('*.xlsx','Get cycled output file');
ExcelFileName_A1 = [A1_PathName A1_FileName];
[status,sheets] = xlsfinfo(ExcelFileName_A1);
set(handles.Sheetlist,'String',sheets);

% --- Executes on selection change in Sheetlist.
function Sheetlist_Callback(hObject, eventdata, handles)
% hObject    handle to Sheetlist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Sheetlist contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Sheetlist
global ExcelFileName_A1

Sheets_index = get(handles.Sheetlist,'Value');
Sheets_list = get(handles.Sheetlist,'String');
SheetName = Sheets_list(Sheets_index);


plotTable(ExcelFileName_A1,SheetName);
% set(handles.uitable1,'data',


% --- Executes during object creation, after setting all properties.
function Sheetlist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Sheetlist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
