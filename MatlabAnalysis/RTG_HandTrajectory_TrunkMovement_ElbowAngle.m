function RTG_HandTrajectory_TrunkMovement_ElbowAngle(pathname)

clc
close all

affectedside='unaffected';
SF=100;
handTrajectory={'init'	'hand'	'test'	'group'	'object'	'affected'	'fileName'	'handTraj_onset1offset1_dur'	'handTraj_onset1offset1_Cumpath'	'handTraj_onset1offset1_smooth'	'trunkMove_onset1offset1'	'Elbow_onset1offset1'	'Elbow_onset1' 'Elbow_offset1' 'handTraj_offset1onset2_dur'	'handTraj_offset1onset2_Cumpath'	'handTraj_offset1onset2_smooth'	'trunkMove_offset1onset2'	'Elbow_offset1onset2'	'Elbow_onset2' 'handTraj_onset2offset2_dur'	'handTraj_onset2offset2_Cumpath'	'handTraj_onset2offset2_smooth'	'trunkMove_onset2offset2'	'Elbow_onset2offset2'	'Elbow_offset2' 'handTraj_offset2onset3_dur'	'handTraj_offset2onset3_Cumpath'	'handTraj_offset2onset3_smooth'	'trunkMove_offset2onset3'	'Elbow_offset2onset3'	'Elbow_onset3' 'handTraj_onset3offset3_dur'	'handTraj_onset3offset3_Cumpath'	'handTraj_onset3offset3_smooth'	'trunkMove_onset3offset3'	'Elbow_onset3offset3' 'Elbow_offset3' 'onset1' 'offset1' 'onset2' 'offset2' 'onset3' 'offset3'};

group=input('Group name please (1 Control Group Cycled; 2 HAS cycled; 3 HAT cycled) : ');
switch group
    case 1
        groupDir=[pathname '\Control Group Cycled'];
        groupName='Controls';
    case 2
        groupDir=[pathname '\HAS cycled'];
        groupName='HAS';
    case 3
        groupDir=[pathname '\HAT cycled'];
        groupName='HAT';
end


subjectList=ls(groupDir);
display('Please input subject name as matrix format, etc: [1 2 3]');
questionDis=['subject name please ('];
for subjectNum=3:size(subjectList,1)
    questionDis=[questionDis num2str(subjectNum-2) ' ' strtrim(subjectList(subjectNum,:)) ';'];
end

questionDis=[questionDis ')'];

subjectName=input(questionDis);

% 4 objects  (1 bigcube; 2 bigcircle; 3 smallcube; 4 smallcircle)
for object=1:4
    %object=input('Which object you want to compare? (1 bigcube; 2 bigcircle; 3 smallcube; 4 smallcircle)');
    
    
    
    
    PreAcrossSubj=[];
    PostAcrossSubj=[];
    
    PreGLAcrossSubj=[];
    PostGLAcrossSubj=[];
    
    finalTitleName=[];
    for Num=1:size(subjectName,2)
        subjectDir=[groupDir '\' strtrim(subjectList(subjectName(Num)+2,:))];
        
        preDir=[subjectDir '\pretest'];
        postDir=[subjectDir '\posttest'];
        
        handside=ls(preDir);
        
        if (strcmp(handside(3,:),'right'))
            glovePrefix='RCG';
        else
            glovePrefix='LCG';
        end
        
        
        preDir=[preDir '\' handside(3,:)];
        postDir=[postDir '\' handside(3,:)];
        
%         %load pre calibration file
%         PreCaliFileList=ls([preDir '\calibration\' glovePrefix '*.txt']);
%         PreCalibration1=load([preDir '\calibration\' PreCaliFileList(1,:)]);
%         PreCalibration2=load([preDir '\calibration\' PreCaliFileList(2,:)]);
%         PreCalibration3=load([preDir '\calibration\' PreCaliFileList(3,:)]);
%         
%         %load post calibration file
%         PostCaliFileList=ls([preDir '\calibration\' glovePrefix '*.txt']);
%         PostCalibration1=load([preDir '\calibration\' PostCaliFileList(1,:)]);
%         PostCalibration2=load([preDir '\calibration\' PostCaliFileList(2,:)]);
%         PostCalibration3=load([preDir '\calibration\' PostCaliFileList(3,:)]);
        
        
        switch object
            case 1
                preDir=[preDir '\bigcube'];
                postDir=[postDir '\bigcube'];
                ObjectName='bigcube';
            case 2
                preDir=[preDir '\bigcircle'];
                postDir=[postDir '\bigcircle'];
                ObjectName='bigcircle';
            case 3
                preDir=[preDir '\smallcube'];
                postDir=[postDir '\smallcube'];
                ObjectName='smallcube';
            case 4
                preDir=[preDir '\smallcircle'];
                postDir=[postDir '\smallcircle'];
                ObjectName='smallcircle';
        end
        
        
        PreCyclefileName=[preDir '\Cycled.xls'];
        [Precycle_data, Precycle_header]=xlsread(PreCyclefileName);
        
        PostCyclefileName=[postDir '\Cycled.xls'];
        [Postcycle_data, Postcycle_header]=xlsread(PostCyclefileName);
        
        
        PreData_acrossTrial=[];
        PostData_acrossTrial=[];
        PreCGData_acrossTrial=[];
        PostCGData_acrossTrial=[];
        
        % load pre test
        for PreNOBfileNum=1:size(Precycle_header,1)
            
            invalidFiles=find(Precycle_data(PreNOBfileNum,1:6)==-999999);
            if (isempty(invalidFiles))
                
                
                %load glove data
%                 PreCGFilename=regexprep(char(Precycle_header(PreNOBfileNum,1)),'NOB',glovePrefix);
%                 PreCGFile_tmp=load([preDir '\' PreCGFilename]);
%                 PreCGFile=calibration(PreCGFile_tmp,PreCalibration1,PreCalibration2,PreCalibration3);
                
                %load NOB data
                PreNOBfilename=[preDir '\' char(Precycle_header(PreNOBfileNum,1))]
                PreNOBfile=load(PreNOBfilename);
                
                Preonset1=Precycle_data(PreNOBfileNum,1);
                Preoffset1=Precycle_data(PreNOBfileNum,2);
                Preonset2=Precycle_data(PreNOBfileNum,3);
                Preoffset2=Precycle_data(PreNOBfileNum,4);
                Preonset3=Precycle_data(PreNOBfileNum,5);
                Preoffset3=Precycle_data(PreNOBfileNum,6);
                
                %
                
                % get all variables
                
                % 1 hand pre trajectory***************
                % onset1 - offset1
                preOnset1Offest1Hand=PreNOBfile(Preonset1:Preoffset1,:);
                [trunkDist1 ElbowAngle1 ElbowAngleStart1 ElbowAngleEnd1]=trunkElbowMove(preOnset1Offest1Hand);
                if ((Preoffset1-Preonset1)>=3)
                    [Duration1,Cumpath1,tsmooth1]=tractory(preOnset1Offest1Hand);
                else
                    Duration1=0;
                    Cumpath1=0;
                    tsmooth1=0;
                end
                
                % offset1 - onset2
                preOffset1Onset2Hand=PreNOBfile(Preoffset1:Preonset2,:);
                [trunkDist2 ElbowAngle2 ElbowAngleStart2 ElbowAngleEnd2]=trunkElbowMove(preOffset1Onset2Hand);
                if ((Preonset2-Preoffset1)>=3)
                    [Duration2,Cumpath2,tsmooth2]=tractory(preOffset1Onset2Hand);
                else
                    Duration2=0;
                    Cumpath2=0;
                    tsmooth2=0;
                end
                
                % onset2 - offset2
                preOnset2Offset2Hand=PreNOBfile(Preonset2:Preoffset2,:);
                [trunkDist3 ElbowAngle3 ElbowAngleStart3 ElbowAngleEnd3]=trunkElbowMove(preOnset2Offset2Hand);
                if ((Preoffset2-Preonset2)>=3)
                    [Duration3,Cumpath3,tsmooth3]=tractory(preOnset2Offset2Hand);
                else
                    Duration3=0;
                    Cumpath3=0;
                    tsmooth3=0;
                end
                
                % offset2 - onset3
                preOffset2Onset3Hand=PreNOBfile(Preoffset2:Preonset3,:);
                [trunkDist4 ElbowAngle4 ElbowAngleStart4 ElbowAngleEnd4]=trunkElbowMove(preOffset2Onset3Hand);
                if ((Preonset3-Preoffset2)>=3)
                    [Duration4,Cumpath4,tsmooth4]=tractory(preOffset2Onset3Hand);
                else
                    Duration4=0;
                    Cumpath4=0;
                    tsmooth4=0;
                end
                
                % onset3 - offset3
                preOnset3Offset3Hand=PreNOBfile(Preonset3:Preoffset3,:);
                [trunkDist5 ElbowAngle5 ElbowAngleStart5 ElbowAngleEnd5]=trunkElbowMove(preOnset3Offset3Hand);
                if ((Preoffset3-Preonset3)>=3)
                    [Duration5,Cumpath5,tsmooth5]=tractory(preOnset3Offset3Hand);
                else
                    Duration5=0;
                    Cumpath5=0;
                    tsmooth5=0;
                end
                
                handTrajectory=cat(1,handTrajectory,{strtrim(subjectList(subjectName(Num)+2,:)) handside(3,:) 'pretest' groupName ObjectName affectedside char(Precycle_header(PreNOBfileNum,1)) Duration1 Cumpath1 tsmooth1 trunkDist1 ElbowAngle1 ElbowAngleStart1 ElbowAngleEnd1 Duration2  Cumpath2    tsmooth2 trunkDist2 ElbowAngle2 ElbowAngleEnd2 Duration3  Cumpath3    tsmooth3 trunkDist3 ElbowAngle3 ElbowAngleEnd3 Duration4 Cumpath4 tsmooth4 trunkDist4 ElbowAngle4 ElbowAngleEnd4 Duration5 Cumpath5 tsmooth5 trunkDist5 ElbowAngle5 ElbowAngleEnd5 Preonset1 Preoffset1 Preonset2 Preoffset2 Preonset3 Preoffset3});
                %*********************************
                
                
                
            end
        end
        
        % load post test
        for PostNOBfileNum=1:size(Postcycle_header,1)
            
            invalidFiles=find(Postcycle_data(PostNOBfileNum,1:6)==-999999);
            if (isempty(invalidFiles))
                
                
                %load glove data
%                 PostCGFilename=regexprep(char(Postcycle_header(PostNOBfileNum,1)),'NOB',glovePrefix);
%                 PostCGFile_tmp=load([postDir '\' PostCGFilename]);
%                 PostCGFile=calibration(PostCGFile_tmp,PostCalibration1,PostCalibration2,PostCalibration3);
                
                %load NOB data
                PostNOBfilename=[postDir '\' char(Postcycle_header(PostNOBfileNum,1))]
                PostNOBfile=load(PostNOBfilename);
                
                Postonset1=Postcycle_data(PostNOBfileNum,1);
                Postoffset1=Postcycle_data(PostNOBfileNum,2);
                Postonset2=Postcycle_data(PostNOBfileNum,3);
                Postoffset2=Postcycle_data(PostNOBfileNum,4);
                Postonset3=Postcycle_data(PostNOBfileNum,5);
                Postoffset3=Postcycle_data(PostNOBfileNum,6);
                
                % get all variables
                
                % 1 hand pre trajectory***************
                % onset1 - offset1
                postOnset1Offest1Hand=PostNOBfile(Postonset1:Postoffset1,:);
                [trunkDist6 ElbowAngle6 ElbowAngleStart6 ElbowAngleEnd6]=trunkElbowMove(postOnset1Offest1Hand);
                if ((Postoffset1-Postonset1)>=3)
                    [Duration6,Cumpath6,tsmooth6]=tractory(postOnset1Offest1Hand);
                else
                    Duration6=0;
                    Cumpath6=0;
                    tsmooth6=0;
                end
                
                % offset1 - onset2
                postOffset1Onset2Hand=PostNOBfile(Postoffset1:Postonset2,:);
                [trunkDist7 ElbowAngle7 ElbowAngleStart7 ElbowAngleEnd7]=trunkElbowMove(postOffset1Onset2Hand);
                if ((Postonset2-Postoffset1)>=3)
                    [Duration7,Cumpath7,tsmooth7]=tractory(postOffset1Onset2Hand);
                else
                    Duration7=0;
                    Cumpath7=0;
                    tsmooth7=0;
                end
                
                % onset2 - offset2
                postOnset2Offset2Hand=PostNOBfile(Postonset2:Postoffset2,:);
                [trunkDist8 ElbowAngle8  ElbowAngleStart8 ElbowAngleEnd8]=trunkElbowMove(postOnset2Offset2Hand);
                if ((Postoffset2-Postonset2)>=3)
                    [Duration8,Cumpath8,tsmooth8]=tractory(postOnset2Offset2Hand);
                else
                    Duration8=0;
                    Cumpath8=0;
                    tsmooth8=0;
                end
                
                % offset2 - onset3
                postOffset2Onset3Hand=PostNOBfile(Postoffset2:Postonset3,:);
                [trunkDist9 ElbowAngle9  ElbowAngleStart9 ElbowAngleEnd9]=trunkElbowMove(postOffset2Onset3Hand);
                if ((Postonset3-Postoffset2)>=3)
                    [Duration9,Cumpath9,tsmooth9]=tractory(postOffset2Onset3Hand);
                else
                    Duration9=0;
                    Cumpath9=0;
                    tsmooth9=0;
                end
                
                % onset3 - offset3
                postOnset3Offset3Hand=PostNOBfile(Postonset3:Postoffset3,:);
                [trunkDist10 ElbowAngle10 ElbowAngleStart10 ElbowAngleEnd10]=trunkElbowMove(postOnset3Offset3Hand);
                if ((Postoffset3-Postonset3)>=3)
                    [Duration10,Cumpath10,tsmooth10]=tractory(postOnset3Offset3Hand);
                else
                    Duration10=0;
                    Cumpath10=0;
                    tsmooth10=0;
                end
                
                handTrajectory=cat(1,handTrajectory,{strtrim(subjectList(subjectName(Num)+2,:)) handside(3,:) 'posttest' groupName ObjectName affectedside char(Postcycle_header(PostNOBfileNum,1)) Duration6 Cumpath6 tsmooth6 trunkDist6 ElbowAngle6 ElbowAngleStart6 ElbowAngleEnd6 Duration7 Cumpath7 tsmooth7 trunkDist7 ElbowAngle7 ElbowAngleEnd7 Duration8 Cumpath8  tsmooth8 trunkDist8 ElbowAngle8 ElbowAngleEnd8 Duration9 Cumpath9 tsmooth9 trunkDist9 ElbowAngle9 ElbowAngleEnd9 Duration10 Cumpath10 tsmooth10 trunkDist10 ElbowAngle10 ElbowAngleEnd10 Postonset1 Postoffset1 Postonset2 Postoffset2 Postonset3 Postoffset3});
                %*********************************
                
            end
        end
        
    end
    
end

xlswrite([pathname '\' groupName '_Unaffected_HandTrajectory_TrunkMovement_ElbowAngle.xls'],handTrajectory);
