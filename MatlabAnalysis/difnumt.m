
function [deriv]=difnumt(f,deltat)
% DIFNUM = numerical differentiation as classically defined
% f = vector
% concatenation used to keep the same # of rows

deriv=diff(f)./deltat;
deriv=[deriv(1); deriv];




