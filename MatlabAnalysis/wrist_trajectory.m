function wrist_trajectory()
clear all
close all
clc

[data text] = xlsread('../results/DataSheet/RTG ALL with average 11_01_2013_imp_unImp.xls','Unimpaired Latest Finger+elbow');

condition_old = [];
figure

for i=1:size(data,1)
    subInit = char(text(i+1,1));
    group_temp = char(text(i+1,2));
    test = char(text(i+1,3));
    hand = char(text(i+1,4));
    object = char(text(i+1,6));
    filename = char(text(i+1,7));
    
    switch group_temp
        case 'HAT'
            group = 'HAT Cycled';
        case 'HAS'
            group = 'HAS Cycled';
        case 'Controls'
            group = 'Control Group Cycled';
    end
    
    onset1 = data(i,13)/10;
    offset1 = data(i,14)/10;
    onset2 = data(i,15)/10;
    offset2 = data(i,16)/10;
    
    condition_new = ['../CYCLED/' group '/' subInit '/' hand '/' object];
    
    if strcmp(condition_old, condition_new)
        hold on
    else
        saveppt('UnImpaired_RTGWrisrTrajectory.ppt');
        close all
        figure;
              
    end
    fileDirectory = ['../CYCLED/' group '/' subInit '/' test '/' hand '/' object '/' filename];
    NOBfile = load(fileDirectory);
    if strcmp(test,'pretest')
        plot(NOBfile(onset1:onset2,4), NOBfile(onset1:onset2,2));
    else
        plot(NOBfile(onset1:onset2,4), NOBfile(onset1:onset2,2),'r');
    end
    title(condition_new);
    condition_old =condition_new;
    
end
    
    
